using System;
using System.Collections;
//抽象访问者
abstract class Visitor
{
	//访问方法，以成员为参数
	abstract public void Visit( Element element );
}
//具体访问者-薪水访问者
class IncomeVisitor : Visitor
{
	public override void Visit( Element element )
	{
		Employee employee = ((Employee)element);
		// 提高10%的薪水
		employee.Income *= 1.10;
		Console.WriteLine( "{0}新的薪水: {1:C}",employee.Name, employee.Income );
	}
}
//具体访问者-假期访问者
class VacationVisitor : Visitor
{
	public override void Visit( Element element )
	{
		Employee employee = ((Employee)element);
		//提供3天额外的假期
		employee.VacationDays += 3;
		Console.WriteLine( "{0}新的假期天数: {1}",employee.Name, employee.VacationDays );
	}
}
// 抽象成员类
abstract class Element
{
	//接受访问者方法
	abstract public void Accept( Visitor visitor );
}

//具体成员-雇员类
class Employee : Element
{
	string name;
	double income;
	int vacationDays;
	public Employee( string name, double income,int vacationDays )
	{
		this.name = name;
		this.income = income;
		this.vacationDays = vacationDays;
	}
	public string Name //名字
	{
		get{ return name; }
		set{ name = value; }
	}
	public double Income//薪水
	{
		get{ return income; }
		set{ income = value; }
	}
	public int VacationDays//假期
	{
		get{ return vacationDays; }
		set{ vacationDays = value; }
	}
	//重载接受访问者对象方法
	public override void Accept( Visitor visitor )
	{
		visitor.Visit( this );
	}
}
//员工聚合类
class Employees
{
	private ArrayList employees = new ArrayList();
	//增加员工
	public void Attach( Employee employee )
	{
		employees.Add( employee );
	}
	//删除聚合中的员工
	public void Detach( Employee employee )
	{
		employees.Remove( employee );
	}
	//所有聚合的员工都接受访问者
	public void Accept( Visitor visitor )
	{
		foreach( Employee e in employees )
			e.Accept( visitor );
	}
}
/// <summary>
/// VisitorApp test访问者应用测试程序
/// </summary>
public class VisitorApp
{
	public static void Main( string[] args )
	{
		// 配置员工聚合
		Employees e = new Employees();
		e.Attach( new Employee( "张三", 25000.0, 14 ) );
		e.Attach( new Employee( "李四", 35000.0, 16 ) );
		e.Attach( new Employee( "张明", 45000.0, 21 ) );
		//生成两个访问者对象
		IncomeVisitor v1 = new IncomeVisitor();
		VacationVisitor v2 = new VacationVisitor();
		//聚合中的员工接受两个访问者对象
		e.Accept( v1 );
		e.Accept( v2 );
		Console.Read();
	}
} 