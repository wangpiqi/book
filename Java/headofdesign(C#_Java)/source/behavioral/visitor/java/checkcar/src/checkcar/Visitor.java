package checkcar;
//visitor接口,包括访问各种成员的方法：
public interface Visitor {
  void visit(Wheel wheel); //检查车轮
  void visit(Engine engine);//检查发动机
  void visit(Body body);//检查车身
  void visit(Car car);//检查一辆车
}