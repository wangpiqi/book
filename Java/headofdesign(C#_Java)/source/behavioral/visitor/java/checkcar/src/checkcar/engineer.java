package checkcar;
//访问者子类维修工程师：
public class engineer implements Visitor {
  protected String name;
  public engineer(String thename){
    name=thename;
  }
  public void visit(Wheel wheel) {
    System.out.println("检查"+wheel.getName());
   }
  public void visit(Engine engine) {
    System.out.println("检查发动机");

  }
  public void visit(Body body) {
    System.out.println("检查车身");
  }
  public void visit(Car car) {
    System.out.println(name+"检查全车");
  }
}