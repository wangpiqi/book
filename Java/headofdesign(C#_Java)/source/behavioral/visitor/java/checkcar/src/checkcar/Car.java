package checkcar;
//整车类，它的检查是所有部件的检查：
public class Car {
  private Engine  engine = new Engine();
   private Body    body   = new Body();
    private Wheel[] wheels
        = { new Wheel("左前轮"), new Wheel("右前轮"),
           new Wheel("左后轮") , new Wheel("右后轮")  };
    void accept(Visitor visitor){
       visitor.visit (this);
       engine.accept (visitor);
       body.accept (visitor);
       for (int i = 0; i < wheels.length; i++)
       {
         ((Wheel)wheels[i]).accept(visitor);
         }
    }
}