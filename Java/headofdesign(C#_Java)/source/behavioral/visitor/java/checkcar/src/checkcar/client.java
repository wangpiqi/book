package checkcar;
//访问者模式应用测试：
public class client {
  public static void main(String[] args) {
    Car car = new Car();
    Visitor visitor = new engineer("工程师小王");
    car.accept(visitor);
  }
}