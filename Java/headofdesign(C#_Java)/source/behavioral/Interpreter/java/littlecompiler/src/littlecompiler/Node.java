package littlecompiler;

public interface Node {
   public void parse(Context context);
   public void execute();
}