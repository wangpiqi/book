using System;
using System.Text;
using System.Collections;
//Context场境
class Context
{
	private string input;
	private int output;
	public Context( string input )
	{
		this.input = input;
	}
	//属性
	public string Input
	{
		get{ return input; }
		set{ input = value; }
	}
	public int Output
	{
		get{ return output; }
		set{ output = value; }
	}
}
//抽象表达式类
abstract class Expression
{
	public void Interpret( Context context )
	{
		if( context.Input.Length == 0 ) return;
		if( context.Input.StartsWith( Nine() ) )
		{
			context.Output += 9 * Multiplier();
			context.Input = context.Input.Substring(2);
		}
		else if( context.Input.StartsWith( Four() ) )
		{
			context.Output += 4 * Multiplier();
			context.Input = context.Input.Substring( 2);
		}
		else if( context.Input.StartsWith( Five() ) )
		{
			context.Output += 5 * Multiplier();
			context.Input = context.Input.Substring( 2);
		}
		while( context.Input.StartsWith( One() ) )
		{
			context.Output += 1 * Multiplier();
			context.Input = context.Input.Substring( 2 );
		}
	}
	public abstract string One();
	public abstract string Four();
	public abstract string Five();
	public abstract string Nine();
	public abstract int Multiplier();
}
//核对千位的上文字,TerminalExpression终结符表达式类

class ThousandExpression : Expression
{
	public override string One() { return "壹仟"; }
	public override string Four(){ return "肆仟"; }
	public override string Five(){ return "伍仟"; }
	public override string Nine(){ return "玖仟"; }
	public override int Multiplier() { return 1000; }
}
//核对百位上的文字,TerminalExpression终结符表达式类
class HundredExpression : Expression
{
	public override string One() { return "壹百"; }
	public override string Four(){ return "肆百"; }
	public override string Five(){ return "伍百"; }
	public override string Nine(){ return "玖百"; }
	public override int Multiplier() { return 100; }
}
//核对十位上的文字,TerminalExpression终结符表达式类
class TenExpression : Expression
{
	public override string One() { return "壹拾"; }
	public override string Four(){ return "肆拾"; }
	public override string Five(){ return "伍拾"; }
	public override string Nine(){ return "玖拾"; }
	public override int Multiplier() { return 10; }
}
//核对个位上的文字,TerminalExpression终结符表达式类
class OneExpression : Expression
{
	public override string One() { return "壹圆"; }
	public override string Four(){ return "肆圆"; }
	public override string Five(){ return "伍圆"; }
	public override string Nine(){ return "玖圆"; }
	public override int Multiplier() { return 1; }
}
/// <summary>
///  解释器模式的应用测试
/// </summary>
public class InterpreterApp
{
	public static void Main( string[] args )
	{
		string chinese = "壹仟伍百玖拾伍圆";
		Context context = new Context( chinese );
		//生成分析树
		ArrayList parse = new ArrayList();
		parse.Add(new ThousandExpression());
		parse.Add(new HundredExpression());
		parse.Add(new TenExpression());
		parse.Add(new OneExpression());
		//逐步的解释
		foreach( Expression exp in parse )
			exp.Interpret( context );
        //输出结果
		Console.WriteLine( "{0} = {1}",
			chinese, context.Output );
		Console.Read();//按键退出，方便查看结果
	}
}  
