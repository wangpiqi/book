package undoable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import java.util.Iterator;
import javax.swing.undo.*;
import java.util.LinkedList;
import java.util.Iterator;
import java.awt.geom.*;
import java.net.*;

class undoapp
    extends JFrame {
  JButton linebutton;
  JButton circlebutton;
  JButton undobutton;
  JButton redobutton;
  JPanel buttonpanel;
  LinkedList shapes;
  UndoManager undomgr;

  JGraphPanel graphpanel;
  LineListener linelistener;
  CircleListener circlelistener;
  UndoListener undolistener;
  RedoListener redolistener;

  public undoapp () throws HeadlessException {
    try {
      jbInit ();
    }
    catch (Exception e) {
      e.printStackTrace ();
    }
  }

  private void jbInit () throws Exception {
    // Force SwingApplet to come up in the System L&F
    String laf = UIManager.getSystemLookAndFeelClassName ();
    try {
      UIManager.setLookAndFeel (laf);
      // If you want the Cross Platform L&F instead, comment out the above line and
      // uncomment the following:
      // UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
    }
    catch (UnsupportedLookAndFeelException exc) {
      System.err.println ("Warning: UnsupportedLookAndFeel: " + laf);
    }
    catch (Exception exc) {
      System.err.println ("Error loading " + laf + ": " + exc);
    }

    getContentPane ().setLayout (new BorderLayout ());
    linebutton = new JButton ("Draw Line");
    circlebutton = new JButton ("Draw Circle");
    undobutton = new JButton ("Undo");
    redobutton = new JButton ("Redo");
    graphpanel = new JGraphPanel (false);
    graphpanel.setPreferredSize (new Dimension (300, 300));
    buttonpanel = new JPanel (false);
    buttonpanel.setLayout (new FlowLayout ());
    buttonpanel.add (linebutton);
    buttonpanel.add (circlebutton);
    buttonpanel.add (undobutton);
    buttonpanel.add (redobutton);
    getContentPane ().add (buttonpanel, BorderLayout.SOUTH);
    getContentPane ().add (graphpanel, BorderLayout.NORTH);
    linelistener = new LineListener ();
    linebutton.addActionListener (linelistener);
    circlelistener = new CircleListener ();
    circlebutton.addActionListener (circlelistener);
    undolistener = new UndoListener ();
    undobutton.addActionListener (undolistener);
    redolistener = new RedoListener ();
    redobutton.addActionListener (redolistener);
    shapes = new LinkedList ();
    undomgr = new UndoManager ();

  }
  public static  void  main(String argv[])
  {
    undoapp app=new undoapp();
    app.setSize(400,300);
    app.show();
  }

  class LineListener
      implements ActionListener {
    Shape temp;
    public void actionPerformed (ActionEvent e) {
      temp = new Line2D.Double (200.0, 150.0, Math.random () * 100.0,
                                Math.random () * 100.0);
      shapes.add (temp);
      repaint ();
      UndoableEdit edit = new graphEdit (temp);
      undomgr.addEdit (edit);
    }
  }

  class CircleListener
      implements ActionListener {
    Shape temp;
    public void actionPerformed (ActionEvent e) {
      temp = new Ellipse2D.Double (200.0, 150.0, Math.random () * 100.0,
                                   Math.random () * 100.0);
      shapes.add (temp);
      repaint ();
      UndoableEdit edit = new graphEdit (temp);
      undomgr.addEdit (edit);

    }
  }

  class UndoListener
      implements ActionListener {
    public void actionPerformed (ActionEvent e) {
      try {
        undomgr.undo ();
      }
      catch (CannotUndoException ex) {
        System.err.println ("Can't Undo More");
      }
    }
  }

  class RedoListener
      implements ActionListener {
    public void actionPerformed (ActionEvent e) {
      try {
        undomgr.redo ();
      }
      catch (CannotRedoException ex) {
        System.err.println ("Can't Redo More");
      }
    }
  }

  class JGraphPanel
      extends JPanel {
    public JGraphPanel (boolean doublebuffer) {
      super (doublebuffer);
    }

    public void paintComponent (Graphics g) {
      Graphics2D g2 = (Graphics2D) g;
      g2.setColor (Color.white);
      g2.fill3DRect (0, 0, getWidth (), getHeight (), true);
      Iterator it;
      Shape shape;
      g2.setColor (Color.black);
      for (it = shapes.iterator (); it.hasNext (); ) {
        shape = (Shape) it.next ();
        g2.draw (shape);
      }
    }
  }

  class graphEdit
      extends AbstractUndoableEdit {
    Shape shape;
    public graphEdit (Shape _shape) {
      shape = _shape;
    }

    public void undo () {
      shapes.remove (shape);
      repaint ();
      System.out.println ("undo draw line");
    }

    public void redo () {
      shapes.add (shape);
      repaint ();
      System.out.println ("redo draw line");
    }
  }
}