package employee;
import javax.swing.undo.*;
public class EmployeeMemento{
  int mId;
  String mFirstName;
  String mLastName;
  int mSalary;
  EmployeeMemento (int pId, String pFirstName, String pLastName, int pSalary) {
    mId = pId;
    mFirstName = pFirstName;
    mLastName = pLastName;
    mSalary = pSalary;
  }
}