package memento;

public class caretaker{
 /**
  * This class manages memento objects.
  * @version 1.0, 10/25/2002
  * @author		Ryan O'Malley
  */

 /**
  * This variable stores the memento object.
  */
  private memento my_memento;


 /**
  * The class constructor.  Sets the my_memento attribute to null;
  */
  public void caretaker(){
    this.my_memento = null;
  }


 /**
  * This method sets(stores) the memento attribute.
  * @param The memento object to be stored.
  */
  public void set_memento(memento in_memento){
    this.my_memento = in_memento;
  }


 /**
  * This method returns the memento attribute.
  * @return The stored memento object.
  */
  public memento get_memento(){
    return this.my_memento;
  }
}
