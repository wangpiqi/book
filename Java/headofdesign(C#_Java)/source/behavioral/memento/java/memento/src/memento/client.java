package memento;

public class client {
  /**
    * This class tests my Memento design pattern class.
    * @see memento.java, originator.java, caretaker.java
    * @version 1.0, 10/25/2002
    * @author Ryan O'Malley
    */


   /**
    * This main method performs the test.  It creates an originator
    * object and a caretaker object.  It saves the originators initial
    * state, modifies its state, and returns the initial state that
    * was saved in a memento object.
    */
    public static void main(String[] args){
      //Create Originator
      originator my_originator = new originator();
      my_originator.name = "Ryan O'Malley";
      my_originator.phone = "(555)555-5555";
      my_originator.show_me();
      System.out.println("---------------------");

      //Store internal state
      caretaker my_caretaker = new caretaker();
      my_caretaker.set_memento(my_originator.save_memento());

      //Edit Originator
      my_originator.name = "Dr. Dick Sidbury";
      my_originator.phone = "(888)555-8888";
      my_originator.show_me();
      System.out.println("---------------------");

      //ReStore saved state
      my_originator.restore_memento(my_caretaker.get_memento());
      my_originator.show_me();
    }

}