package memento;

public class originator{
 /**
  * @version 1.0, 10/25/2002
  * @author Ryan O'Malley
  */

 /**
  * Attributes of this class.
  */
  public String name;
  public String phone;


 /**
  * The class constructor.  Sets the attributes to null.
  */
  public void originator(){
    this.name = null;
    this.phone = null;
  }


 /**
  * @param  The memento to resore this object from.
  *         The parameter is sent from the caretaker.
  * @see caretaker.java
  */
  public void restore_memento(memento in_memento){
    this.name = in_memento.get_name();
    this.phone = in_memento.get_phone();
  }


 /**
  * This method returns a new memento object.  Ultimately, by
  * doing this, you save this object as a memento in the caretaker.
  * This method is called by the caretaker.
  * @see caretaker.java
  * @return a new memento object.
  */
  public memento save_memento(){
    return (new memento(this.name, this.phone));
  }


 /**
  * This method simply prints out the attributes of the current object.
  */
  public void show_me(){
    System.out.println("Name : " + this.name);
    System.out.println("Phone : " + this.phone);
  }
}
