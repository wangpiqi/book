package memento;

public class memento{
 /**
  * This class is an example of the Memento Design Pattern.
  * @version 1.0, 10/25/2002
  * @author Ryan O'Malley
  */


 /**
  * Attributes of this class.
  */
  private String name;
  private String phone;


 /**
  * The class constructor.
  * @param name The name parameter.
  * @param phone The phone parameter.
  */
  public memento(String name, String phone){
    this.name = name;
    this.phone = phone;
  }


 /**
  * Returns the name attribute.
  * @return name
  */
  public String get_name(){
    return this.name;
  }


 /**
  * Returns the phone attribute.
  * @return phone
  */
  public String get_phone(){
    return this.phone;
  }
}
