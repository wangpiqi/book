package taxonomy;

import java.io.*;

/**
  A simple driver program for our TreeGUI application.
  @see TreeGUI
*/
public class Driver {

  public static void main (String[] args) {
    try {
      // establish a way to read strings from a taxonomy
      // input file specified as a command line argument
      // build a FileReader, then a BufferedReader, then a StreamTokenizer
      FileReader fr = new FileReader("tax.data");
      BufferedReader br = new BufferedReader(fr);
      StreamTokenizer st = new StreamTokenizer(br);

      // construct and display the TreeGUI
      // build the initial tree displayed based on the contents
      // of the stream tokenizer
      TreeGUI t = new TreeGUI(st);
      t.setVisible(true);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
