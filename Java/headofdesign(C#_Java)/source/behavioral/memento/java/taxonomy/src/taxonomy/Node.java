package taxonomy;

import java.util.Enumeration;
import java.util.Vector;

/**
  A simple class defining nodes for use in constructing general trees.
  @author Scott W. Lewandowski
*/
public class Node {
  private Object element;
  private Node   parent;
  private Vector children;

  /**
    Construct a new leaf/external node.
    A leaf/external node has no children.
    @param e The element/item to store in this leaf/external node
    @param p The parent node for this leaf/external node
  */
  public Node (Object e, Node p) {
    element = e;
    parent = p;
    children = new Vector();
  }

  /**
    Return the element/item stored in a node.
    @return The element stored in this node
  */
  public Object getElement () {
    return element;
  }

  /**
    Return a reference to the parent node of a node.
    @return A reference to the parent node of this node
  */
  public Node getParent () {
    return parent;
  }

  /**
    Return an enumeration over the children of a node.
    @return An enumeration over the children of this node
  */
  public Enumeration getChildren () {
    return children.elements();
  }

  /**
    Determine if a node is a leaf.
    A leaf (or external) node is a node with no children.
    @return <code>true</code> if this node is a leaf,
                <code>false</code> otherwise
  */
  public boolean isLeaf () {
    return (children.isEmpty());
  }

  /**
    Add a child to a node.  The new child is a leaf/external node.
    @param e The element/item to store in the child
    @return A reference to the child node added to this node
  */
  public Node addChild(Object e) {
    Node n = new Node(e, this);
    children.add(n);
    return n;
  }

  /**
    Remove a child (and any subtree below it) from a node.
    @param e A reference to the child node to be removed
  */
  public void removeChild(Node e) {
    children.remove(e);
  }

  /**
    Return the position of the specified node.
    @param n A reference to the node to locate
    @return An <code>int</code> representing the position of <code>n</code>
                 as a child of this node, or -1 if <code>n</code> is not a child
            of this node
    @see TaxTreeModel
  */
  public int positionOfChild(Node n) {
    return this.children.indexOf(n);
  }

  /**
    Return a hash code for a node.
    @return The hash code for this node (based on the hash code for the element
            stored in this node)
  */
  public int hashCode() {
    return element.hashCode();
  }

  /**
    Compare two nodes for equality.  Two nodes are equal if they contain the
    same element.  Assumes no two nodes in the tree will have the same contents.
    @param o A reference to a node
    @return <code>true</code> if <code>o</code> if the same as this node,
            <code>false</code> otherwise
  */
  public boolean equals(Object o) {
    return (element.equals(((Node)o).getElement()));
  }

  /**
    Return the number of children (or the fan out of) a node has.
    @return The number of children of this node
    @see TaxTreeModel
  */
  public int fanOut() {
    return children.size();
  }

  /**
    Return the nth child of a node.  Assumes the nth child exists.
    @return A reference to the nth child of this node
    @see TaxTreeModel
  */
  public Object nthChild(int n) {
    return children.elementAt(n);
  }

  /**
    Return a string representation for a node based on its element.
    @return A string representation for this node
  */
  public String toString() {
    return element.toString();
  }

  /**
    Compute the path from the root of a tree to a node.
    @param p A vector to store the path from the root of this tree to this node.
             <code>p.elementAt(0)</code> should be the root of this tree.
             <code>p.lastelement()</code> should be this node.
  */
  public void getPathToRoot(Vector p) {
    p.insertElementAt(this, 0);
    Node newParent = parent;
    while (newParent != null) {
      p.insertElementAt(newParent, 0);
      newParent = newParent.getParent();
    }
  }


}
