package taxonomy;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.undo.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/**
  A GUI class for displaying taxonomy trees.  Supports addition of taxonomy
  elements as well as undo and redo of additions to the taxonomy.
  @see TaxTreeModel
  @see javax.swing.undo.UndoManager
*/
public class TreeGUI extends JFrame
                     implements ActionListener, UndoableEditListener {
  /**
    The JTree to display.
  */
  private JTree tree;

  /**
    The tree model that the JTree provides the view for.
  */
  private TaxTreeModel treeModel;

  private JButton addButton = new JButton("Add Child");

  /**
    Menu item for undo
  */
  private JMenuItem undoMenuItem = new JMenuItem("Undo");

  /**
    Menu item for redo
  */
  private JMenuItem redoMenuItem = new JMenuItem("Redo");

  /**
    The undo manager for this application.
  */
  private UndoManager manager = new UndoManager();

  /**
    Construct a new TreeGUI.  The first token in <code>st</code>
    represents the root of the taxonomy.
    @param st A StreamTokenizer containing taxonomy information
    @see TreeGUI#populateTaxonomy()
  */
  public TreeGUI(StreamTokenizer st) {
    super("Animal, Vegetable, or Mineral");

    setSize(250,600);
    setLocation(75,75);
    addWindowListener(
      new WindowAdapter() {
        public void windowClosing(WindowEvent e) { System.exit(0); }
      }
    );

    JMenuBar theMenuBar = new JMenuBar();
    JMenu fileMenu = new JMenu("File");
    JMenu editMenu = new JMenu("Edit");

    theMenuBar.add(fileMenu);
    theMenuBar.add(editMenu);
    setJMenuBar(theMenuBar);

    JMenuItem exitMenuItem = new JMenuItem("Exit");
    exitMenuItem.addActionListener(this);
    fileMenu.add(exitMenuItem);

    undoMenuItem.addActionListener(this);
    editMenu.add(undoMenuItem);

    redoMenuItem.addActionListener(this);
    editMenu.add(redoMenuItem);

    // initially there's nothing to undo or redo, so have menu options disabled
    undoMenuItem.setEnabled(false);
    redoMenuItem.setEnabled(false);

    // populate the taxonomy (i.e. build a new taxTreeModel based on the
    // contents of the stream tokenizer
    try {
      if(st.nextToken() != StreamTokenizer.TT_EOF) {
        treeModel = new TaxTreeModel(new Node(st.sval, null), this);
        populateTreeModel(st);
      }

    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(1);
    }

    // create a JTree that wraps the treeModel

    tree = new JTree(treeModel);

    // change the icons associated with nodes in the JTree
    DefaultTreeCellRenderer cr = new DefaultTreeCellRenderer();
    cr.setLeafIcon(new ImageIcon("/home/s01_3601/PublicCode/paw.gif"));
    cr.setOpenIcon(new ImageIcon("/home/s01_3601/PublicCode/bird16.gif"));
    cr.setClosedIcon(new ImageIcon("/home/s01_3601/PublicCode/bird16.gif"));

    tree.setCellRenderer(cr);

    // add a button (initially disabled) to the bottom of the TreeGUI
    JPanel buttonPanel = new JPanel();
    addButton.addActionListener(this);
    addButton.setEnabled(false);
    buttonPanel.add(addButton);
    getContentPane().add(buttonPanel, "South");

    // see javax.swing.event.TreeSelectionEvent
    // only enable the button if a node is selected in the tree
    tree.addTreeSelectionListener(
      new TreeSelectionListener() {
        public void valueChanged(TreeSelectionEvent e) {
          addButton.setEnabled(true);
        }
      }
    );

    // make it so that only one node of the tree may be selected at once
    // see Java Swing pp 604

    TreeSelectionModel tsm = new DefaultTreeSelectionModel();
    tsm.setSelectionMode(DefaultTreeSelectionModel.SINGLE_TREE_SELECTION);
    tree.setSelectionModel(tsm);

    // make it so that the root node is collapsable
    tree.setShowsRootHandles(true);

    // add the tree to a scroll pane and add the scroll pane to the TreeGUI
    JScrollPane scrollPane = new JScrollPane(tree);
    getContentPane().add(scrollPane, BorderLayout.CENTER);
  }

  /**
    Required by the ActionListener interface.  Handle the various actions
    associated with buttons and menu items.
  */
  public void actionPerformed(ActionEvent evt) {
    String command = evt.getActionCommand();
    if (command.equals("Exit")) {
      System.exit(0);
    } else if (command.startsWith("Undo")) {
      // see Java Swing pp 668
      try {
        manager.undo();

      } catch (CannotUndoException ex) {
        ex.printStackTrace();
      } finally {
        updateMenuItems();
      }
    } else if (command.startsWith("Redo")) {
      // see Java Swing pp 668
      try {
        manager.redo();

      } catch (CannotUndoException ex) {
        ex.printStackTrace();
      } finally {
        updateMenuItems();
      }
    } else if (command.equals("Add Child")) {
      handleAdd();
    }
  }

  /**
    Routine to allow the user the add an element into the taxonomy.
  */
  private void handleAdd() {
    // identify the node that's going to have a child added to it.
    // see javax.swing.JTree
    TreePath selectedNodePath = tree.getSelectionPath();
    Node selectedNode = (Node)selectedNodePath.getLastPathComponent();

    // construct and display an appropriate JOptionPane to get the new
    // element from the user
    // see javax.swing.JOptionPane
    String newChildVal = (String)JOptionPane.showInputDialog(this,
                                                         "Enter new taxonomic class",
                                                         "Add to Taxonomy",
                                                         JOptionPane.PLAIN_MESSAGE);

    // clear the tree selection
    tree.clearSelection();
    addButton.setEnabled(false);

    // if the user has entered a value, add it (via the TreeModel) into the
    // underlying taxonomy
    if(!newChildVal.equals("")) {
      treeModel.addChild(selectedNode.getElement(), newChildVal);

    }

  }

  /**
    Update undo and redo menu options when an undoable edit has happened.
    @see javax.swing.undo.UndoManager
    @see <u>Java Swing</u> pp 668 (bottom)
  */
  private void updateMenuItems() {
    undoMenuItem.setText(manager.getUndoPresentationName());
    redoMenuItem.setText(manager.getRedoPresentationName());
    undoMenuItem.getParent().validate();
    undoMenuItem.setEnabled(manager.canUndo());
    redoMenuItem.setEnabled(manager.canRedo());
  }

  /**
    When an undoable edit has happened (when the "add child" button is pressed),
    add the edit to the undo manager and update the state of the GUI menu items.
    @see <u>Java Swing</u> pp 668
  */
  public void undoableEditHappened(UndoableEditEvent e) {
    manager.addEdit(e.getEdit());
    updateMenuItems();
  }

  /**
    Repeatedly add items from a StreamTokenizer object into the taxonomy.
    Assumes the taxonomy's root value has been set.
    @param st A StreamTokenizer containing taxonomy data
  */
  private void populateTreeModel(StreamTokenizer st) throws IOException {
    while (st.nextToken() != StreamTokenizer.TT_EOF) {
      // lines in the taxonomy input file consist of three tokens
      // two taxonomy entries separated by 'isa'
      // 'isa' input lines add items to the taxonomy
      // e.g. bat isa mammal

      // get the three tokens
      String childValue;
      String middle;
      String parentValue;

      childValue = st.sval;
      st.nextToken();
      middle = st.sval;
      st.nextToken();
      parentValue =st.sval;

      //System.out.println(childValue + " " + middle + " " + parentValue);

      // add this entry (via the TreeModel) into the underlying taxonomy
      treeModel.addChild(parentValue, childValue);

    }
  }

}
