
using System;
//原发器-销售前景
class SalesProspect
{
	private string name;
	private string phone;
	private double budget;
	public string Name
	{
		get{ return name; }
		set{ name = value; }
	}
	public string Phone
	{
		get{ return phone; }
		set{ phone = value; }
	}
	//预算
	public double Budget
	{
		get{ return budget; }
		set{ budget = value; }
	}
	//保存备忘录
	public Memento SaveMemento()
	{
		return (new Memento( name, phone, budget ));
	}
    //恢复备忘录
	public void RestoreMemento( Memento memento )
	{
		this.name = memento.Name;
		this.phone = memento.Phone;
		this.budget = memento.Budget;
	}
	//显示信息
	public void Show()
	{
		Console.WriteLine( "\n销售期望 ---- " );
		Console.WriteLine( "名字: {0}", this.name );
		Console.WriteLine( "电话: {0}", this.phone );
		Console.WriteLine( "预算: {0:C}", this.budget );
	}
}
//备忘录类
class Memento
{
	private string name;
	private string phone;
	private double budget;
	//构造函数初始化数据
	public Memento(string name,string phone,double budget)
	{
		this.name = name;
		this.phone = phone;
		this.budget = budget;
	}
	public string Name
	{
		get{ return name; }
		set{ name = value; }
	}
	public string Phone
	{
		get{ return phone; }
		set{ phone = value; }
	}
	public double Budget
	{
		get{ return budget; }
		set{ budget = value; }
	}
}
//看管者
class ProspectMemory
{
	private Memento memento;
	//包括备忘录属性
	public Memento Memento
	{
		set{ memento = value; }
		get{ return memento; }
	}
}

/// <summary>
/// 备忘录应用测试
/// </summary>
public class MementoApp
{
	public static void Main( string[] args )
	{
		SalesProspect s = new SalesProspect();
		s.Name = "张主任";
		s.Phone = "(020) 12560990";
		s.Budget = 25000.0;
		s.Show();

		//保存内部状态
		ProspectMemory m = new ProspectMemory();
		m.Memento = s.SaveMemento();
		//继续改变原发器
		s.Name = "陈经理";
		s.Phone = "(020) 12097111";
		s.Budget = 1000000.0;
		s.Show();
		//复原原来保存的状态
		s.RestoreMemento( m.Memento );
		s.Show();
		Console.Read();
	}
} 