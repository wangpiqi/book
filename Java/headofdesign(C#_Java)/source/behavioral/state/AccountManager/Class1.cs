using System;

//抽象状态类
abstract class State
{
	protected Account account;
	protected double balance;
	protected double interest;
	protected double lowerLimit;
	protected double upperLimit;
	//账户
	public Account Account
	{
		get{ return account; }
		set{ account = value; }
	}
	public double Balance
	{
		get{ return balance; }
		set{ balance = value; }
	}
	abstract public void Initialize();
	abstract public void Deposit( double amount );
	abstract public void Withdraw( double amount );
	abstract public void PayInterest();
	abstract public void StateChangeCheck();
}

// 具体状态--(透支的账户)红色状态
// 
class RedState : State
{
	double serviceFee;
	//构筑函数,由其它状态对象传值过来
	public RedState( State state )
	{
		this.balance = state.Balance;
		this.account = state.Account;
		Initialize();
	}
	override public void Initialize()
	{
		//数据可能从数据库取得
		interest = 0.0;
		lowerLimit = -100.0;
		upperLimit = 0.0;
		serviceFee = 15.00;//手续费
	}
    //存款
	override public void Deposit( double amount )
	{
		balance += amount;
		StateChangeCheck();
	}
	//取款
	override public void Withdraw( double amount )
	{
		amount = amount - serviceFee;
		Console.WriteLine("没有存款可供支取!支取失败!" );
	}
	//付利息
	override public void PayInterest()
	{
		// 没有存款，银行不会支付利息
	}
	override public void StateChangeCheck()
	{
		if( balance > upperLimit )//当有存款,状态变为银色
			account.State = new SilverState( this );
	}
}
// 具体状态--(没欠钱，有小量存款的账户)银色状态
class SilverState : State
{
	public SilverState(  
		double balance, Account account )
	{
		this.balance = balance;
		this.account = account;
		Initialize();
	}

	public SilverState( State state )
	{
		this.balance = state.Balance;
		this.account = state.Account;
		Initialize();
	}
	override public void Initialize()
	{
		//数据可能从数据库取得
		interest = 0.0;
		lowerLimit = 0.0;
		upperLimit = 1000.0;
	}
	//存款
	override public void Deposit( double amount )
	{
		balance += amount;
		StateChangeCheck();
	}
    //取款
	override public void Withdraw( double amount )
	{
		balance -= amount;
		StateChangeCheck();
	}
    //付利息
	override public void PayInterest()
	{
		balance += interest * balance;
		StateChangeCheck();
	}

	override public void StateChangeCheck()
	{
		if( balance < lowerLimit ) //当存款小于0
			account.State = new RedState( this );
		else if( balance > upperLimit )//当存款大于1000
			account.State = new GoldState( this );
	}
}

//具体状态--(有大量存款的账户)金色状态

class GoldState : State
{
	public GoldState(  
		double balance, Account account )
	{
		this.balance = balance;
		this.account = account;
		Initialize();
	}

	public GoldState( State state )
	{
		this.balance = state.Balance;
		this.account = state.Account;
		Initialize();
	}

	override public void Initialize()
	{
		//数据可能从数据库取得
		interest = 0.05;
		lowerLimit = 1000.0;
		upperLimit = 10000000.0;
	}
	//存款
	override public void Deposit( double amount )
	{
		balance += amount;
		StateChangeCheck();
	}
	//取款
	override public void Withdraw( double amount )
	{
		balance -= amount;
		StateChangeCheck();
	}

	override public void PayInterest()
	{
		balance += interest * balance;
		StateChangeCheck();
	}

	override public void StateChangeCheck()
	{
		if( balance < 0.0 )//欠款
			account.State = new RedState( this );
		else if( balance < lowerLimit )//有少量存款
			account.State = new SilverState( this );
	}
}
//场景,包含有状态属性
class Account
{
	private State state;
	private string owner;
	public Account( string owner )
	{
		//新开的账户是银色状态
		this.owner = owner;
		state = new SilverState( 0.0, this );
	}
	//现存金额
	public double Balance
	{
		get{ return state.Balance; }
	}
	//状态属性
	public State State
	{
		get{ return state; }
		set{ state = value; }
	}
	//存款
	public void Deposit( double amount )
	{
		state.Deposit( amount );
		Console.WriteLine( "存入 {0:C} --- ",amount);
		Console.WriteLine( "现存金额 = {0:C}",this.Balance );
		Console.WriteLine( "状态 = {0}" ,this.State );
		Console.WriteLine( "" );
	}
	//取款
	public void Withdraw( double amount )
	{
		state.Withdraw( amount );
		Console.WriteLine( "取出 {0:C} --- ",amount);
		Console.WriteLine( "现存金额 = {0:C}",this.Balance );
		Console.WriteLine( "状态 = {0}" ,this.State );
		Console.WriteLine( "" );
	}

	public void PayInterest()
	{
		state.PayInterest();
		Console.WriteLine( "银行支付利息 --- ");
		Console.WriteLine( "现存金额 = {0:C}",this.Balance );
		Console.WriteLine( "状态 = {0}" ,this.State );
		Console.WriteLine( "" );
	}
}

/// <summary>
/// 状态模式应用测试
/// </summary>
public class StateApp
{
	public static void Main( string[] args )
	{
		//开一个新账户
		Account account = new Account( "张三" );
		//实施处理事务
		account.Deposit( 500.0 );
		account.Deposit( 300.0 );
		account.Deposit( 550.0 );
		account.PayInterest();
		account.Withdraw( 2000.00 );
		account.Withdraw( 1100.00 );
		Console.Read(); 
	}
}
