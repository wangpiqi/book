//
// NextColleague.java
//
package mediator;
class NextColleague extends Colleague
{
	private String _text;

	public NextColleague(Mediator m, String t)
	{
		super( m );
		_text = t;
	}

	public void setText(String text)
	{
		_text = text;
	}

	public String getSelection()
	{
		return( _text );
	}

	public void Display()
	{
		System.out.println("NextColleague = " + _text);
	}
}