//
// Mediator.java
//
package mediator;
import java.io.*;

abstract class Mediator
{

	public abstract void colleagueChanged(Colleague theChangedColleague);

	public static void main(String args[])
	{

		ConcreteMediator aConcreteMediator = new ConcreteMediator();
		aConcreteMediator.createConcreteMediator();

		(aConcreteMediator.getOneColleage()).Display();
		(aConcreteMediator.getNextColleague()).Display();

		OneColleague newColleague = new OneColleague(aConcreteMediator, "OneColleague");

		aConcreteMediator.colleagueChanged( (OneColleague)newColleague );

		(aConcreteMediator.getOneColleage()).Display();
		(aConcreteMediator.getNextColleague()).Display();

	}

}