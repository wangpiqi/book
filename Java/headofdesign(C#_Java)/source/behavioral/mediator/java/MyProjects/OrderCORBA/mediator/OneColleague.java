//
// OneColleague.java
//
package mediator;
class OneColleague extends Colleague
{
	private String _text;

	public OneColleague(Mediator m, String t)
	{
		super( m );
		_text = t;
	}

	public void setText(String text)
	{
		_text = text;
	}

	public String getSelection()
	{
		return(_text);
	}

	public void Display()
	{
		System.out.println("OneColleague = " + _text);
	}
}
