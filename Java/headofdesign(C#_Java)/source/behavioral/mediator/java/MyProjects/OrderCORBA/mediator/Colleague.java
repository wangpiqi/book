//
// Colleague.java
//
package mediator;
abstract class Colleague
{
	private Mediator _aMediator;

	public Colleague(Mediator m)
	{
		_aMediator = m;
	}

	public void changed()
	{
		_aMediator.colleagueChanged(this);
	}

	public Mediator getMediator()
	{
		return(_aMediator);
	}

	public abstract void Display();
}
