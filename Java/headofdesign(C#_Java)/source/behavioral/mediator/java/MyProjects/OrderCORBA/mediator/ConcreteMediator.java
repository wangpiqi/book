//
// ConcreteMediator.java
//
package mediator;
class ConcreteMediator extends Mediator
{
	private OneColleague _aOneColleague;
	private NextColleague _aNextColleague;

	public void colleagueChanged( Colleague theChangedColleague )
	{

		if ((( OneColleague)theChangedColleague).getSelection().equals( _aOneColleague.getSelection() ) )
		{
			_aNextColleague.setText( _aOneColleague.getSelection() );
		}

	}

	public void createConcreteMediator()
	{

		_aOneColleague = new OneColleague(this, "OneColleague");
		_aNextColleague = new NextColleague(this, "NextColleague");

	}

	public OneColleague getOneColleage()
	{
		return(_aOneColleague);
	}

	public NextColleague getNextColleague()
	{
		return(_aNextColleague);
	}
}