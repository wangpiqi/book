// This program has been compiled and run on Java 1.0
// To compile, type "javac Mediator.java"
// To execute, type "java Mediator"

// ------------ FILE Mediator.java -------------

import java.io.*;

abstract class Mediator
{

	public abstract void colleagueChanged(Colleague theChangedColleague);

	public static void main(String args[])
	{

		ConcreteMediator cConcreteMediator = new ConcreteMediator();
		cConcreteMediator.createConcreteMediator();

		OneColleage c1ConcreteMediator = new OneColleage(cConcreteMediator, "TheColleage");

		cConcreteMediator.colleagueChanged((OneColleage)c1ConcreteMediator );

		(cConcreteMediator.getOneColleage()).prt();
		(cConcreteMediator.getNextColleague()).prt();

	}

}

class ConcreteMediator extends Mediator
{

	public void colleagueChanged(Colleague theChangedColleague)
	{

		if (((OneColleage)theChangedColleague).getSelection().equals(_OneColleage.getSelection()) )
    {
			_NextColleague.setText(_OneColleage.getSelection() );

		}

	}

	public void createConcreteMediator()
	{

		_OneColleage = new OneColleage(this, "OneColleage");
		_NextColleague = new NextColleague(this, "NextColleague");

	}

	public OneColleage getOneColleage() {return(_OneColleage);}

	public NextColleague getNextColleague() {return(_NextColleague);}

	private OneColleage _OneColleage;
	private NextColleague _NextColleague;

}

abstract class Colleague
{

	public Colleague(Mediator m) { _mediator=m; }

	public void changed()
  {
   _mediator.colleagueChanged(this);
  }

	public Mediator getMediator() { return(_mediator); }

	public abstract void prt();

	private Mediator _mediator;

}

class OneColleage extends Colleague
{

	public OneColleage(Mediator m, String t)
	{

		super(m);
		_text=t;

	}

	public void setText(String text) { _text= text; }

	public String getSelection() { return(_text); }

	public void prt() { System.out.println("OneColleage = "+_text); }

	private String _text;

}

class NextColleague extends Colleague
{

	public NextColleague(Mediator m, String t)
	{

		super(m);
		_text=t;

	}

	public void setText(String text) { _text= text; }

	public String getSelection() { return(_text); }

	public void prt() { System.out.println("NextColleague = "+_text); }

	private String _text;

}



