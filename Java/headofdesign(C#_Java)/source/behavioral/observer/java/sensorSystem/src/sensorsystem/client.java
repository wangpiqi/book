package sensorsystem;
//监视系统应用测试
public class client {
  public static void main (String[] args) {
    SensorSystem ss = new SensorSystem ();
    ss.register (new Gates ());
    ss.register (new Lighting ());
    ss.register (new Surveillance ());
    ss.soundTheAlarm ();
  }
}