package sensorsystem;
//传感器系统,内包括具体观察者类的定义
class SensorSystem {
  private java.util.Vector listeners = new java.util.Vector ();

  public void register (AlarmListener al) {
    listeners.addElement (al);
  }

  public void soundTheAlarm () {
    System.out.println("传感器系统检测有人入侵");
    for (java.util.Enumeration e = listeners.elements (); e.hasMoreElements (); )
      ( (AlarmListener) e.nextElement ()).alarm ();
  }
}
//类继承
class Lighting implements AlarmListener {
  public void alarm () {
    System.out.println ("自动打开灯");
  }
}
//class inheritance
class Gates implements AlarmListener {
  public void alarm () {
    System.out.println ("自动关上门");
  }
}

class CheckList {
  public void byTheNumbers () {
    //Template Method design pattern
    localize ();
    isolate ();
    identify ();
  }

  protected void localize () {
    System.out.println ("建立周界");
  }

  protected void isolate () {
    System.out.println ("隔离栅格");
  }

  protected void identify () {
    System.out.println ("确认入侵源是只猫");
  }
}

//type inheritance
class Surveillance extends CheckList implements AlarmListener {
  public void alarm () {
    System.out.println ("自动监视系统启动:");
    byTheNumbers ();
  }

  protected void isolate () {
    System.out.println ("摄像枪瞄准");
  }
}


