using System;

namespace comandclient
{
class Fan {
        public void startRotate() {
                System.Console.WriteLine("Fan is rotating");
        }
        public void stopRotate() {
                System.Console.WriteLine("Fan is not rotating");
        }
}
class Light {
        public void turnOn( ) {
                System.Console.WriteLine("Light is on ");
        }
        public void turnOff( ) {
                System.Console.WriteLine("Light is off");
        }
}
class Switch {
        private Command UpCommand, DownCommand;
        public Switch( Command Up, Command Down) {
                UpCommand = Up; // concrete Command registers itself with the invoker 
                DownCommand = Down;
        }
        public void flipUp( ) { // invoker calls back concrete Command, which executes the Command on the receiver 
                        UpCommand . execute ( ) ; 


        }
        public void flipDown( ) {
                        DownCommand . execute ( );
        }
}
class LightOnCommand : Command {
        private Light myLight;
        public LightOnCommand ( Light L) {
                myLight  =  L;
        }
        public void execute( ) {
                myLight . turnOn( );
        }
}
class LightOffCommand : Command {
        private Light myLight;
        public LightOffCommand ( Light L) {
                myLight  =  L;
        }
        public void execute( ) {
                myLight . turnOff( );
        }
}
class FanOnCommand : Command {
        private Fan myFan;
        public FanOnCommand ( Fan F) {
                myFan  =  F;
        }
        public void execute( ) {
                myFan . startRotate( );
        }
}
class FanOffCommand : Command {
        private Fan myFan;

        public FanOffCommand ( Fan F) {
                myFan  =  F;
        }
        public void execute( ) {
                myFan . stopRotate( );
        }
}
              
public interface Command {
        void execute ( );
}




	class CommandClient
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			//
			// TODO: 在此处添加代码以启动应用程序
			//
            Light  testLight = new Light( );
            LightOnCommand testLOC = new LightOnCommand(testLight);
            LightOffCommand testLFC = new LightOffCommand(testLight);
            Switch testSwitch = new Switch( testLOC,testLFC);       
            testSwitch.flipUp( );
            testSwitch.flipDown( );
            Fan testFan = new Fan( );
            FanOnCommand foc = new FanOnCommand(testFan);
            FanOffCommand ffc = new FanOffCommand(testFan);
            Switch ts = new Switch( foc,ffc);
            ts.flipUp( );
            ts.flipDown( ); 
		}
	}
}
