package testcommand;
//总开关类，控制风扇和灯开关：
class Switch{
  private Command UpCommand, DownCommand;
  public Switch(Command Up, Command Down) {
    UpCommand = Up; //先注册开关的命令
    DownCommand = Down;
  }
  public void flipUp () { //调用Command接收者的方法
    UpCommand.execute ();
  }
  public void flipDown () {
    DownCommand.execute ();
  }
}