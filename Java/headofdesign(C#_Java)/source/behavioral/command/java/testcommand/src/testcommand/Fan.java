package testcommand;
//风扇类，具有开转和关转方法：
public class Fan {
  public void startRotate () {
    System.out.println ("Fan is rotating");
  }
  public void stopRotate () {
    System.out.println ("Fan is not rotating");
  }
}