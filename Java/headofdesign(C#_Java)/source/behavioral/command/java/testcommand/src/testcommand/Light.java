package testcommand;
//灯类，具有开灯及关灯两种方法
public class Light {
  public void turnOn () {
    System.out.println ("Light is on ");
  }
  public void turnOff () {
    System.out.println ("Light is off");
  }
}