package testcommand;
//先定义Command接口，具有执行的方法是本模式的特征：
public interface Command {
  public abstract void execute();
}