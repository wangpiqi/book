using System;
//抽象传递者
abstract class Approver
{
	protected string name;
	protected Approver successor;//上级
	public Approver( string name )
	{
		this.name = name;
	}
	public void SetSuccessor( Approver successor )
	{
		this.successor = successor;
	}
	abstract public void ProcessRequest(
		PurchaseRequest request );
}
//具体传递者-主任
class Director : Approver
{
	public Director ( string name ) : base( name ) {}
	override public void ProcessRequest(PurchaseRequest request )
	{
		if( request.Amount < 10000.0 ) //总额小于10000就处理
			Console.WriteLine( "{0} {1} 审批了采购请求# {2}","主任", name, request.Number);
		else//否则请他的上司来处理
			if( successor != null )
			successor.ProcessRequest( request );
	}
}
//具体传递者-副董事长
class VicePresident : Approver
{
	public VicePresident ( string name ) : base( name ) {}
	override public void ProcessRequest(
		PurchaseRequest request )
	{
		if( request.Amount < 25000.0 )//总额小于25000就处理
			Console.WriteLine( "{0} {1} 审批了采购请求# {2}","副董事长", name, request.Number);
		else//否则请他的上司来处理
			if( successor != null )
			successor.ProcessRequest( request );
	}
}
//具体传递者-董事长
class President : Approver
{
	public President ( string name ) : base( name ) {}
	override public void ProcessRequest(
		PurchaseRequest request )
	{
		if( request.Amount < 100000.0 )//总额小于100,000就处理
			Console.WriteLine( "{0} {1} 审批了采购请求# {2}",
				"董事长", name, request.Number);
		else//否则开表决会议
			Console.WriteLine( "采购请求# {0} 需要" +
				"开会来决定!", request.Number );
	}
}
//采购单
class PurchaseRequest
{
	private int number;
	private double amount;
	private string purpose;
	public PurchaseRequest( int number,  
		double amount, string purpose )
	{
		this.number = number;
		this.amount = amount;
		this.purpose = purpose;
	}
	public double Amount//总额
	{
		get{ return amount; }
		set{ amount = value; }
	}
	public string Purpose//目的
	{
		get{ return purpose; }
		set{ purpose = value; }
	}
	public int Number//总数
	{
		get{ return number; }
		set{ number = value; }
	}
}

/// <summary>
/// 职责链的应用测试
/// </summary>
public class ChainApp
{
	public static void Main( string[] args )
	{
		//创立职责链
		Director Larry = new Director( "Larry" );
		VicePresident Sam = new VicePresident( "Sam" );
		President Tammy = new President( "Tammy" );
		//Larry的上司是Sam,Sam的上司是Tammy
		Larry.SetSuccessor( Sam );
		Sam.SetSuccessor( Tammy );
		//生成并处理不同的采购请求
		PurchaseRequest rs = new PurchaseRequest(2034, 350.00, "Supplies" );
		Larry.ProcessRequest( rs );
		PurchaseRequest rx = new PurchaseRequest(2035, 32590.10, "Project X" );
		Larry.ProcessRequest( rx );
		PurchaseRequest ry = new PurchaseRequest(2036, 122100.00, "Project Y" );
		Larry.ProcessRequest( ry );
		Console.Read();//按键退出，方便查看结果。
	}
}
 

