package securitysystem;
//安全区域测验:
public class client {
  public static void main(String[] args) {
    Area a=new Area("经理室");
    Warehouse w=new Warehouse("仓库(B)");
    TemperatureSensor ts_at_corner=new TemperatureSensor("右边角");
    a.setParent(w);//区域a在仓库w中
    Building b=new Building("#2");
    w.setParent(b);//仓库w在大楼b中
    a.notify(200,ts_at_corner);//区域A传感器的测量值是200
  }
}