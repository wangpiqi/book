package securitysystem;
//具体区域,继承SecurityZone类:
public class Area extends SecurityZone {
  //调用父类的构造函数
  public Area (String string) {
    super(string);
  }
  //处理传感器的测量值
   boolean handleNotification(int measurement, Sensor sensor) {
    if (sensor instanceof TemperatureSensor) {
           if (measurement > 150) {
               fireAlarm(this,sensor);
               return true;
           }
       }
       return false;
  }
}