package bubblesorter;
//双精度型冒泡排序，其实现的算法采用父类方法doSort:
public class DoubleBubbleSorter extends BubbleSorter {
  private double[] array = null;
  public int sort (double[] theArray) {
    array = theArray;
    length = array.length;
    return doSort ();
  }

  protected void swap (int index) {
    double temp = array[ index ];
    array[ index ] = array[ index + 1 ];
    array[ index + 1 ] = temp;
  }

  protected boolean outOfOrder (int index) {
    return (array[ index ] > array[ index + 1 ]);
  }

}