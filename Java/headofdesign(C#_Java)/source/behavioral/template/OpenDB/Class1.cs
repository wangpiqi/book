using System;
using System.Data;
using System.Data.SqlClient;

// "AbstractClass"抽象类

abstract class DataObject
{
	// Methods 方法
	abstract public void Connect();
	abstract public void Select();
	abstract public void Process();
	abstract public void Disconnect();

	// The "Template Method" 模板方法
	public void Run()
	{
		Connect();
		Select();
		Process();
		Disconnect();
	}
}

// "ConcreteClass" 具体类

class CustomerDataObject : DataObject
{
	private string connectionString =
		"Server=.;User ID=sa;password=123;database=Northwind;Connection Reset=FALSE";
	private string commandString;
	private DataSet dataSet;
    private SqlConnection dataConnection;
 
	// Methods 方法
	public override void Connect( )
	{
		dataConnection=new SqlConnection(connectionString);
		dataConnection.Open();
	}

	public override void Select( )
	{
		commandString = "select top 5  CompanyName from Customers";
		SqlDataAdapter dataAdapter = new SqlDataAdapter(
			commandString,dataConnection);
		dataSet = new DataSet();
		dataAdapter.Fill( dataSet, "Customers" );
	}

	public override void Process()
	{
		DataTable dataTable = dataSet.Tables["Customers"];
		foreach( DataRow dataRow in dataTable.Rows )
			Console.WriteLine( dataRow[ "CompanyName" ] );
	}

	public override void Disconnect()
	{
		dataSet.Dispose();
		dataConnection.Close();

	}
}

/// <summary>
///  TemplateMethodApp test 模板方法例子检验
/// </summary>
public class TemplateMethodApp
{
	public static void Main( string[] args )
	{
		CustomerDataObject c = new CustomerDataObject( );
		c.Run();
		Console.Read();
	}
} 
