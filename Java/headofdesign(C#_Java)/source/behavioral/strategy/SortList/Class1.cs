using System;
using System.Collections;
//抽象排序策略类
abstract class SortStrategy
{
	abstract public void Sort( ArrayList list );
}
//具体策略-快速排序法
class QuickSort : SortStrategy 
{
	public override void Sort(ArrayList list )
	{
		list.Sort(); //ArrayList的Sort方法缺省采用快速排序法
		Console.WriteLine("采用快速排序法排序 ");
	}
}
//具体策略-外壳排序法 
class ShellSort : SortStrategy
{
	public override void Sort(ArrayList list )
	{
		//list.ShellSort();执行排序,这里省略
		Console.WriteLine("采用外壳排序法排序 ");
	}
}
// 具体策略-混合排序法
class MergeSort : SortStrategy
{
	public override void Sort( ArrayList list )
	{
		//list.MergeSort();执行排序,这里省略
		Console.WriteLine("采用混合排序法排序 ");
	}
}
// 场景
class SortedList
{
	private ArrayList list = new ArrayList();
	private SortStrategy sortstrategy;
	// 设定排序方法，传入排序策略对象
	public void SetSortStrategy( SortStrategy sortstrategy )
	{
		this.sortstrategy = sortstrategy;
	}
	//采用传入的排序策略对象排序
	public void Sort()
	{
		sortstrategy.Sort( list );
	}
	//增加要排的元素
	public void Add( string name )
	{
		list.Add( name );
	}
	public void Display()
	{
		foreach( string name in list )
			Console.WriteLine( " " + name );
	}
}
/// <summary>
/// 策略模式应用测试
/// </summary>
public class StrategyApp
{
	public static void Main( string[] args )
	{
		SortedList studentRecords = new SortedList( );
		studentRecords.Add( "Samual" );
		studentRecords.Add( "Jimmy" );
		studentRecords.Add( "Sandra" );
		studentRecords.Add( "Anna" );
		studentRecords.Add( "Vivek" );
		//采用快速排序法排序
		studentRecords.SetSortStrategy( new QuickSort() );
		studentRecords.Sort();
		studentRecords.Display();
		Console.Read();
	}
} 