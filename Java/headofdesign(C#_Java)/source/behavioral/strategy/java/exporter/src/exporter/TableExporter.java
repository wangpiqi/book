package exporter;

abstract class TableExporter {
  abstract String getExported(String[][] data);
}