using System;
using System.Collections;
namespace MultiPage
{
    //产品基类
	abstract class Page
	{
	}
	//工厂抽象类
	abstract class Document
	{
		protected ArrayList pages = new ArrayList();
		public Document()
		{
			this.CreatePages();
		}
		public ArrayList Pages
		{
			get{ return pages; }
		}
		// Factory Method
		abstract public void CreatePages();
	}
	//派生类具体产品类-技能页
	class SkillsPage : Page
	{
	}
	//具体产品类-教育页

	class EducationPage : Page
	{
	}
	//具体产品类-经验页
	class ExperiencePage : Page
	{
	}
	// 具体产品类-介绍页
	class IntroductionPage : Page
	{
	}
	//具体产品类-结果页
	class ResultsPage : Page
	{
	}
	//具体产品类-结论页
	class ConclusionPage : Page
	{
	}
	//具体产品类-总结页
	class SummaryPage : Page
	{
	}
	//具体产品类-文献页
	class BibliographyPage : Page
	{
	}

	//具体工厂类-个人简历，包括技能,教育,经验
	class Resume : Document
	{
		//Factory Method实现
		override public void CreatePages()
		{
			pages.Add( new SkillsPage() );
			pages.Add( new EducationPage() );
			pages.Add( new ExperiencePage() );
		}
	}

	//具体工厂类-报告，包括介绍,结果,结论,总结,文献
	class Report : Document
	{
		//Factory Method实现
		override public void CreatePages()
		{
			pages.Add( new IntroductionPage() );
			pages.Add( new ResultsPage() );
			pages.Add( new ConclusionPage() );
			pages.Add( new SummaryPage() );
			pages.Add( new BibliographyPage() );
		}
	}
	/// <summary>
	/// 客户应用测试
	/// </summary>
	/// 
	class Client
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Document[] docs = new Document[ 2 ];
			//在构造函数就调用了Factory Method,创建两种文档
			docs[0] = new Resume();
			docs[1] = new Report();
			//显示文档的内容
			foreach( Document document in docs )
			{
				Console.WriteLine( "\n" + document + " ------- " );
				foreach( Page page in document.Pages )
					Console.WriteLine( " " + page );
			}
			Console.Read();
		}
	}
}
