using System;

namespace CarShop
{
	using System;
	using System.Collections;
	//指挥者
	class Shop{
		public void Construct( VehicleBuilder vehicleBuilder ){
            vehicleBuilder.BuildFrame();
			vehicleBuilder.BuildEngine();
			vehicleBuilder.BuildWheels();
			vehicleBuilder.BuildDoors();
		}
	}
	/* "Builder 建造者"
	抽象建造者具有四种方法
	装配框架
	装配发动机
	装配轮子
	装配车门
	*/
	abstract class VehicleBuilder
	{
		protected Vehicle vehicle;
		//返回建造的车辆属性
		public Vehicle Vehicle{
            get{ return vehicle; }
		}
		abstract public void BuildFrame();
		abstract public void BuildEngine();
		abstract public void BuildWheels();
		abstract public void BuildDoors();
	}
	//具体建造者-摩托车车间
	class MotorCycleBuilder : VehicleBuilder
	{
		override public void BuildFrame(){
			vehicle = new Vehicle( "摩托车" );
			vehicle[ "frame" ] = "MotorCycle Frame";
		}

		override public void BuildEngine(){
			vehicle[ "engine" ] = "500 cc";
		}

		override public void BuildWheels(){
			vehicle[ "wheels" ] = "2";
		}

		override public void BuildDoors(){
			vehicle[ "doors" ] = "0";
		}
	}
	//具体建造者-轿车车间
	class CarBuilder : VehicleBuilder
	{
		override public void BuildFrame(){
			vehicle = new Vehicle( "轿车" );
			vehicle[ "frame" ] = "Car Frame";
		}
		override public void BuildEngine(){
			vehicle[ "engine" ] = "2500 cc";
		}
		override public void BuildWheels(){
			vehicle[ "wheels" ] = "4";
		}
		override public void BuildDoors(){
			vehicle[ "doors" ] = "4";
		}
	}
	// 具体建造者-单脚滑行车车间
	class ScooterBuilder : VehicleBuilder
	{
		override public void BuildFrame(){
			vehicle = new Vehicle( "单脚滑行车" );
			vehicle[ "frame" ] = "Scooter Frame";
		}

		override public void BuildEngine(){
			vehicle[ "engine" ] = "none";
		}

		override public void BuildWheels(){
			vehicle[ "wheels" ] = "2";
		}

		override public void BuildDoors(){
			vehicle[ "doors" ] = "0";
		}
	}
	//车辆产品类
	class Vehicle
	{
		private string type;
		private Hashtable parts = new Hashtable();
		//筑构函数,决定类型
		public Vehicle( string type ){
			this.type = type;
		}
		//索引
		public object this[ string key ]{
			get{ return parts[ key ]; }
			set{ parts[ key ] = value; }
		}
		//显示方法
		public void Show()
		{
			Console.WriteLine( "\n---------------------------");
			Console.WriteLine( "车辆类类型: "+ type );
			Console.WriteLine( " 框架 : " + parts[ "frame" ] );
			Console.WriteLine( " 发动机 : "+ parts[ "engine"] );
			Console.WriteLine( " #轮子数: "+ parts[ "wheels"] );
			Console.WriteLine( " #车门数 : "+ parts[ "doors" ] );
		}
	}
	/// <summary>
	/// 建造者模式应用测试
	/// </summary>
 	class CarShop
	{
		[STAThread]
		static void Main(string[] args)
		{
			// 创造车间及车辆建造者
			Shop shop = new Shop();
			VehicleBuilder b1 = new ScooterBuilder();
			VehicleBuilder b2 = new CarBuilder();
			VehicleBuilder b3 = new MotorCycleBuilder();
			// 筑构并显示车辆
			shop.Construct( b1 );
			b1.Vehicle.Show();
			shop.Construct( b2 );
			b2.Vehicle.Show();
			shop.Construct( b3 );
			b3.Vehicle.Show();
			Console.Read();

		}
	}
}
