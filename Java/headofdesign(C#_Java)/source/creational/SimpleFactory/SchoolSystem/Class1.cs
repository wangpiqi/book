using System;
namespace SchoolSystem
{
	//Base Class 基类
	public class SchoolUser
	{
		public string FName;
		public string LName;
		public string UserType;
		public void Show()
		{
			Console .Write("First Name: "+FName+"\n");
			Console .Write("Last Name: "+LName+"\n");
			Console .Write("User Type: "+UserType+"\n");
		}
	}
	///Derived Classes派生类 校长类
	public class SchoolPrincipal : SchoolUser
	{
		public SchoolPrincipal()
		{

			FName="David";
			LName="Smith";
			UserType="Principal";
		} 		
	}	
	//派生类 校教师
	public class SchoolTeacher: SchoolUser
	{
		public SchoolTeacher()
		{
			FName="Patrecia";
			LName="Terry";
			UserType="Teacher";
		}
	}
	public class SimpleFactory
	{
		public SchoolUser GetSchoolUser(string user,string password)
		{
			if(user=="Principal"&&password=="Principal")
				return new SchoolPrincipal();
			if(user=="Teacher"&&password=="Teacher")
				return new SchoolTeacher();
			return null;
		}
	}
	//客户应用
	class Client
	{
		[STAThread]
		static void Main(string[] args)
		{
			SimpleFactory sf=new SimpleFactory ();
			SchoolUser su;
			su=sf.GetSchoolUser("Principal","Principal");
			Console.Write("-------------校长登录---------\n");
			su.Show();
			Console.Write("-------------教师登录-----------\n");
			su=sf.GetSchoolUser("Teacher","Teacher");
			su.Show ();
			Console.Read(); //读取按键才结束程序，方便查看结果
		}
	}
}
