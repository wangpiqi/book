using System;
namespace EFT
{
	//抽象电子付款类
	abstract class EFT
	{
	    public abstract void process();				
	}
	//具体子类，虚拟检查
	class VirtualCheck:EFT
	{
		public override void process()
		{
            Console.WriteLine("虚拟支票处理中");
		}
	}
    //具体子类,万事达卡
	class MasterCard:EFT
	{
		public override void process()
		{
			Console.WriteLine("万事达卡处理中");
		}
	}
	//简单工厂类
	class EFTFactory
	{
		public EFT createEFT(string type)
		{
			switch(type.ToLower())
			{
				case "virtualcheck":
					return new VirtualCheck();
				case "mastercard":
					return new MasterCard();
				default:
					return null;
			}
		}
	}
	//客户应用测试
	class Client
	{
		[STAThread]
		static void Main(string[] args)
		{
			EFT eft;
			EFTFactory eftFactory=new EFTFactory();
			eft=eftFactory.createEFT("VirtualCheck");
			eft.process();
			eft=eftFactory.createEFT("mastercard");
			eft.process();
			Console.Read();
		}
	}
}
