namespace LoadBalance
{
	using System;
	using System.Collections;
	using System.Threading;
	class LoadBalancer
	{
		private static LoadBalancer balancer;
		private ArrayList servers = new ArrayList();
		private Random random = new Random();
		// Constructors 构筑函数(protected)
		protected LoadBalancer()
		{
			// List of available servers 列出所有服务器
			servers.Add( "ServerI" );
			servers.Add( "ServerII" );
			servers.Add( "ServerIII" );
			servers.Add( "ServerIV" );
			servers.Add( "ServerV" );
		}
		public static LoadBalancer GetLoadBalancer()
		{
			// Support multithreaded applications through
			// "Double checked locking" pattern which avoids
			// locking every time the method is invoked
			//通过"双检锁"模式支持多线程的客户
			if( balancer == null )
			{
				// Only one thread can obtain a mutex
				//只有一个线程可以取得mutex。
				Mutex mutex = new Mutex();
				mutex.WaitOne();
				if( balancer == null )
					balancer = new LoadBalancer();
				mutex.Close();
			}
			return balancer;
		}
		// Properties 属性
		public string Server
		{
			get
			{
				//简单有效的随机load balancer
				int r = random.Next( servers.Count );
				return servers[ r ].ToString();
			}
		}
	}
 	class Client
	{
		[STAThread]
		static void Main(string[] args)
		{
			LoadBalancer b1 = LoadBalancer.GetLoadBalancer();
			LoadBalancer b2 = LoadBalancer.GetLoadBalancer();
			LoadBalancer b3 = LoadBalancer.GetLoadBalancer();
			LoadBalancer b4 = LoadBalancer.GetLoadBalancer();
			// 是否相同
			if( (b1 == b2) && (b2 == b3) && (b3 == b4) )
				Console.WriteLine( "Same instance" );
			// 实现负载均衡
			Console.WriteLine( b1.Server );
			Console.WriteLine( b2.Server );
			Console.WriteLine( b3.Server );
			Console.WriteLine( b4.Server );
			//按键才退出，方便查看结果
			Console.Read();
		}
	}
}

public sealed class Singleton
{
	private static volatile Singleton instance;
	private static object syncRoot = new Object();

	private Singleton() {}

	public static Singleton Instance
	{
		get 
		{
			if (instance == null) 
			{
				lock (syncRoot) 
				{
					if (instance == null) 
						instance = new Singleton();
				}
			}

			return instance;
		}
	}
}

