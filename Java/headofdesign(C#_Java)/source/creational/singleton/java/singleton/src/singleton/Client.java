package singleton;

public class Client {
  public  static void  main(String argv[])
  {
    //初始化Logger
    Logger.initialize();
    //取得实例
    Logger loger=Logger.getLogger();
    loger.logMsg("client log message");
  }
}