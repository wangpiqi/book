package computerworld;
//抽象工厂类派生类Dell，定义其返回的系列配件产品:
public class Dell extends ComputerFactory {
  Dell()
  {
    cpu=new AMD();
    hd=new Maxtor();
    mb=new MSIK7N2G();
  }
}