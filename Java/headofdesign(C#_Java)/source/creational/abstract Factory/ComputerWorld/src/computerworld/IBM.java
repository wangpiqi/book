package computerworld;

//抽象工厂类派生类IBM，定义其返回的系列配件产品
public class IBM extends ComputerFactory {
  IBM(){
    cpu=new Intel();
    hd=new WestDigit();
    mb=new MSI865PE();
    }
}