package computerworld;

public interface HardDisk {
  public String getSize();
}