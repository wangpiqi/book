package computerworld;
//定义抽象电脑工厂类
public abstract class ComputerFactory {
  CPU cpu;
  HardDisk hd;
  MainBoard mb;
  public void show () {
    try {
      System.out.println (this.getClass ().getName ().toString () + "生产的电脑配置");
      System.out.println ("CPU:" + cpu.getCPU ());
      System.out.println ("HardDisk:" + hd.getSize ());
      System.out.print ("MainBoard:");
      mb.Attach (cpu);
    }
    catch (Exception e) {
      System.err.println (e.getMessage ());
    }
  }
}