package computerworld;

public interface MainBoard {
  public void Attach(CPU cpu) throws Exception;
}