package shallowvsdeep;

public class Lay1 implements Cloneable {
  public int x;
  public Lay2 lay2;
  public Object clone() {
    Object clone = null;
    try {
      clone = super.clone ();
    }
    catch (CloneNotSupportedException e) {
      // should never happen
    }
    ((Lay1)clone).lay2=(Lay2)this.lay2.clone();
    return clone;
  }
}