package shallowvsdeep;
public class Client {
  public static  void main(String argv[])
  {
    Lay1 obj1=new Lay1();
    obj1.lay2=new Lay2();
    obj1.x=1;
    obj1.lay2.y=1;
    Lay1 obj2=(Lay1)obj1.clone();
    obj2.x=2;
    obj2.lay2.y=2;
    System.out.println("obj1.x is:"+obj1.x+"\tObj1.lay2.y is:"+obj1.lay2.y);
    System.out.println("obj2.x is:"+obj2.x+"\tObj2.lay2.y is:"+obj2.lay2.y);
  }
}