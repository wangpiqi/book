package prototype;
import java.util.*;
public class Toolbar {
  public HashMap objPool;
  Toolbar()
  {
    objPool=new HashMap();
    this.Register("circle",new Circle());
    this.Register("rectangle",new Rectangle());
  }

  public void Register(Object key,Object object)
  {
    objPool.put(key,object);
  }
  public Object getClone(String key)
  {
    return  objPool.get(key);
  }
}
