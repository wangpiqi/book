package prototype;

public class Client {
  public static void main(String argv[])
  {
    Toolbar toolbar=new Toolbar();
    String key="circle";
    Circle c1=(Circle)toolbar.getClone(key);
    c1.Draw();
    key="rectangle";
    Rectangle r1=(Rectangle)toolbar.getClone(key);
    r1.Draw();
  }
}