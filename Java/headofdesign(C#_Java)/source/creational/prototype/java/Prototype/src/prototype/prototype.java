package prototype;

public interface prototype extends Cloneable {
  public Object Clone();
  public String GetName();
}
