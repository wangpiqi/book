package prototype;

public interface Command {
  public void Draw();
}