package prototype;

public class Circle implements prototype,Command {
  public Object Clone() {
    Object clone=null;
    try{
      clone=super.clone();
    }
    catch(CloneNotSupportedException exception)
    {
      System.err.println("Clone not support");
    }
    return clone;
  }
  public String GetName() {
    return "Circle";
  }
  public void Draw()
  {
    System.out.println("Draw a circle");
  }
}
