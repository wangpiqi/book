using System;
namespace ShallowVSDeep
{
	class Client
	{
		class Lay1
		{
			public int x;
			public Lay2 lay2;
			public Lay1 Clone()
			{
				return (Lay1)this.MemberwiseClone();
			}
		} 
		class Lay2
		{
			public int y;
			public Lay2 Copy()
			{
				return this.MemberwiseClone() as Lay2;
			}
		}
		[STAThread]
		static void Main(string[] args)
		{
			Lay1 Obj1=new Lay1();
			Obj1.x=1;
			Obj1.lay2=new Lay2();
			Obj1.lay2.y=1;
			//Obj2为Obj1的浅表复制对象
			Lay1 Obj2=Obj1.Clone();
			Obj2.x=2;
			Obj2.lay2.y=2;
			Console.WriteLine("Obj1.x is:{0}\tObj1.lay2.y={1}",Obj1.x,Obj1.lay2.y);
			Console.WriteLine("Obj2.x is:{0}\tObj2.lay2.y={1}",Obj2.x,Obj2.lay2.y);
			Console.Read();
		}
	}
}

