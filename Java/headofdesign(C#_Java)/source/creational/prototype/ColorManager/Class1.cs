using System;
	using System.Collections;
namespace ColorManager
{
	// "定义抽象原形 Prototype"
	abstract class ColorPrototype{
		// Clone Methods 克隆方法
		public abstract ColorPrototype Clone();
	}
	// "ConcretePrototype 具体原型"
	class Color : ColorPrototype{
		private int red, green, blue;
		// Constructors 筑构函数
		public Color( int red, int green, int blue){
			this.red = red;
			this.green = green;
			this.blue = blue;
		}
		// Overrrid Clone Methods 
		public override ColorPrototype Clone(){
			// Creates a 'shallow copy'创造一个浅拷贝
			return (ColorPrototype) this.MemberwiseClone();
		}
		public void Display(){
			Console.WriteLine( "RGB values are: {0},{1},{2}",
				red, green, blue );
		}
	}
	// Prototype manager 原型管理者
	class ColorManager
	{
		Hashtable colors = new Hashtable();
		// Indexers
		public ColorPrototype this[ string name ]{
			get{ return (ColorPrototype)colors[ name ]; }
			set{ colors.Add( name, value ); }
		}
	} 

	class Client
	{
		[STAThread]
		static void Main(string[] args)
		{
			ColorManager colormanager = new ColorManager();
			// 初始化标准的颜色
			colormanager[ "red" ] = new Color( 255, 0, 0 );
			colormanager[ "green" ] = new Color( 0, 255, 0 );
			colormanager[ "blue" ] = new Color( 0, 0, 255 );
			// 增加个性化的颜色
			colormanager[ "angry" ] = new Color( 255, 54, 0 );
			colormanager[ "peace" ] = new Color( 128, 211, 128 );
			colormanager[ "flame" ] = new Color( 211, 34, 20 );
			// 用户选择了定义的颜色
			string colorName = "red";
			Color c1 = (Color)colormanager[ colorName ].Clone();
			c1.Display();

			colorName = "peace";
			Color c2 = (Color)colormanager[ colorName ].Clone();
			c2.Display();

			colorName = "flame";
			Color c3 = (Color)colormanager[ colorName ].Clone();
			c3.Display();
			Console.Read();
		}
	}
}
