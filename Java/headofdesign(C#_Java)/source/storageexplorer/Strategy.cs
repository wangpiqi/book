using System;
using System.IO;
using System.Collections;
using System.Windows.Forms;


namespace StorageExplorer 
{

	//事件自变量，由ExplorationFinishEventHandler句柄所用
	public class ExplorationFinishEventArgs : EventArgs
	{
		public Hashtable ExplorationResult;
	}

	// 事件自变量，由ProgressEventHandler句柄所用
	public class ExplorationProgressEventArgs : EventArgs
	{
		public string Path;
	}

	// 注册完成事件
	public delegate void ExplorationFinishEventHandler (object sender, ExplorationFinishEventArgs e);

	// 注册处理事件
	public delegate void ExplorationProgressEventHandler (object sender, ExplorationProgressEventArgs e);

	/* 本抽象类是以下具体策略模式的基础类:
	 * 1. FolderStrategy (取得目录下的子目录的文件大小)
	 * 2. FileTypeStrategy (取得目录下文件类型的文件大小)
	 */
	public abstract class ExplorationStrategy
	{
		//保持勘探的结果，(哈希)表的键表示文件名-大小，值表示文件类型-大小
		// For holding the exploration result. The key-value pair 
		// can contain information: FolderName-Size or FileType-Size
		protected Hashtable ExplorationResult;

		// 当勘探完了，激发事件Event raised when an exploration has finished
		public event ExplorationFinishEventHandler Finish;

		// 勘探过程中激发事件Event raised during the exploration
		public event ExplorationProgressEventHandler Progess;

		public ExplorationStrategy()
		{
			ExplorationResult = new Hashtable();
		}

		// 勘探过程结束后，调用OnFinish()方法
		// For executing the exploration process. When an exploration finishes
		// the method calls OnFinish()
		public virtual void Explore (string path){}

		//这个方法激发完成事件 Method for raising Finish event
		protected void OnFinish (object sender)
		{
			if (Finish != null)
			{
				ExplorationFinishEventArgs args = new ExplorationFinishEventArgs();
				args.ExplorationResult = ExplorationResult;
				Finish (sender, args);
			}
		}

		//这个方法激发勘探过程事件
		// Method for raising Progress event
		protected void OnProgress (object sender, string path)
		{
			if (Progess != null)
			{
				ExplorationProgressEventArgs args = new ExplorationProgressEventArgs();
				args.Path = path;
				Progess (sender, args);
			}
		}
	}

	// 目录策略类FolderStrategy (取得目录下的子目录的文件大小)
	/* This class retrieves folders size under a given path then fill
	 * ExplorationResult hastable containing folder names 
	 * and the size of the folders.
	 */
	public class FolderStrategy : ExplorationStrategy
	{
		private Hashtable folderSizeCache; //定义存取结果的哈希表

		public FolderStrategy() 
		{ 
			folderSizeCache = new Hashtable(); //构造函数就初始化哈希表
		}

		//此方法勘探所有子目录的文件，以文件夹为单位计算出总文件大小。
		/* The method to explore all files and folder under a given path while 
		 * summing the file size and group the result based on the folder name
		 */
		public override void Explore (string pPath)
		{
			long dirSize = 0;

			ExplorationResult.Clear();
			try 
			{				
				//循环指定目录下的所有子文件夹Loop to explore all the directories under pPath
				foreach (string dirName in Directory.GetDirectories(pPath))
				{
					//如果大小信息已经存在于缓存中，不用再勘探
					// If the size information is already on the cache just 
					// retrieve it from the cache, do not access the file system
					if (folderSizeCache.ContainsKey(dirName)) 
					{
						dirSize = (long) folderSizeCache[dirName];
					} 
					else  //缓存中不存在
					{
						try
						{
							//访问文件系统，取得目录大小
							// Access the file system to get the directory size
							DirectoryInfo dirInfo = new DirectoryInfo (dirName);
							dirSize = GetDirectorySize (dirInfo);
							
							//将结果保存到缓存哈希表,以后不用重新访问文件系统 Add the size to the cache so the subsequent call
							// doesn't need file access anymore
							folderSizeCache.Add (dirName, dirSize);
						}

						//获取异常情况，有可能没有访问目录的权限 Catch any exception if a folder cannot be accessed
						// e.g. due to security restriction
						catch (Exception) {}
					}

					//将结果保存到结果哈希表 Add the result to the hashtable. This is the information
					// that is going to be displayed
					ExplorationResult.Add (Path.GetFileName (dirName), dirSize);
				}

				const string CURRENT_FOLDER_KEY = "(Current Directory)";

				//如果缓存中现目录大小的数据，就直接取得 Get the current directory size if it is already in cache 
				if (folderSizeCache.ContainsKey(CURRENT_FOLDER_KEY)) 
					dirSize = (long) folderSizeCache[CURRENT_FOLDER_KEY];
				else
				{
					//计算当前目录的文件大小 Sum the file size under the current directory
					dirSize = 0;
					foreach (string fileName in Directory.GetFiles(pPath))
					{
						try
						{
							FileInfo fileInfo = new FileInfo(fileName);
							dirSize += fileInfo.Length;
						}

							//处理文件没有不能访问的异常（可能受安全设定限制）Catch any exception if a file cannot be accessed
							// e.g. due to security restriction
						catch (Exception) {}
					}

					//当值存入哈希表 Add the result again to the hashtable
					ExplorationResult.Add (CURRENT_FOLDER_KEY, dirSize);
				}
			}

			////处理文件没有不能访问的异常（可能受安全设定限制）
			// Catch any exception if a folder cannot be accessed
			// e.g. due to security restriction
			catch (Exception) {}

			//知会事件订阅者，勘探结束Notify all the subscribers that the exploration has finished
			OnFinish (this);
		}

		//一个递归方法计算文件夹的大小 A recursive method to calculate directory size 
		private long GetDirectorySize(DirectoryInfo pDirInfo) 
		{    
			long dirSize = 0;    
			
			//合计当天目录的文件大小 Sum file size in the current folder
			FileInfo[] fileInfos = pDirInfo.GetFiles();
			foreach (FileInfo fileInfo in fileInfos) 
			{
				try 
				{
					dirSize += fileInfo.Length;
				}
				
				//处理文件没有不能访问的异常（可能受安全设定限制）
				//Catch any exception if a file cannot be accessed
				// e.g. due to security restriction
				catch (Exception) {}
			}

			//合计子目录的大小 Sum subdirectory size
			DirectoryInfo[] dirInfos = pDirInfo.GetDirectories();
			foreach (DirectoryInfo dirInfo in dirInfos) 
			{
				try 
				{
					OnProgress (this, dirInfo.FullName);
					dirSize += GetDirectorySize(dirInfo); 
				}
				//处理文件没有不能访问的异常（可能受安全设定限制）
				//Catch any exception if a folder cannot be accessed
				// e.g. due to security restriction
				catch (Exception) {}
			}
			return dirSize;  
		}
	}



	//文件类型策略类，取得所分析目录的所有类刑的文件大小
	//将结果数据的文件类型及大小存入ExplorationResult哈希表中
	/* This class retrieves total file size under a given path then fill
	 * ExplorationResult hastable containing file type and the total size 
	 * of each file type.
	 */
	public class FileTypeStrategy : ExplorationStrategy
	{
		private Hashtable typeSizeCache;

		public FileTypeStrategy() 
		{ 
			typeSizeCache = new Hashtable(); 
		}

		//这个方法开始勘探 The method to start the explorration
		public override void Explore (string pPath)
		{
			ExplorationResult.Clear();

			if (!typeSizeCache.Contains(pPath))
			{
				// 如果缓存中没有此文件的信息，就可以勘探
				//If the current folder information is not cached then
				// do the exploration
				GetFileTypeSize (new DirectoryInfo (pPath));
				//将结果存入缓存
				typeSizeCache.Add (pPath, (Hashtable) ExplorationResult.Clone());
			}
			else
				//取缓存中取得信息 Otherwise just retrieve the result from the cache
				ExplorationResult = (Hashtable) 
									((Hashtable) typeSizeCache[pPath]).Clone();

			//知会订阅者勘探处理完成Notify all the subscribers that the exploration has finished
			OnFinish (this);
		}

		//勘探文件并且合计相同类型的文件大小
		// Explore the file and sum the size of files that have a same extension,
		// i.e. sum .doc with .doc, .exe with .exe, and so on
		private void GetFileTypeSize (DirectoryInfo d)
		{   
			try 
			{
				FileInfo[] fileInfos = d.GetFiles();
				foreach (FileInfo fileInfo in fileInfos) 
				{
					try 
					{
						string fileExt = "*" + fileInfo.Extension.ToLower();

						//增加相同扩展名的文件大小 Add the file size with the same extension
						if (!ExplorationResult.ContainsKey(fileExt))
						{
							ExplorationResult.Add (fileExt, (long) 0);
						}
						else
						{
							ExplorationResult[fileExt] = (long) ExplorationResult[fileExt] 
								+ fileInfo.Length;
						}
					}
					// 处理文件没有不能访问的异常（可能受安全设定限制）
					//Catch any exception if a file cannot be accessed
					// e.g. due to security restriction
					catch (Exception) {}
				}

				//同样处理子目录 Do the same process to the subdirectories
				DirectoryInfo[] dirInfos = d.GetDirectories();
				foreach (DirectoryInfo dirInfo in dirInfos) 
				{
					OnProgress (this, dirInfo.FullName);
					GetFileTypeSize(dirInfo); 
				}
			}
			//处理文件没有不能访问的异常（可能受安全设定限制）
			//Catch any exception if a folder/file cannot be accessed
			// e.g. due to security restriction
			catch (Exception) {}
		}
	}
}
