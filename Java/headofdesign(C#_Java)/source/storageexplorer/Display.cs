using System;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;

namespace StorageExplorer
{
	//勘探观察者抽象类

	public abstract class ExplorationObserver
	{

		public ExplorationObserver() {}

		//订阅的实例完成事件，使得对象在激发事件取得知会
		// Method to subscribe the class instance to the Finish event
		// so that the object can be notified when the event is raised
		public void SubscribeToExplorationEvent (ExplorationStrategy obj)
		{
			obj.Finish += new ExplorationFinishEventHandler (UpdateDisplay);
		}

		//抽象方法类，输出哈希表数据，用于显示，显示的格式由子类决定。
		// Method to output the hashtable data to a display.
		// The format of the output is specified by the subclass.
		public abstract void UpdateDisplay (object sender, 
											ExplorationFinishEventArgs e);

		//将显示结果以一定格式加入容器的虚拟函数
		// Method to add the display to the container
		public virtual void AddToContainer (Control container) {} 
	}

	public class ListViewAdapter : ExplorationObserver
	{
		//控制显示列表式的勘探结果
		// Control to display the exploration result as a list.
		protected ListView listView;

		public ListViewAdapter()
		{
			// 创建一个ListView对象并初始化它
			// Create a ListView object and initialze it
			listView = new ListView();
			listView.Dock = DockStyle.Fill;
			listView.View = View.Details;
			listView.Columns.Clear();
			listView.Columns.Add ("Name", 150, HorizontalAlignment.Left);
			listView.Columns.Add ("Size", 100, HorizontalAlignment.Right);
			listView.Columns.Add ("Percentage", 70, HorizontalAlignment.Right);
			listView.ColumnClick += new ColumnClickEventHandler
											(ListViewColumn_Click);
		}

		//模板类的方法,将结果呈递到ListView的过程。不同于模板的方法是 SelectImageList()方法和setLineIcon()方法
		/* This is the template method as specified in Template Method
		 * design patterns. It is used to render the exploration result 
		 * in the ListView. The primitive methods that varies the template
		 * method are the SelectImageList() and the SetLineIcon().
		 */ 
		public override void UpdateDisplay (object sender, 
											ExplorationFinishEventArgs e)
		{
			listView.BeginUpdate();
			listView.Items.Clear();

			//取得集合collection中的列举enumerator
			// Get the enumerator to iterate the collection
			IDictionaryEnumerator listEnumerator = 
							e.ExplorationResult.GetEnumerator();

			//合计所有的文件大小
			// Sum all the files size. The total will be used 
			// to calculate the files size in percent
			long totalSize = 0;
			while (listEnumerator.MoveNext())
				totalSize += (long) listEnumerator.Value;

			//循环加入ListView中去
			// Loop to fill ListView with ListViewItem
			ListViewItem listItem;
			listEnumerator.Reset();

			while (listEnumerator.MoveNext())
			{
				string itemName = listEnumerator.Key.ToString();
				listItem = new ListViewItem (itemName);

				//设计单项的图标
				// Set item icon. This operation needs to be overriden
				SetIcon (listItem);

				//以千字节计算大小
				// The file size in kilobytes
				string sizeInfo = ((long) listEnumerator.Value/1024).ToString 
													("#,##0") + " KB";
				listItem.SubItems.Add (sizeInfo);

				//计算并且显示占大小的百分比
				// Calculate & display the size in percentage
				float percent = (totalSize>0)? 
					(long) listEnumerator.Value * 100 / (float) totalSize : 0;
				listItem.SubItems.Add (percent.ToString("#,##0.00") + " %");
				//将ListViewItem单项加入listView中
				// Add the ListViewItem to the listView
				listView.Items.Add(listItem);
			}

			listView.EndUpdate();
		}

		protected virtual void SetIcon (ListViewItem item) {}

		//在ListView开始排序事件句柄
		// Event handler to start sorting the information in the listView
		private void ListViewColumn_Click(object o, ColumnClickEventArgs e)
		{
			listView.Sorting = (listView.Sorting == SortOrder.Descending) ?
									SortOrder.Ascending : SortOrder.Descending;
            //排序点击的列
			// Sort the clicked column using ListViewItemComparer implementation.
			listView.ListViewItemSorter = new ListViewItemComparer(e.Column);
			listView.Sort();
		}

		//将listView加入容器中
		// Add the listView to the container
		public override void AddToContainer(Control container) 
		{
			container.Controls.Add (listView);
		}
	}
	// 在ListView中显示目录大小
	// The class for showing folder size information in the ListView
	public class FolderListView : ListViewAdapter
	{
		public FolderListView()
		{
			ImageList imageList = new ImageList();
			imageList.Images.Add (IconReader.Instance.GetClosedFolderIcon());
			listView.SmallImageList = imageList;
		}

		protected override void SetIcon (ListViewItem item)
		{
			item.ImageIndex = 0;
		}
	}

	//在ListView中显示文件类型的大小
	// The class for showing file-type size information in the ListView
	public class FileTypeListView : ListViewAdapter
	{
		public FileTypeListView ()
		{
			listView.SmallImageList = FileIcons.Instance.SmallImageList;
		}

		protected override void SetIcon (ListViewItem item)
		{
			item.ImageIndex = FileIcons.Instance.GetIconIndex (item.Text);
		}
	}

	// ListView排序处理过程
	// The class to be used by ListView sorting process
	sealed class ListViewItemComparer : IComparer 
	{
		private int col;
		
		public ListViewItemComparer() {col=0;}
		public ListViewItemComparer(int column) {col=column;}

		public int Compare(object x, object y) 
		{
			string s;
			long v1, v2;
			int result = 0 ;
			SortOrder sorting = ((ListViewItem)x).ListView.Sorting;

			switch (col)
			{
				//如果第一列选中，采用字符串比较
				// If colum 0 is clicked, do string comparison
				case 0:
					result = (sorting == SortOrder.Ascending )?
						String.Compare
								(((ListViewItem)x).SubItems[col].Text, 
								((ListViewItem)y).SubItems[col].Text) :
						String.Compare
								(((ListViewItem)y).SubItems[col].Text, 
								((ListViewItem)x).SubItems[col].Text);
					break;

				//如果第二列选中，采用整数比较
				// If colum 1 is clicked, do integer comparison
				// However we need to parse the string before comparing
				case 1:
					s = ((ListViewItem)x).SubItems[col]
						.Text.Trim("KB".ToCharArray());
					s = s.Replace(",", "");
					try {v1 = Int64.Parse(s);} 
					catch (Exception) {v1=0;};

					s = ((ListViewItem)y).SubItems[col].Text.Trim
						("KB".ToCharArray());
					s = s.Replace(",", "");
					try {v2 = Int64.Parse(s);} 
					catch (Exception) {v2=0;};

					result = (sorting == SortOrder.Ascending )? 
						Decimal.Compare(v1, v2) : Decimal.Compare(v2, v1);
					break;
				//如果第三列选中，采用字符串比较，比较前要先转换
				// If colum 2 is clicked, do string comparison
				// However we need to parse the string before comparing
				case 2:
					s = ((ListViewItem)x).SubItems[col].Text.Trim
							("%".ToCharArray());
					s = s.Replace(".", "");
					try {v1 = Int64.Parse(s);} 
					catch (Exception) {v1=0;};

					s = ((ListViewItem)y).SubItems[col].Text.Trim
							("%".ToCharArray());
					s = s.Replace(".", "");
					try {v2 = Int64.Parse(s);} 
					catch (Exception) {v2=0;};

					result = (sorting == SortOrder.Ascending )? 
						Decimal.Compare(v1, v2) : Decimal.Compare(v2, v1);
					break;
			}
			return result;
		}
	}
	//此馅饼图表适配器类提供了在馅饼图表类(PieChart)和勘探观察者(ExplorationObserver)两个类中提供了适配作用

	/* This clas provides interface between concrete PieChart class
	 * and the ExplorationObserver class. The assumption is we don't
	 * want to modify the existing PieChart class. 
	 */
	public class PieChartAdapter : ExplorationObserver
	{
		private Chart pieChart;

		public PieChartAdapter ()
		{
			//创建馅饼图表 Create a piechart
			pieChart = new PieChart ();
			pieChart.IsStretch = true;
		}

		///在容器里增加馅饼图表 Add the pieChart to the container
		public override void AddToContainer(Control container) 
		{
			container.Controls.Add (pieChart);
		}

		public override void UpdateDisplay (object sender, 
											ExplorationFinishEventArgs e)
		{
			//使用结果来画图 Draw the chart using the given data
			pieChart.Data = e.ExplorationResult;
			pieChart.Draw();
		}
	}
	//棒条图表适配器类提供了在棒条图表类(barChart)和勘探观察者(ExplorationObserver)两个类中提供了适配作用

	public class BarChartAdapter : ExplorationObserver
	{
		private Chart barChart;

		public BarChartAdapter ()
		{
			//创建棒条图表 Create a piechart
			barChart = new BarChart ();
			barChart.IsStretch = true;
		}

		//在容器里增加棒条图表 Add the barChart to the container
		public override void AddToContainer(Control container) 
		{
			container.Controls.Add (barChart);
		}

		public override void UpdateDisplay (object sender, 
											ExplorationFinishEventArgs e)
		{
			//用结果来画图 Draw the chart using the given data
			barChart.Data = e.ExplorationResult;
			barChart.Draw();
		}
	}



}

