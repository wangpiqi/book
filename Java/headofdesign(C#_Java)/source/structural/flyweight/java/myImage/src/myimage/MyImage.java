package myimage;
//定义MyImage类,包括Image属性,绘画方法：
import java.awt.*;
import java.awt.image.*;
import java.io.*;
class MyImage{
    Image image;
    public MyImage(String file){
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        file="D:\\software2\\pattern\\structural\\flyweight\\java\\myImage\\classes\\myimage\\"+file;
        //file=".//"+file;
        File f = new File(file);
        if (f.exists() && f.isFile() && f.canRead()) {
          image = toolkit.getImage (file);
        }
        else System.err.println("Unable to load file " + file);
      }
    public void draw(Graphics g, int x, int y, String name, ImageObserver obs){
       g.drawImage(image,x,y,30,30,obs);
       g.drawString(name,x,y+40);
    }
}

