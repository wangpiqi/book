namespace Mortgage
{
	using System;
	//子系统 类A
	class Bank
	{
		public bool SufficientSavings( Customer c )
		{
			Console.WriteLine("Check bank for {0}", c.Name );
			return true;
		}
	}
	//子系统 类B
	class Credit
	{
		public bool GoodCredit( int amount, Customer c )
		{
			Console.WriteLine( "Check credit for {0}", c.Name );
			return true;
		}
	}
	//子系统 类C
	class Loan
	{
		public bool GoodLoan( Customer c )
		{
			Console.WriteLine( "Check loan for {0}", c.Name );
			return true;
		}
	}
	//申请抵押的顾客
	class Customer
	{
		private string name;
		//Constructors构造函数
		public Customer( string name )
		{
			this.name = name;
		}
		// Properties属性
		public string Name
		{
			get{ return name; }
		}
	}
	// "Facade" 
	class MortgageApplication
	{
		int amount;
		private Bank bank = new Bank();
		private Loan loan = new Loan();
		private Credit credit = new Credit();
		// Constructors
		public MortgageApplication( int amount )
		{
			this.amount = amount;
		}
		public bool IsEligible( Customer c )
		{
			//检查信用是否值得
			if( !bank.SufficientSavings( c ) )  
				return false;
			if( !loan.GoodLoan( c ) )  
				return false;
			if( !credit.GoodCredit( amount, c ))  
				return false;

			return true;
		}
	}
	/// <summary>
	/// Facade 客户测试程序
	/// </summary>
	public class FacadeApp
	{
		public static void Main(string[] args)
		{
			// 创建Facade
			MortgageApplication mortgage =new MortgageApplication( 125000 );
			//通过Facade调用子系统
			mortgage.IsEligible(new Customer( "Gabrielle McKinsey" ) );
			Console.Read();
		}
	}  

}
