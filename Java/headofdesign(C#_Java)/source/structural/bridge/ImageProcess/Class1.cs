using System;

namespace ImageProcess
{
	using System;
	//Base class for OS implemenations
	interface ImageImp
	{
		void DoPaint(string str); 
	}
	//Windows specific implemenation
	class WinImp : ImageImp
	{
		public void DoPaint(string str)
		{
			Console.WriteLine(str+"  WIN OS");               
		}      
	}
	//Abstract class for all image paintings
	class Image
	{
        protected ImageImp impToUse;
		public void SetImageImp(ImageImp ip)
		{
			impToUse = ip;
		}
		public virtual void Method(string s1)
		{
               
		}
	}
	//BMP specific paintings
	class BMPImage : Image
	{ 

		override public void Method(string s1)
		{
			string s2 = s1 + " BMP IMAGE";
			impToUse.DoPaint(s2);   
		}
	}
	//Client
	class MyPaint
	{
		public Image SetUpMethod()
		{
			Image im = new BMPImage(); // BMP IMAGE
			ImageImp win = new WinImp();// WIN OS
			im.SetImageImp(win);
			return im;
		}
	} 

	class Class1
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			MyPaint mp = new MyPaint();
			Image im = mp.SetUpMethod();
			im.Method("PAINTING-->");
			Console.Read();
		}
	}
}
