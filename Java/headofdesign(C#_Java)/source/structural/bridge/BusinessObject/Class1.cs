
namespace BusinessObjectApp
{
	using System;
	using System.Collections;
	// 商业对象类
	class BusinessObject
	{
		private DataObject dataObject;
		protected string group;
		// 构筑函数
		public BusinessObject( string group )
		{
			this.group = group;
		}
		//数据对象属性
		public DataObject DataObject
		{
			set{ dataObject = value; }
			get{ return dataObject; }
		}
		//调用了数据对象的引用来处理
		virtual public void Next()
		{			
			dataObject.NextRecord();
		}
		virtual public void Prior()
		{
			dataObject.PriorRecord();
		}
		virtual public void New( string name )
		{
			dataObject.NewRecord( name );
		}
		virtual public void Delete( string name )
		{
			dataObject.DeleteRecord( name );
		}
		//单个显示
		virtual public void Show()
		{
			dataObject.ShowRecord();
		}
		//全部显示
		virtual public void ShowAll()
		{
			Console.WriteLine( "客户集合: {0}", group );
			dataObject.ShowAllRecords();
		}
	}
	// 商业对象派生类-客户商业对象
	class CustomersBusinessObject : BusinessObject
	{
		// 构筑函数，调用原接口的构筑函数
		public CustomersBusinessObject( string group )
			: base( group ){}
		override public void ShowAll()
		{
			//在原显示所有数据基础上增加上下分行线
			Console.WriteLine();
			Console.WriteLine( "------------------------" );
			base.ShowAll();
			Console.WriteLine( "------------------------" );
		}
	}
	//抽象数据对象实现类
	abstract class DataObject
	{
		abstract public void NextRecord();
		abstract public void PriorRecord();
		abstract public void NewRecord( string name );
		abstract public void DeleteRecord( string name );
		abstract public void ShowRecord();
		abstract public void ShowAllRecords();
	}
	//具体实现类--客户数据对象
	class CustomersDataObject : DataObject
	{
		private ArrayList customers = new ArrayList();
		private int current = 0;
		public CustomersDataObject()
		{
			//可以从数据库取得
			customers.Add( "张三" );
			customers.Add( "李四" );
			customers.Add( "王五" );
			customers.Add( "张六" );
			customers.Add( "赵七" );
		}
		public override void NextRecord()
		{
			if( current <= customers.Count - 1 )
				current++;
		}
		public override void PriorRecord()
		{
			if( current > 0 )
				current--;
		}
		public override void NewRecord( string name )
		{
			customers.Add( name );
		}
		public override void DeleteRecord( string name )
		{
			customers.Remove( name );
		}
		public override void ShowRecord()
		{
			Console.WriteLine( customers[ current ] );
		}
		public override void ShowAllRecords()
		{
			foreach( string name in customers )
				Console.WriteLine( " " + name );
		}
	}
	/// <summary>
	/// 客户测试程序
	/// </summary>
	public class BusinessApp
	{
		public static void Main( string[] args )
		{
			// 创建扩充抽象类-客户商业对象 
			CustomersBusinessObject customers =
				new CustomersBusinessObject("广州顾客名单");
			// 设定它的具体实现类-客户数据对象
			customers.DataObject = new CustomersDataObject();
			//单个显示出来
			customers.Show();
			customers.Next();
			customers.Show();
			customers.Next();
			customers.Show();
			customers.New( "陈八" );
			//显示所有其数据
			customers.ShowAll();
			Console.Read();
		}
	}

}
