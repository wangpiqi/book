package frog;

public class DancingFrog extends NormalFrog {
  NormalFrog frog;
  DancingFrog(String name)
  {
    super(name);
  }
  public void Jump();

  public void Eat();
  public void Train(NormalFrog frog)
  {
    this.frog=frog;
  }
}