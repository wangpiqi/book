package frog;

public abstract class  NormalFrog {
  String name;
  public NormalFrog(String name){
    this.name=name;
  }
  public abstract void Jump(){
    System.out.println(this.name+" jump");
  }
  public abstract void Eat(){
    System.out.println(this.name+" Eat");
  }
}