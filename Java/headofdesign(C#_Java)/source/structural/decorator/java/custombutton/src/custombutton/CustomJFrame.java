package custombutton;
import javax.swing.*;
import java.awt.*;
public class CustomJFrame extends JFrame {
  public CustomJFrame()
  {
     setSize(400,200);
     getContentPane().setLayout(new FlowLayout());
     getContentPane().add(new CustomJButton(new JButton("Save")));
     show();
  }
  public static void main(String args[])
  {
     new CustomJFrame();
  }
}