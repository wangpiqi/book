
//Title:        UsingPlastic
//Version:      
//Copyright:    Copyright (c) 1999
//Author:       T-Kiang Tan
//Company:      University of Chicago: CSPP 523
//Description:  Demo.
package UsingPlastic;

public interface PlasticProxyInterface
{
    public String getApproval(String card_type);
    public String initialize();
} 