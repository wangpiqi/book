
//Title:        UsingPlastic
//Version:
//Copyright:    Copyright (c) 1999
//Author:       T-Kiang Tan
//Company:      University of Chicago: CSPP 523
//Description:  Demo.
package UsingPlastic;

import java.util.*; //Using ArrayList and HashTable

public class PlasticProxy implements PlasticProxyInterface
{

    ArrayList list;
    String[] card_types;
    public PlasticProxy(String[] cardTypes)
    {
        card_types = cardTypes;
        list = new ArrayList(card_types.length);
    }
    public String getApproval(String card_type)
    {
        System.out.println("****PlasticProxy: Connecting to " + card_type);
        Plastic plastic= new Plastic(card_type);
        System.out.println("****PlasticProxy: " + plastic.initialize());
        System.out.println("****PlasticProxy: Requesting for approval from " + card_type);

        System.out.println("****PlasticProxy: " + plastic.getApproval(card_type));
        return("Transaction has been approved");
    }
    public String initialize()
    {
        System.out.println("****PlasticProxy: Initializing proxy. done only once");
        return(new String("PlasticProxy is initialized"));
    }

}