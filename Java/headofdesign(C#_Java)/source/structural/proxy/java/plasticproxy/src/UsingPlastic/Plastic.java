
//Title:        UsingPlastic
//Version:
//Copyright:    Copyright (c) 1999
//Author:       T-Kiang Tan
//Company:      University of Chicago: CSPP 523
//Description:  Demo.
package UsingPlastic;

public class Plastic implements PlasticProxyInterface
{
    String card;
    public Plastic(String card_type){ card = card_type;}
    public String getApproval(String card_type)
    {
        System.out.println("******" + card + ": Request is in progress");
        return(card + " - Transaction Approved");
    }

    public String initialize()
    {
        System.out.println("******" +card + ": Initializing is in progress");
        return("Connection Established with " + card);
    }
}