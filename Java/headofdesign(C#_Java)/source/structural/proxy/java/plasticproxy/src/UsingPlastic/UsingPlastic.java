
//Title:      UsingPlastic
//Version:
//Copyright:  Copyright (c) 1999
//Author:     T-Kiang Tan
//Company:    University of Chicago: CSPP 523
//Description:Demo.

//Title:      UsingPlastic
//Version:
//Copyright:  Copyright (c) 1999
//Author:     T-Kiang Tan
//Company:    University of Chicago: CSPP 523
//Description:Demo.
package UsingPlastic;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class UsingPlastic
{
    public static void main(String[] card_types)
    {
	    String input = " ";
        String[] tmp;
        tmp = new String[4];
        tmp[0] = "Visa";
        tmp[1] = "MasterCard";
        tmp[2] = "AmericanExpress";
        tmp[3] = "Diners";
        int i = 0;
        if (card_types.length == 0)
            card_types = tmp;

        PlasticProxy plasticproxy = new PlasticProxy(card_types);
        System.out.println("**UsingPlastic: " + plasticproxy.initialize());
	    while (true)
        {
            System.out.println("Main: Please slide the card when ready:");
            for(i = 0; i < card_types.length; i++)
                System.out.println("Main: " + (i+1) + ". " + card_types[i]);
            System.out.println("Main:To exit, type \"exit\" ");
	        System.out.print("Main: Which card did you slide again? >> ");
	        input = ParserUtils.getKeyInput();
            if(input.equalsIgnoreCase("exit"))
                break;
            Integer integer = new Integer(input);
            i = integer.intValue() - 1;
            System.out.println("#########################");
            System.out.println("**UsingPlastic: Requesting for approval from " + card_types[i]);
            System.out.println("**UsingPlastic: " + plasticproxy.getApproval(card_types[i]) + " by " + card_types[i]);
            System.out.println("#########################");
	    }
        System.out.println("Main: Thanks you for using the Proxy Presentation Demo");
    }

}

