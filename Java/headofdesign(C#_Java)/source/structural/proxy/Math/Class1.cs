	using System;
	using System.Runtime.Remoting;
//定义接口IMath
	public interface IMath
	{
		//方法:加，减，乘，除
		double Add( double x, double y );
		double Sub( double x, double y );
		double Mul( double x, double y );
		double Div( double x, double y );
	}
	//实体,实现两种接口,MarshalByRefObject接口充许不同的程序域(进程间)通信
	class Math : MarshalByRefObject, IMath
	{
		public double Add( double x, double y ){ return x + y; }
		public double Sub( double x, double y ){ return x - y; }
		public double Mul( double x, double y ){ return x * y; }
		public double Div( double x, double y ){ return x / y; }
	}
	//远程代理Proxy对象,包括Math的一个实例,通过它来处理加，减，乘，除
	class MathProxy : IMath
	{
		Math math;
		public MathProxy()
		{
			math = new Math();
		}
		public double Add( double x, double y )
		{
			return math.Add(x,y);
		}
		public double Sub( double x, double y )
		{
			return math.Sub(x,y);
		}
		public double Mul( double x, double y )
		{
			return math.Mul(x,y);
		}
		public double Div( double x, double y )
		{
			return math.Div(x,y);
		}
	}
	/// <summary>
	/// Proxy应用测试
	/// </summary>
	public class ProxyApp
	{
		public static void Main( string[] args )
		{
			// 生成数学代理
			MathProxy p = new MathProxy();
			//进行加，减，乘，除计算
			Console.WriteLine( "4 + 2 = {0}", p.Add( 4, 2 ) );
			Console.WriteLine( "4 - 2 = {0}", p.Sub( 4, 2 ) );
			Console.WriteLine( "4 * 2 = {0}", p.Mul( 4, 2 ) );
			Console.WriteLine( "4 / 2 = {0}", p.Div( 4, 2 ) );
			Console.Read();
		}
	}

