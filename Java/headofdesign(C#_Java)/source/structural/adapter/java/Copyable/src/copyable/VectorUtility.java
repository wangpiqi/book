package copyable;
import java.util.*;
public class VectorUtility {
  public static  Vector Copy(Vector vin){
    Vector vout=new Vector();
    Enumeration e=vin.elements();
    while(e.hasMoreElements()){
      Copyable c=(Copyable)e.nextElement();
      if(c.isCopyable())
        vout.addElement(c);
    }
    return vout;
  }

  }
