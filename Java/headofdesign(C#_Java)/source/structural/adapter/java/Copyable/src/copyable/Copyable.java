package copyable;

public interface Copyable {
  public boolean isCopyable();
}