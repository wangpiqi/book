package copyable;
import javax.swing.*;
import java.awt.event.*;
class Test extends JFrame {

    public Test () {
        setSize(200,200);
        setVisible(true);
        addWindowListener(new Closer());
    }
    public static void main(String[] args) {
        new Test();
    }
    class Closer extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }
    }
}

