package clean;

public class Client {
  static void Jobs(Clean job)
  {
    if(job instanceof Clean){
      ((Clean)job).makeClean();
    }
    if(job instanceof Extra){
      ((Extra)job).takeCare();
    }
  }
  public static void main(String[] args) {
       Extra e = new Facility();
       Jobs(e);
       Clean c1 = new Office();
       Clean c2 = new Workshop();
       Jobs(c1);
       Jobs(c2);
  }
}