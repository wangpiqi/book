package peg;

public class Client {
  public static void main(String argv[]){
    RoundPeg roundPeg=new RoundPeg();
    SquarePeg squarePeg=new SquarePeg();
    PegAdapter adapter=new PegAdapter(roundPeg);
    squarePeg.Insert("Insert square peg...");
    adapter.Insert("Insert round peg...");
  }
}