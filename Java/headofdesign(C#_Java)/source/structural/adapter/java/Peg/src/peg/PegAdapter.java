package peg;

public class PegAdapter extends SquarePeg {
  private RoundPeg roundPeg;
  PegAdapter(RoundPeg peg){
    this.roundPeg=peg;
  }
  public void Insert(String str)
  {
    roundPeg.InsertIntoHole(str);
  }
}