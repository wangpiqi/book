using System;
namespace ChemicalDatabank
{
	using System;
	//目标抽象类
	class ChemicalCompound
	{
		protected string name;
		protected float boilingPoint;
		protected float meltingPoint;
		protected double molecularWeight;
		protected string molecularFormula;
		//构制函数
		public ChemicalCompound( string name )
		{
			this.name = name;
		}
		//属性
		public float BoilingPoint
		{
			get{ return boilingPoint; }
		}
		public float MeltingPoint
		{
			get{ return meltingPoint; }
		}
		public double MolecularWeight
		{
			get{ return molecularWeight; }
		}
		public string MolecularFormula
		{
			get{ return molecularFormula; }
		}
	}
	//适配器(公接口)
	class Compound : ChemicalCompound
	{
		private ChemicalDatabank bank;
		//
		public Compound( string name ) : base( name )
		{
			//适配器(母接口)
			bank = new ChemicalDatabank();
			//调用适配器(母接口)
			boilingPoint = bank.GetCriticalPoint( name, "B" );
			meltingPoint = bank.GetCriticalPoint( name, "M" );
			molecularWeight = bank.GetMolecularWeight( name );
			molecularFormula = bank.GetMolecularStructure( name );
		}
		public void Display()
		{
			Console.WriteLine("\n化合物: {0} ------ ",name );
			Console.WriteLine(" 分子式: {0}",MolecularFormula);
			Console.WriteLine(" 分子量: {0}",MolecularWeight );
			Console.WriteLine(" 熔点: {0}",MeltingPoint );
			Console.WriteLine(" 沸点: {0}",BoilingPoint );
		}
	}
	//适配器(母接口)
	class ChemicalDatabank
	{
		//取得临界点
		public float GetCriticalPoint( string compound, string point )
		{
			float temperature = 0.0F;
			//熔点
			if( point == "M" )
			{
				switch( compound.ToLower() )
				{
					case "water": temperature = 0.0F; break;
					case "benzene" : temperature = 5.5F; break;
					case "alcohol": temperature = -114.1F; break;
				}
			}
			//沸点
			else
			{
				switch( compound.ToLower() )
				{
					case "water": temperature = 100.0F;break;
					case "benzene" : temperature = 80.1F; break;
					case "alcohol": temperature = 78.3F; break;
				}
			}
			return temperature;
		}
		//取得分子式
		public string GetMolecularStructure( string compound )
		{
			string structure = "";
			switch( compound.ToLower() )
			{
				case "water": structure = "H20"; break;
				case "benzene" : structure = "C6H6"; break;
				case "alcohol": structure = "C2H6O2"; break;
			}
			return structure;
		}
		//取得分子量
		public double GetMolecularWeight( string compound )
		{
			double weight = 0.0;
			switch( compound.ToLower() )
			{
				case "water": weight = 18.015; break;
				case "benzene" : weight = 78.1134; break;
				case "alcohol": weight = 46.0688; break;
			}
			return weight;
		}
	}
	/// <summary>
	///适配器的测试应用
	/// </summary>
	public class AdapterApp
	{
		public static void Main(string[] args)
		{
			//显示取得水的特性
			Compound water = new Compound( "Water" );
			water.Display();
			//显示取得苯的特性
			Compound benzene = new Compound( "Benzene" );
			benzene.Display();
			//显示取得乙醇的特性
			Compound alcohol = new Compound( "Alcohol" );
			alcohol.Display();
			Console.Read();//取键结束，方便查看结果
		}
	}
}
