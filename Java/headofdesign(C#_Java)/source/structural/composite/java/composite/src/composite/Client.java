package composite;

public class Client {

  public static void main (String[] args) {
    Square square1 = new Square ();
    square1.setName ("square1");
    Circle circle1 = new Circle ();
    circle1.setName ("circle1");
    CompoundShape compoundShape1 = new CompoundShape ();
    compoundShape1.setName ("CompoundShape1");
    compoundShape1.addShape (square1);
    compoundShape1.addShape (circle1);
    Square square2 = new Square ();
    square2.setName ("square2");
    CompoundShape compoundShape2 = new CompoundShape ();
    compoundShape2.setName ("CompoundShape2");
    Square square3 = new Square ();
    square3.setName ("square3");
    compoundShape2.addShape (square2);
    compoundShape2.addShape (square3);
    compoundShape2.addShape (compoundShape1);
    compoundShape2.draw ();
    System.out.println ("-------");
    CompoundShape.Iterator itr = new CompoundShape.Iterator (compoundShape2);
    Shape s = null;
    while (itr.hasNext ()) {
      s = itr.next ();
      System.out.println (s.getName ());
    }
    System.out.println ("-------");
    CompoundShape.Iterator itr2 = new CompoundShape.Iterator (compoundShape2);
    while (itr2.hasNext ()) {
      s = itr2.next ();
      System.out.println (s.getName ());
    }
  }
}
