package composite;

public class Circle implements PrimitiveShape {
  private String name;
  private Shape parent;
  public Circle () {
  }

  public String getName () {
    return name;
  }

  public void setName (String n) {
    name = n;
  }

  public Shape getParent () {
    return parent;
  }

  public void setParent (Shape s) {
    parent = s;
  }

  public void draw () {
    System.out.println ("Drawing Circle: " + name);
  }
}