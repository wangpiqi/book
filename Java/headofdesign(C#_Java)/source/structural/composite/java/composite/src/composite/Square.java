package composite;

public class Square implements PrimitiveShape {
  private String name ;
  private Shape parent ;
  public Square () {
  }

  public Shape next () {
    return this;
  }

  public String getName () {
    return name;
  }

  public void setName (String n) {
    name = n;
  }

  public Shape getParent () {
    return parent;
  }

  public void setParent (Shape s) {
    parent = s;
  }

  public void draw () {
    System.out.println ("Drawing Square: " + name);
  }

}