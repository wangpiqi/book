package composite;

public interface Shape {
  public String getName ();
  public void draw ();
  public Shape getParent ();
  public void setParent (Shape s);
}