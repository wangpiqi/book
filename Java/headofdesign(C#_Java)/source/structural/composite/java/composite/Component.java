package composite;
import java.lang.*;

public abstract class Component {

  // la classe Component implementa sia l'interfaccia di Leaf che di Composite
  public Component(String aName){
  	name = aName;
  }

  // questa \uFFFD l'operazione, che il Leaf implementer\uFFFD direttamente
  // mentre il Composite delegher\uFFFD ai suoi componenti

  public abstract void printName();

  // questa \uFFFD l'interfaccia di gestione dei figli.
  // \uFFFD stato implementato un controllo sfruttando il DP
  // Marker Interface per evitare che i Leaf tentino di aggiungere o rimuovere
  // figli.

  public void add(Component c){
  	if (this instanceof LeafInterface){
    	throw new LeafException("Method don't  support");
  	}
  }

  public void remove(Component c){
  	if (this instanceof LeafInterface){
            throw new LeafException("Method don't  support");
          }
        }
  }

  // questa operazione invece pu\uFFFD anche avere un senso, come da
  // punto 3 del GoF. Comunque, una volta implementata la soluzione
  // MarkerInterface, anche getChild() potrebbe sollevare un'eccezione

  public Component getChild(int n){
  	return null;
  }

  // il solito attributo, pubblico per semplificare il codice

  public String name;
}