package composite;
class LeafException extends Exception {
  public LeafException(String msg){
    super(msg);
  }
}