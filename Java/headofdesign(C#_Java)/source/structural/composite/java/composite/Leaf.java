package composite;

public class Leaf extends Component implements LeafInterface{

  // la classe implementa la LeafInterface, in modo da poter
  // sollevare un eccezione nel caso vengano richiamati i metodi
  // di gestione dei figli propri del composite

	public Leaf(String aName) {
  	super(aName);
	}

  public void printName(){
  	System.out.println(name);
  }
}