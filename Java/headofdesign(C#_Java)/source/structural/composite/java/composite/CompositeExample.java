package composite;
public class CompositeExample {
	public static void main(String[] args) {

    // creiamo 4 componenti "Leaf"
    Component pino = new Leaf("Pino");
    Component gino = new Leaf("Gino");
    Component lino = new Leaf("Lino");
    Component tino = new Leaf("Tino");

    // un composite con 3 dei 4 leaf
    Component trio = new Composite("Trio");
    try {
      trio.add(pino);
      trio.add(gino);
      trio.add(lino);
    }
    catch (LeafException e){
    	e.printStackTrace();
    }

    // un composite composto da un altro composite e da un leaf

    Component mino = new Composite("Mino");
    try{
    	mino.add(trio);
    	mino.add(tino);
    }
    catch (LeafException e){
    	e.printStackTrace();
    }

    // prova di richiamo operation() su alcuni oggetti

    pino.printName();
    trio.printName();
    mino.printName();

    // errore: si prova a richiamare add() su un Leaf

    try{
    	tino.add(trio);
    }
    catch (LeafException e){
    	e.printStackTrace();
    }
	}
}