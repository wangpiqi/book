package composite;
import java.util.Vector;
import java.util.Enumeration;

public class Composite extends Component {

	public Composite(String aName) {
  	super(aName);
	}

  public void printName(){

    // l'operazione viene inoltrata ai componenti interni del composite,
    // in maniera ricorsiva
  	System.out.println(name);
    System.out.println("Sono composto da:");
    System.out.println("___________________");
    Enumeration iterator = childs.elements();
    Component c = null;
    while (iterator.hasMoreElements()){
    	c = (Component)iterator.nextElement();
      c.printName();
    }
    System.out.println("___________________");
  }

  public void add(Component c){
  	childs.addElement(c);
  }

  public void remove(Component c){
  	childs.removeElement(c);
  }
  public Component getChild(int n){
  	return (Component)childs.elementAt(n);
  }
  private Component component;

  // il Vector che contiene i figli
  private Vector childs = new Vector();
}