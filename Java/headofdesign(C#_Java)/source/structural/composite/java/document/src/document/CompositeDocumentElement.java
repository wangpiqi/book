package document;

//定义抽象的CompositeDocumentElement类,实现DocumentElement接口：
import java.util.*;
public abstract class CompositeDocumentElement implements DocumentElement{
  private Vector vector=new Vector();
  public void Draw() {
    for(int i=0;i<vector.size();i++){
      ((DocumentElement)vector.elementAt(i)).Draw();
    }
  }
  public void Add(DocumentElement doc){
    vector.add(doc);
  }
}