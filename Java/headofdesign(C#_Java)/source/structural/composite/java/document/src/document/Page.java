package document;
//定义CompositeDocumentElement的子类
public class Page extends CompositeDocumentElement {
  public void Draw() {
    System.out.println("Page content:");
    super.Draw();
  }
}