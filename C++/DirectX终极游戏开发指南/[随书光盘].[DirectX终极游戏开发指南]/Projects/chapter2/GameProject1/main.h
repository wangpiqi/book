/*
   Demo Name:  Game Project 1
      Author:  Allen Sherrod
     Chapter:  Chapter 2
*/


#ifndef _UGP_MAIN_H_
#define _UGP_MAIN_H_


#include"StrandedEngine/engine.h"
#pragma comment(lib, "lib/StrandedEngine.lib")

#define WINDOW_CLASS    "StrandedGame"
#define WINDOW_NAME     "Stranded"
#define WIN_WIDTH       800
#define WIN_HEIGHT      600
#define FULLSCREEN      1

// Function Prototypes...
bool InitializeEngine();
void ShutdownEngine();

// Main game functions.
bool GameInitialize();
void GameLoop();
void GameShutdown();

#endif