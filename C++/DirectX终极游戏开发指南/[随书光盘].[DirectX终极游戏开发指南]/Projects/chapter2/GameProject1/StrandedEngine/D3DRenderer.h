/*
   Demo Name:  Game Project 1
      Author:  Allen Sherrod
     Chapter:  Chapter 2
*/


#ifndef _D3D_RENDERER_H_
#define _D3D_RENDERER_H_

#include<windows.h>
#include<d3d9.h>
#include<d3dx9.h>
#include"RenderInterface.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


struct stD3DStaticBuffer
{
   stD3DStaticBuffer() : vbPtr(0), ibPtr(0), numVerts(0),
                         numIndices(0), stride(0), fvf(0),
                         primType(NULL_TYPE) {}

   LPDIRECT3DVERTEXBUFFER9 vbPtr;
   LPDIRECT3DINDEXBUFFER9 ibPtr;
   int numVerts;
   int numIndices;
   int stride;
   unsigned long fvf;
   PrimType primType;
};


class CD3DRenderer : public CRenderInterface
{
   public:
      CD3DRenderer();
      ~CD3DRenderer();
      
      bool Initialize(int w, int h, WinHWND mainWin,
                      bool fullScreen);
      void Shutdown();
      
      void SetClearCol(float r, float g, float b);
      void StartRender(bool bColor, bool bDepth, bool bStencil);
      void ClearBuffers(bool bColor, bool bDepth, bool bStencil);
      void EndRendering();

      void CalculateProjMatrix(float fov, float n, float f);
      void CalculateOrthoMatrix(float n, float f);

      int CreateStaticBuffer(VertexType, PrimType,
         int totalVerts, int totalIndices,
         int stride, void **data, unsigned int *indices,
         int *staticId);

      int Render(int staticId);

   private:
      void OneTimeInit();

   
   private:
      D3DCOLOR m_Color;
      LPDIRECT3D9 m_Direct3D;
      LPDIRECT3DDEVICE9 m_Device;
      bool m_renderingScene;
      
      stD3DStaticBuffer *m_staticBufferList;
      int m_numStaticBuffers;
      int m_activeStaticBuffer;
};

bool CreateD3DRenderer(CRenderInterface **pObj);

#endif