/*
   Demo Name:  Bounding Box
      Author:  Allen Sherrod
     Chapter:  Ch 9
*/


#include<d3d9.h>
#include<d3dx9.h>
#include<stdio.h>

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Bounding Box"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

// Function Prototypes...
bool InitializeD3D(HWND hWnd, bool fullscreen);
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;

// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


struct stBoundingBox
{
   stBoundingBox () { min[0] = min[1] = min[2] =
                      max[0] = max[1] = max[2] = 0; }
   float min[3];
   float max[3];
};

stBoundingBox g_boundingBox;


// Amounts to move each object.
float g_obj1XPos = -1;
float g_obj1MoveAmt = 0;
float g_obj1Dir = -0.02f;

float g_obj2XPos = 1;
float g_obj2MoveAmt = 0;
float g_obj2Dir = 0.02f;

// True if collision occurred, false if not.
bool g_collision = false;


// DirectX font object.
LPD3DXFONT g_Font = NULL;


// Vertex buffer to hold the geometry.
LPDIRECT3DVERTEXBUFFER9 g_VertexBuffer = NULL;

// A structure for our custom vertex type
struct stD3DVertex
{
    float x, y, z;
    unsigned long color;
};

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_VERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE)


void CalculateBoundingBox(stD3DVertex *vList, int totalVerts, stBoundingBox *bb)
{
   if(!vList || totalVerts <= 0 || !bb) return;

   for(int i = 0; i < totalVerts; i++)
      {
         if(vList[i].x < bb->min[0]) bb->min[0] = vList[i].x;
         if(vList[i].x > bb->max[0]) bb->max[0] = vList[i].x;

         if(vList[i].y < bb->min[1]) bb->min[1] = vList[i].y;
         if(vList[i].y > bb->max[1]) bb->max[1] = vList[i].y;

         if(vList[i].z < bb->min[2]) bb->min[2] = vList[i].z;
         if(vList[i].z > bb->max[2]) bb->max[2] = vList[i].z;
      }
}


// Point inside box.
bool OnCollision(stBoundingBox bb, float x, float y, float z)
{
	if(bb.max[0] <= x) return false;
	if(bb.min[0] >= x) return false;
   if(bb.max[1] <= y) return false;
	if(bb.min[1] >= y) return false;
	if(bb.max[2] <= z) return false;
	if(bb.min[2] >= z) return false;

   // Return true then we have collision.
	return true;	
}


// Box on box.
bool OnCollision(stBoundingBox &bb1, stBoundingBox &bb2)
{
   if((bb1.min[0] > bb2.max[0]) || (bb2.min[0] > bb1.max[0]))
      return false;
   if((bb1.min[1] > bb2.max[1]) || (bb2.min[1] > bb1.max[1]))
      return false;
   if((bb1.min[2] > bb2.max[2]) || (bb2.min[2] > bb1.max[2]))
      return false;

   return true;
}


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Initialize Direct3D
   if(InitializeD3D(hWnd, false))
      {
         // Show the window
         ShowWindow(hWnd, SW_SHOWDEFAULT);
         UpdateWindow(hWnd);

         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D(HWND hWnd, bool fullscreen)
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(fullscreen)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
   // Create the font.
   if(FAILED(D3DXCreateFont(g_D3DDevice, 18, 0, 0, 1, 0,
      DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY,
      DEFAULT_PITCH | FF_DONTCARE, "Arial",
      &g_Font))) return false;


   // Fill in our structure to draw an object.
   // x, y, z, color, texture coords.
   stD3DVertex objData[] =
      {
         {-0.3f, -0.4f, 0, D3DCOLOR_XRGB(255,255,255)},
         {0.3f, -0.4f, 0, D3DCOLOR_XRGB(255,255,255)},
	      {0.3f, 0.4f, 0, D3DCOLOR_XRGB(255,255,255)},

	      {0.3f, 0.4f, 0, D3DCOLOR_XRGB(255,255,255)},
         {-0.3f, 0.4f, 0, D3DCOLOR_XRGB(255,255,255)},
	      {-0.3f, -0.4f, 0, D3DCOLOR_XRGB(255,255,255)}
      };


   CalculateBoundingBox(objData, 6, &g_boundingBox);


   // Create the vertex buffer.
   if(FAILED(g_D3DDevice->CreateVertexBuffer(sizeof(objData), 0,
             D3DFVF_VERTEX, D3DPOOL_DEFAULT,
             &g_VertexBuffer, NULL))) return false;
   
   // Fill the vertex buffer.
   void *ptr;

   if(FAILED(g_VertexBuffer->Lock(0, sizeof(objData),
      (void**)&ptr, 0))) return false;

   memcpy(ptr, objData, sizeof(objData));

   g_VertexBuffer->Unlock();


	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, 0.0f, -5.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


void RenderScene()
{
   // RECT used to position the font and a string.
   RECT fontPos = {0, 125, WINDOW_WIDTH, WINDOW_HEIGHT};
   char str[64] = {0};


   // Move objects. If position limit is hit, switch directions.
   g_obj1MoveAmt += g_obj1Dir;
   if(g_obj1MoveAmt > 2) g_obj1Dir *= -1;
   if(g_obj1MoveAmt < -2) g_obj1Dir *= -1;

   g_obj2MoveAmt += g_obj2Dir;
   if(g_obj2MoveAmt > 2) g_obj2Dir *= -1;
   if(g_obj2MoveAmt < -2) g_obj2Dir *= -1;

   // Get bounding box for each object.
   stBoundingBox newBB1, newBB2;
   memcpy(&newBB1, &g_boundingBox, sizeof(stBoundingBox));
   memcpy(&newBB2, &g_boundingBox, sizeof(stBoundingBox));

   // Move boxes for each object along since they have moved.
   newBB1.min[0] = g_boundingBox.min[0] + (g_obj1XPos + g_obj1MoveAmt);
   newBB1.max[0] = g_boundingBox.max[0] + (g_obj1XPos + g_obj1MoveAmt);
   newBB2.min[0] = g_boundingBox.min[0] + (g_obj2XPos + g_obj2MoveAmt);
   newBB2.max[0] = g_boundingBox.max[0] + (g_obj2XPos + g_obj2MoveAmt);
   
   // Test for collision and record results.
   g_collision = OnCollision(newBB1, newBB2);
   
   // Create string.
   if(g_collision) sprintf(str, "Collision: TRUE");
   else sprintf(str, "Collision: FALSE");


   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Display if collision occurred.
      g_Font->DrawText(NULL, str, -1, &fontPos, DT_CENTER,
                       D3DCOLOR_XRGB(255,255,255));


      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);


      // Set stream.
      g_D3DDevice->SetStreamSource(0, g_VertexBuffer,
                                   0, sizeof(stD3DVertex));

      // Draw square 1.
      D3DXMatrixTranslation(&g_worldMatrix, g_obj1XPos + g_obj1MoveAmt, 0, 0);
      g_D3DDevice->SetTransform(D3DTS_WORLD, &g_worldMatrix);
      g_D3DDevice->SetFVF(D3DFVF_VERTEX);
      g_D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);


      // Draw square 2.
      D3DXMatrixTranslation(&g_worldMatrix, g_obj2XPos + g_obj2MoveAmt, 0, 0);
      g_D3DDevice->SetTransform(D3DTS_WORLD, &g_worldMatrix);
      g_D3DDevice->SetFVF(D3DFVF_VERTEX);
      g_D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   if(g_VertexBuffer != NULL) g_VertexBuffer->Release();
   g_VertexBuffer = NULL;

   if(g_Font != NULL) g_Font->Release();
   g_Font = NULL;
}