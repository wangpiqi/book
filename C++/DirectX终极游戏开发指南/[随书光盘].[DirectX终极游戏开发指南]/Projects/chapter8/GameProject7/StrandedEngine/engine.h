/*
   Demo Name:  Game Project 7
      Author:  Allen Sherrod
     Chapter:  Chapter 8
*/


#ifndef _UGP_ENGINE_H_
#define _UGP_ENGINE_H_

#include"structs.h"
#include"RenderInterface.h"
#include"D3DRenderer.h"
#include"light.h"
#include"material.h"
#include"MathLibrary.h"

#endif