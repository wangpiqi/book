/*
   Demo Name:  Game Project 7
      Author:  Allen Sherrod
     Chapter:  Chapter 8
*/


#ifndef _UGP_MATH_LIBRARY_H_
#define _UGP_MATH_LIBRARY_H_


#include"Vector.h"
#include"Matrix.h"
#include"Quaternion.h"
#include"Physics.h"

class CRay;
class CPlane;
class CPolygon;

#include"Ray.h"
#include"Plane.h"
#include"Polygon.h"
#include"MathDefines.h"


#endif