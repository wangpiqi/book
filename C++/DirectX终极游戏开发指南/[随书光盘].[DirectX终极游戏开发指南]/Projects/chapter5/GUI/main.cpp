/*
   Demo Name:  Direct3D Graphical User Interface
      Author:  Allen Sherrod
     Chapter:  Ch 5
*/


#include<d3d9.h>
#include<d3dx9.h>
#include"D3DGUI.h"

#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "D3D GUI Demo"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

// Function Prototypes...
bool InitializeD3D(HWND hWnd, bool fullscreen);
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;

// DirectX gui object.
CD3DGUISystem *g_gui = NULL;

// Mouse state information.
bool LMBDown = false;
int mouseX = 0, mouseY = 0;

// Font ids used to create font objects.
int arialID = -1;
int timesID = -1;

// ids for our GUI controls.
#define STATIC_ID_1  1
#define STATIC_ID_2  2
#define BUTTON_ID_1  3
#define BUTTON_ID_2  4
#define BUTTON_ID_3  5
#define BUTTON_ID_4  6


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;

         case WM_LBUTTONDOWN:
            LMBDown = true;
            break;

         case WM_LBUTTONUP:
            LMBDown = false;
            break;

         case WM_MOUSEMOVE:
            mouseX = LOWORD (lParam);
            mouseY = HIWORD (lParam);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Initialize Direct3D
   if(InitializeD3D(hWnd, false))
      {
         // Show the window
         ShowWindow(hWnd, SW_SHOWDEFAULT);
         UpdateWindow(hWnd);

         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D(HWND hWnd, bool fullscreen)
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(fullscreen)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;

   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING |
             D3DCREATE_PUREDEVICE, &d3dpp, &g_D3DDevice)))
      {
         return false;
      }

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
   // Set the image states.
	g_D3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE);
	g_D3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
	g_D3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

   // Create the GUI system.
   g_gui = new CD3DGUISystem(g_D3DDevice, WINDOW_WIDTH, WINDOW_HEIGHT);
   if(!g_gui) return false;

   // Add background image.
   if(!g_gui->AddBackdrop("backdrop.jpg")) return false;

   // Add two types of fonts to the system.
   if(!g_gui->CreateFont("Arial", 18, &arialID)) return false;
   if(!g_gui->CreateFont("Times New Roman", 18, &timesID)) return false;

   // Add the static text to the GUI.
   if(!g_gui->AddStaticText(STATIC_ID_1, "Main Menu Version: 1.0",
      450, 100, D3DCOLOR_XRGB(255,255,255), arialID)) return false;

   if(!g_gui->AddStaticText(STATIC_ID_2, "Chapter 4 - GUI Demo",
      450, 340, D3DCOLOR_XRGB(255,255,255), timesID)) return false;

   // Add 3 buttons to the GUI.
   if(!g_gui->AddButton(BUTTON_ID_1, 50, 200, "startUp.png",
      "StartOver.png", "startDown.png")) return false;

   if(!g_gui->AddButton(BUTTON_ID_2, 50, 232, "loadUp.png",
      "loadOver.png", "loadDown.png")) return false;

   if(!g_gui->AddButton(BUTTON_ID_3, 50, 264, "optionsUp.png",
      "optionsOver.png", "optionsDown.png")) return false;

   if(!g_gui->AddButton(BUTTON_ID_4, 50, 296, "quitUp.png",
      "quitOver.png", "quitDown.png")) return false;

   return true;
}


void GUICallback(int id, int state)
{
   switch(id)
      {
         case BUTTON_ID_1:
         case BUTTON_ID_2:
         case BUTTON_ID_3:
            // Demonstration.  No need to do anything.
            break;

         
         case BUTTON_ID_4:
            // If the quit button is pressed, quit app.
            if(state == UGP_BUTTON_DOWN) PostQuitMessage(0);
            break;
      }
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Process and render our gui system.
      ProcessGUI(g_gui, LMBDown, mouseX, mouseY, GUICallback);

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release(); g_D3DDevice = NULL;
   if(g_D3D != NULL) g_D3D->Release(); g_D3D = NULL;
   if(g_gui) { g_gui->Shutdown(); delete g_gui; g_gui = NULL; }
}