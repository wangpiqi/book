/*
   Demo Name:  Direct3D Frames Per Second
      Author:  Allen Sherrod
     Chapter:  Chapter 5
*/


#include<d3d9.h>
#include<d3dx9.h>
#include<stdio.h>

#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "D3D FPS Demo"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

// Function Prototypes...
bool InitializeD3D(HWND hWnd, bool fullscreen);
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;

// DirectX font object.
LPD3DXFONT g_Font = NULL;

// RECT used to position the font.
RECT g_FontPosition = {0, 0, 0, 0};

// FPS info.
int g_fps = 0;
char g_fpsStr[16] = {0};
float g_time = 0.0f;
float g_lastTime = 0.0f;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Initialize Direct3D
   if(InitializeD3D(hWnd, false))
      {
         // Show the window
         ShowWindow(hWnd, SW_SHOWDEFAULT);
         UpdateWindow(hWnd);

         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D(HWND hWnd, bool fullscreen)
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(fullscreen)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;

   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING |
             D3DCREATE_PUREDEVICE, &d3dpp, &g_D3DDevice)))
      {
         return false;
      }

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
   // Create the font.
   if(FAILED(D3DXCreateFont(g_D3DDevice, 18, 0, 0, 1, 0,
         DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY,
         DEFAULT_PITCH | FF_DONTCARE, "Arial", &g_Font))) return false;
   
   // Here we are setting the position of the font.
   g_FontPosition.top = 0;
   g_FontPosition.left = 0;
   g_FontPosition.right = WINDOW_WIDTH;
   g_FontPosition.bottom = WINDOW_HEIGHT;

   return true;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Set next position and draw text.
      g_FontPosition.top = 150;
      g_Font->DrawText(NULL, "Ultimate Game Programming using DirectX!",
                       -1, &g_FontPosition, DT_CENTER, D3DCOLOR_XRGB(255,255,255));

      // Set next position and draw text.
      g_FontPosition.top = 180;
      g_Font->DrawText(NULL, "Demo on displaying text in a 3D application.",
                       -1, &g_FontPosition, DT_CENTER, D3DCOLOR_XRGB(255,255,255));

      // Set next position and draw text.
      g_FontPosition.top = 210;
      g_Font->DrawText(NULL, g_fpsStr, -1, &g_FontPosition,
                       DT_CENTER, D3DCOLOR_XRGB(255,255,255));

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);


   // Get the FPS.

   // Get the second in millisecond then multiply it to convert to seconds.
   g_time = GetTickCount() * 0.001f;

   // If the time we have now substracted from the previous second is greater than
   // or equal to 1 (i.e. if a second has passed) then we display the FPS number.
   if(g_time - g_lastTime > 1.0f)
      {
         // Record last time.
	      g_lastTime = g_time;

	      // Save FPS.
	      sprintf(g_fpsStr, "FPS: %d", g_fps);
		   
         // Reset the FPS counter.
         g_fps = 0;
      }
   else
      {
         // Add to the frames per second.
         g_fps++;
      }
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release(); g_D3DDevice = NULL;
   if(g_D3D != NULL) g_D3D->Release(); g_D3D = NULL;
   if(g_Font) g_Font->Release(); g_Font = NULL;
}