/*
   Demo Name:  Sample Framework
      Author:  Allen Sherrod
     Chapter:  Ch 1
*/


#include"dxstdafx.h"

#define WINDOW_NAME     L"Sample Framework"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

ID3DXFont*              g_pFont = NULL;         // Used to draw text.
ID3DXSprite*            g_pTextSprite = NULL;   // Used to batch drawing text.
CDXUTDialog             g_HUD;                  // Standard controls (upper right).
CDXUTDialog             g_SampleUI;             // Application specific controls.

// Vertex buffer to hold the geometry.
LPDIRECT3DVERTEXBUFFER9 g_VertexBuffer = NULL;

// A structure for our custom vertex type
struct stD3DVertex
{
    float x, y, z, rhw;
    unsigned long color;
};

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_VERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)


// Function prototypes.
void CALLBACK OnGUIEvent(UINT nEvent, int nControlID, CDXUTControl* pControl);
void RenderText();


void InitApp()
{
   // Used to help position controls.
   int iY = 10; 

   // Initialize main HUD.
   g_HUD.SetCallback(OnGUIEvent);
   g_HUD.AddButton(1, L"Toggle full screen", 35, iY, 125, 22);
   g_HUD.AddButton(2, L"Toggle REF (F3)", 35, iY += 24, 125, 22);
   g_HUD.AddButton(3, L"Change device (F2)", 35, iY += 24, 125, 22);

   iY = 10;

   // Initialize application GUI.
   g_SampleUI.SetCallback(OnGUIEvent);
   g_SampleUI.AddComboBox(4, 35, iY += 24, 125, 22);
   g_SampleUI.GetComboBox(4)->AddItem(L"Text 1", NULL);
   g_SampleUI.GetComboBox(4)->AddItem(L"Text 2", NULL);
   g_SampleUI.AddCheckBox(5, L"Checkbox1", 35, iY += 24, 125, 22);
   g_SampleUI.AddCheckBox(6, L"Checkbox2", 35, iY += 24, 125, 22);
   g_SampleUI.AddRadioButton(7, 1, L"Radio 1", 35, iY += 24, 125, 22);
   g_SampleUI.AddRadioButton(8, 1, L"Radio 2", 35, iY += 24, 125, 22);
   g_SampleUI.GetRadioButton(8)->SetChecked(true); 
   g_SampleUI.AddButton(9, L"Button 1", 35, iY += 24, 125, 22);
   g_SampleUI.AddButton(10, L"Button 2", 35, iY += 24, 125, 22);
   g_SampleUI.AddSlider(11, 50, iY += 24, 100, 22);
   g_SampleUI.GetSlider(11)->SetRange(0, 100);
   g_SampleUI.GetSlider(11)->SetValue(0);
}


bool CALLBACK IsDeviceAcceptable(D3DCAPS9* pCaps, D3DFORMAT AdapterFormat, 
                                 D3DFORMAT BackBufferFormat, bool bWindowed)
{
   // Check if alpha blending is supported on the hardware.
   IDirect3D9* pD3D = DXUTGetD3DObject(); 
   if(FAILED(pD3D->CheckDeviceFormat(pCaps->AdapterOrdinal, pCaps->DeviceType,
                           AdapterFormat, D3DUSAGE_QUERY_POSTPIXELSHADER_BLENDING, 
                           D3DRTYPE_TEXTURE, BackBufferFormat))) return false;
   return true;
}


void CALLBACK ModifyDeviceSettings(DXUTDeviceSettings* pDeviceSettings, const D3DCAPS9* pCaps)
{
   // Make sure the hardware supports hardware T&L or vertex shader version 1.1.
   if((pCaps->DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == 0 ||
   pCaps->VertexShaderVersion < D3DVS_VERSION(1,1))
      {
         pDeviceSettings->BehaviorFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
      }
   else
      pDeviceSettings->BehaviorFlags = D3DCREATE_HARDWARE_VERTEXPROCESSING;

   // If we can set to pure device.
   if((pCaps->DevCaps & D3DDEVCAPS_PUREDEVICE) != 0 && 
      (pDeviceSettings->BehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING) != 0)
         pDeviceSettings->BehaviorFlags |= D3DCREATE_PUREDEVICE;
}


HRESULT CALLBACK OnCreateDevice(IDirect3DDevice9* pd3dDevice, const D3DSURFACE_DESC* pBackBufferSurfaceDesc)
{
   HRESULT hr;

   // Initialize the font object.
   V_RETURN(D3DXCreateFont(pd3dDevice, 15, 0, FW_BOLD, 1, FALSE, DEFAULT_CHARSET, 
                           OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
                           L"Arial", &g_pFont));

   // Fill in our structure to draw a triangle.
   stD3DVertex objData[] =
      {
         { 320.0f, 150.0f, 0.5f, 1.0f, D3DCOLOR_XRGB(255, 255, 0), },
         { 420.0f, 350.0f, 0.5f, 1.0f, D3DCOLOR_XRGB(255, 0, 0), },
         { 220.0f, 350.0f, 0.5f, 1.0f, D3DCOLOR_XRGB(0, 255, 0), },
      };

   // Create the vertex buffer.
   V_RETURN(pd3dDevice->CreateVertexBuffer(3 * sizeof(stD3DVertex), 0,
            D3DFVF_VERTEX, D3DPOOL_DEFAULT, &g_VertexBuffer, NULL));

   // Fill the vertex buffer.
   void *ptr;
   V_RETURN(g_VertexBuffer->Lock(0, sizeof(objData), (void**)&ptr, 0));
   memcpy(ptr, objData, sizeof(objData));
   g_VertexBuffer->Unlock();

   return S_OK;
}


HRESULT CALLBACK OnResetDevice(IDirect3DDevice9* pd3dDevice, const D3DSURFACE_DESC* pBackBufferSurfaceDesc)
{
   HRESULT hr;

   if(g_pFont) V_RETURN(g_pFont->OnResetDevice());

   // Create a sprite to help batch calls when drawing many lines of text
   V_RETURN(D3DXCreateSprite(pd3dDevice, &g_pTextSprite));

   // Set UIs data.
   g_HUD.SetLocation(pBackBufferSurfaceDesc->Width - 170, 0);
   g_HUD.SetSize(170, 170);
   g_SampleUI.SetLocation(pBackBufferSurfaceDesc->Width - 170, pBackBufferSurfaceDesc->Height - 350);
   g_SampleUI.SetSize(170, 300);

   return S_OK;
}


void CALLBACK OnFrameMove(IDirect3DDevice9* pd3dDevice, double fTime, float fElapsedTime)
{
}


void CALLBACK OnFrameRender(IDirect3DDevice9* pd3dDevice, double fTime, float fElapsedTime)
{
   HRESULT hr;

   // Clear the buffers.
   V(pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                       D3DCOLOR_ARGB(0, 45, 50, 170), 1.0f, 0));

   // Begin rendering the scene.
   if(SUCCEEDED(pd3dDevice->BeginScene()))
      {
         // Render text and UI.
         DXUT_BeginPerfEvent(DXUT_PERFEVENTCOLOR, L"HUD / Stats");

            RenderText();
            V(g_HUD.OnRender(fElapsedTime));
            V(g_SampleUI.OnRender(fElapsedTime));

         DXUT_EndPerfEvent();

         // Render object.
         pd3dDevice->SetStreamSource(0, g_VertexBuffer, 0, sizeof(stD3DVertex));
         pd3dDevice->SetFVF(D3DFVF_VERTEX);
         pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);

         V(pd3dDevice->EndScene());
      }
}


void RenderText()
{
   //const D3DSURFACE_DESC* pd3dsdBackBuffer = DXUTGetBackBufferSurfaceDesc();
   CDXUTTextHelper txtHelper(g_pFont, g_pTextSprite, 15);

   // Output statistics
   txtHelper.Begin();
   txtHelper.SetInsertionPos(5, 5);
   txtHelper.SetForegroundColor(D3DXCOLOR(1.0f, 1.0f, 0.0f, 1.0f));
   txtHelper.DrawTextLine(DXUTGetFrameStats());
   txtHelper.DrawTextLine(DXUTGetDeviceStats());

   txtHelper.SetForegroundColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
   txtHelper.DrawTextLine(L"Ultimate Game Programming Sample (Press ESC to quit)");

   txtHelper.End();
}


LRESULT CALLBACK MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing)
{
   // Process messages through the HUD's first.
   *pbNoFurtherProcessing = g_HUD.MsgProc(hWnd, uMsg, wParam, lParam);
   if(*pbNoFurtherProcessing) return 0;
   *pbNoFurtherProcessing = g_SampleUI.MsgProc(hWnd, uMsg, wParam, lParam);
   if(*pbNoFurtherProcessing) return 0;

   return 0;
}


void CALLBACK KeyboardProc(UINT nChar, bool bKeyDown, bool bAltDown)
{
}


void CALLBACK OnGUIEvent(UINT nEvent, int nControlID, CDXUTControl* pControl)
{
   switch(nControlID)
      {
         case 1: DXUTToggleFullScreen(); break;
         case 2: DXUTToggleREF(); break;
         case 3: DXUTSetShowSettingsDialog(!DXUTGetShowSettingsDialog()); break;
      }
}


void CALLBACK OnLostDevice()
{
   if(g_pFont) g_pFont->OnLostDevice();
   SAFE_RELEASE(g_pTextSprite);
}


void CALLBACK OnDestroyDevice()
{
   SAFE_RELEASE(g_pFont);
   SAFE_RELEASE(g_VertexBuffer);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
   _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

   DXUTSetCallbackDeviceCreated(OnCreateDevice);
   DXUTSetCallbackDeviceReset(OnResetDevice);
   DXUTSetCallbackDeviceLost(OnLostDevice);
   DXUTSetCallbackDeviceDestroyed(OnDestroyDevice);
   DXUTSetCallbackMsgProc(MsgProc);
   DXUTSetCallbackKeyboard(KeyboardProc);
   DXUTSetCallbackFrameRender(OnFrameRender);
   DXUTSetCallbackFrameMove(OnFrameMove);

   // Show the cursor and clip it when in full screen
   DXUTSetCursorSettings(true, true);

   InitApp();

   // Initialize framework, create window (set title), create the device.
   DXUTInit(true, true, true);
   DXUTCreateWindow(WINDOW_NAME);
   DXUTCreateDevice(D3DADAPTER_DEFAULT, true, WINDOW_WIDTH, WINDOW_HEIGHT,
                    IsDeviceAcceptable, ModifyDeviceSettings);

   // Enter main loop.
   DXUTMainLoop();

   // Do app clean up before leading.
   return DXUTGetExitCode();
}