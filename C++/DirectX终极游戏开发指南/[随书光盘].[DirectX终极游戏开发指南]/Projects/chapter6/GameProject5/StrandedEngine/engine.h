/*
   Demo Name:  Game Project 5
      Author:  Allen Sherrod
     Chapter:  Chapter 6
*/


#ifndef _UGP_ENGINE_H_
#define _UGP_ENGINE_H_

#include"structs.h"
#include"RenderInterface.h"
#include"D3DRenderer.h"
#include"light.h"
#include"material.h"

#endif