/*
   Demo Name:  Frustum Culling
      Author:  Allen Sherrod
     Chapter:  Ch 15
*/


#include<d3d9.h>
#include<d3dx9.h>
#include<stdio.h>
#include"Frustum.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Frustum Culling"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

// Function Prototypes...
bool InitializeD3D(HWND hWnd, bool fullscreen);
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;

// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_modelMatrix;
D3DXMATRIX g_viewMatrix;

// Scene light source and material.
D3DLIGHT9 g_light;
D3DMATERIAL9 g_material;

// DirectX font object.
LPD3DXFONT g_font = NULL;

// RECT used to position the font.
RECT g_fontPosition = {0, 0, 0, 0};

// Mesh object we'll be drawing.
LPD3DXMESH g_mesh = NULL;

// Size of the cube.
float g_boxSize = 2;

// Frustum object used to test if object is in view.
CFrustum g_frustum;

// Used to move our object side to side.
float g_amount = 0;
float g_dir = 1;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Initialize Direct3D
   if(InitializeD3D(hWnd, false))
      {
         // Show the window
         ShowWindow(hWnd, SW_SHOWDEFAULT);
         UpdateWindow(hWnd);

         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D(HWND hWnd, bool fullscreen)
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(fullscreen)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;

   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
             D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &g_D3DDevice)))
      {
         return false;
      }

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);


   // Setup the light source.
   g_light.Type = D3DLIGHT_DIRECTIONAL;
   g_light.Direction = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
   g_light.Diffuse.r = g_light.Diffuse.g = 1;
   g_light.Diffuse.b = g_light.Diffuse.a = 1;
   g_light.Specular.r = g_light.Specular.g = 1;
   g_light.Specular.b = g_light.Specular.a = 1;

   // Register the light.
   g_D3DDevice->SetLight(0, &g_light);
   g_D3DDevice->LightEnable(0, TRUE);

   // Setup the material properties for the teapot.
   ZeroMemory(&g_material, sizeof(D3DMATERIAL9));
   g_material.Diffuse.r = g_material.Ambient.r = 0.6f;
   g_material.Diffuse.g = g_material.Ambient.g = 0.6f;
   g_material.Diffuse.b = g_material.Ambient.b = 0.7f;
   g_material.Specular.r = 0.4f;
   g_material.Specular.g = 0.4f;
   g_material.Specular.b = 0.4f;
   g_material.Power = 8.0f;


   // Create the objects.
   if(FAILED(D3DXCreateBox(g_D3DDevice, g_boxSize, g_boxSize,
      g_boxSize, &g_mesh, NULL))) return false;


   // Create the font.
   if(FAILED(D3DXCreateFont(g_D3DDevice, 18, 0, 0, 1, 0,
      DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY,
      DEFAULT_PITCH | FF_DONTCARE, "Arial",
      &g_font))) return false;
   
   // Here we are setting the position of the font.
   g_fontPosition.top = 0;
   g_fontPosition.left = 0;
   g_fontPosition.right = WINDOW_WIDTH;
   g_fontPosition.bottom = WINDOW_HEIGHT;


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, 45.0f,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0, 0, -5);
   D3DXVECTOR3 lookAtPos(0, 0, 0);
   D3DXVECTOR3 upDir(0, 1, 0);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_viewMatrix, &cameraPos,
                     &lookAtPos, &upDir);

   return true;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_viewMatrix);

      // Move the amounts so we can move the object.
      g_amount += (0.03f * g_dir);
      if(g_amount > 5 || g_amount < -5) g_dir *= -1;

      // Draw object.
      D3DXMatrixIdentity(&g_modelMatrix);
      D3DXMatrixTranslation(&g_modelMatrix, g_amount, 0, 0);
      g_D3DDevice->SetTransform(D3DTS_WORLD, &g_modelMatrix);
      g_D3DDevice->SetMaterial(&g_material);
      g_mesh->DrawSubset(0);

      char str[64] = { 0 };

      // Update the frustum (only needed if camera moved).
      D3DXMATRIX mv = g_modelMatrix * g_viewMatrix;
      g_frustum.CalculateFrustum((float*)&mv,
                                 (float*)&g_projection);

      // Test if object is visible.
      if(g_frustum.isBoxVisiable(g_amount, 0, 0, g_boxSize))
         sprintf(str, "Cube is visible.");
      else
         sprintf(str, "Cube is NOT visible.");

      // Set next position and draw text.
      g_fontPosition.top = 50;
      g_font->DrawText(NULL, "Frustum Culling Demo Application.",
                       -1, &g_fontPosition, DT_LEFT,
                       D3DCOLOR_XRGB(255,255,255));

      // Set next position and draw text.
      g_fontPosition.top = 80;
      g_font->DrawText(NULL, str, -1, &g_fontPosition, DT_LEFT,
                       D3DCOLOR_XRGB(255,255,255));

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   if(g_mesh != NULL) g_mesh->Release();
   g_mesh = NULL;

   if(g_font) g_font->Release();
   g_font = NULL;
}