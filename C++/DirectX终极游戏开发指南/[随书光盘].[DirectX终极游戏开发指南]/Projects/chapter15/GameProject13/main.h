/*
   Demo Name:  Game Project 13
      Author:  Allen Sherrod
     Chapter:  Chapter 15
*/


#ifndef _UGP_MAIN_H_
#define _UGP_MAIN_H_


#include"StrandedEngine/engine.h"
#pragma comment(lib, "lib/StrandedEngine.lib")

#define WINDOW_CLASS    "StrandedGame"
#define WINDOW_NAME     "Stranded"
#define WIN_WIDTH       800
#define WIN_HEIGHT      600
#define FULLSCREEN      1

// Function Prototypes...
bool InitializeEngine();
void ShutdownEngine();

// Menu functions.
bool InitializeMainMenu();
void MainMenuCallback(int id, int state);
void MainMenuRender();

// Input functions.
void ProcessInput();

// Main game functions.
bool GameInitialize();
void GameLoop();
void GameShutdown();
void GameReleaseAll();

// Level loading and rendering.
bool LoadLevel(char *file);
void LevelRender();


// Main menu defines.
#define GUI_MAIN_SCREEN       1
#define GUI_START_SCREEN      2
#define GUI_CREDITS_SCREEN    3

// ids for our GUI controls.
#define STATIC_TEXT_ID     1
#define BUTTON_START_ID    2
#define BUTTON_CREDITS_ID  3
#define BUTTON_QUIT_ID     4
#define BUTTON_BACK_ID     5
#define BUTTON_LEVEL_1_ID  6

struct stGameWorld
{
   stGameWorld() : m_levelID(-1), m_skyBoxID(-1) {}

   // Level id and skydome.
   int m_levelID;
   int m_skyBoxID;
};

#endif