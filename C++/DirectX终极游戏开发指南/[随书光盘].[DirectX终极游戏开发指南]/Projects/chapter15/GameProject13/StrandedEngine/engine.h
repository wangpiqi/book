/*
   Demo Name:  Game Project 13
      Author:  Allen Sherrod
     Chapter:  Chapter 15
*/


#ifndef _UGP_ENGINE_H_
#define _UGP_ENGINE_H_

#include"structs.h"
#include"RenderInterface.h"
#include"D3DRenderer.h"
#include"light.h"
#include"material.h"
#include"MathLibrary.h"
#include"InputInterface.h"
#include"DirectInput.h"
#include"SoundInterface.h"
#include"DirectMusic.h"
#include"CommandScript.h"
#include"PropertyScript.h"
#include"LogSystem.h"

#endif