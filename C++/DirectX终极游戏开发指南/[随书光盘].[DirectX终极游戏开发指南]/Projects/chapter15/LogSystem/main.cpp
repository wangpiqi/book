/*
   Demo Name:  Logging
      Author:  Allen Sherrod
     Chapter:  Ch 15
*/


#include<iostream>
#include"LogSystem.h"

using namespace std;


int main(int arg, char **argc)
{
   cout << "Writing logs." << endl << endl;

   CLogSystem log;

   log.WriteLog("log.txt", "This is something to write.\n");
   log.WriteLog("log.txt", "Bla bla bla bla bla\n");
   log.ClearLog("log.txt");

   log.WriteLog("log.txt", "Bla bla bla bla bla\n");
   log.WriteLog("log.txt", "This is something to write.\n");
   log.AppendLog("log.txt", "This was appended.\n");

   return 0;
}