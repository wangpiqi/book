/*
   Demo Name:  Tokens
      Author:  Allen Sherrod
     Chapter:  Ch 7
*/


#include<iostream>
#include"Token.h"

using namespace std;


int main(int arg, char **argc)
{
   // Open file for input.
   FILE *file = fopen("tokenFile.txt", "r");

   // Display errors if any.
   if(!file)
      {
         cout << "Error reading file." << endl << endl;
         return 0;
      }

   // Get the length of the file.
   fseek(file, 0, SEEK_END);
   int length = ftell(file);
   fseek(file, 0, SEEK_SET);

   // Read in all data from the file.
   char *data = new char[(length + 1) * sizeof(char)];
   if(!data) return NULL;
   fread(data, length, 1, file);
   data[length] = '\0';

   // Close the file when we are done.
   fclose(file);

   // Set our file to our lexer.
   CToken token;
   token.SetTokenStream(data);

   // No longer need.
   delete[] data; data = NULL;


   // Get total tokens.
   int totalTokens = 0;
   char buff[256] = { 0 };
   
   while(token.GetNextToken(buff))
      if(buff[0] != '\0') totalTokens++;


   // Print statistics.
   cout << "             File name: " << "Tokens.txt." << endl;
   cout << "    File size in bytes: " << length << "." << endl;
   cout << "Total number of tokens: " << totalTokens << "." << endl;

   cout << endl << endl;

   cout << "Token stream:" << endl << endl;

   // Reset.
   token.Reset();

   // Print all tokens.
   while(token.GetNextToken(buff))
      if(buff[0] != '\0') cout << buff << " ";


   // Release all memory.
   token.Shutdown();

   cout << endl << endl << endl;

   return 0;
}