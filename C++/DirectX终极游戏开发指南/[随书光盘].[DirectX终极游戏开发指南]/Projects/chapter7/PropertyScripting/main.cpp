/*
   Demo Name:  Property Scripting
      Author:  Allen Sherrod
     Chapter:  Ch 7
*/


#include<d3d9.h>
#include<d3dx9.h>
#include"PropertyScript.h"

#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Property Scripting"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

// Function Prototypes...
bool InitializeD3D(HWND hWnd, bool fullscreen);
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;

// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_ViewMatrix;
D3DXMATRIX g_WorldMatrix;

// Mesh objects
LPD3DXMESH g_teapot = NULL;
D3DMATERIAL9 g_material;

// Scene light source.
D3DLIGHT9 g_light;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
   switch(msg)
      {
         case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wp == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wp, lp);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE ph, LPSTR cmd, int s)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME,
      WS_OVERLAPPEDWINDOW, 100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
      GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Initialize Direct3D
   if(InitializeD3D(hWnd, false))
      {
         // Show the window
         ShowWindow(hWnd, SW_SHOWDEFAULT);
         UpdateWindow(hWnd);

         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D(HWND hWnd, bool fullscreen)
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT,
      &displayMode))) return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(fullscreen)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;

   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
      hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp,
      &g_D3DDevice))) return false;

   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, 45.0f,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);
   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
   // Property scripting system and properties for our object.
   CPropertyScript propertyScript;
   stVector teapotCol, camPos;

   // Load the script.
   if(!propertyScript.LoadScriptFile("script.spt"))
      return false;

   // Get the variables we need.
   teapotCol = propertyScript.GetVariableAsVector("teapotColor");
   camPos = propertyScript.GetVariableAsVector("cameraPosition");

   // Release all resources.  This script only needs to run once.
   propertyScript.Shutdown();


   // Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
   g_D3DDevice->SetRenderState(D3DRS_AMBIENT,
      D3DCOLOR_COLORVALUE(0.3f, 0.3f, 0.3f, 1.0f));

   // Setup the light source.
   g_light.Type = D3DLIGHT_DIRECTIONAL;
   g_light.Direction = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
   g_light.Diffuse.r = g_light.Diffuse.g = 1;
   g_light.Diffuse.b = g_light.Diffuse.a = 1;
   g_light.Specular.r = g_light.Specular.g = 1;
   g_light.Specular.b = g_light.Specular.a = 1;

   // Register the light.
   g_D3DDevice->SetLight(0, &g_light);
   g_D3DDevice->LightEnable(0, TRUE);

   // Setup the material properties for the teapot.
   ZeroMemory(&g_material, sizeof(D3DMATERIAL9));
   g_material.Ambient.r = 0.6f;
   g_material.Ambient.g = 0.6f;
   g_material.Ambient.b = 0.7f;
   g_material.Diffuse.r = teapotCol.x;
   g_material.Diffuse.g = teapotCol.y;
   g_material.Diffuse.b = teapotCol.z;
   g_material.Specular.r = 0.4f;
   g_material.Specular.g = 0.4f;
   g_material.Specular.b = 0.4f;
   g_material.Power = 8.0f;

   // Create the objects.
   if(FAILED(D3DXCreateTeapot(g_D3DDevice, &g_teapot, NULL)))
      return false;


   // Define camera information.
   D3DXVECTOR3 cameraPos(camPos.x, camPos.y, camPos.z);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos, &lookAtPos, &upDir);

   return true;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      // Draw teapot.
      g_D3DDevice->SetMaterial(&g_material);
      g_teapot->DrawSubset(0);

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   // Release all resources.
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   if(g_D3D != NULL) g_D3D->Release();
   if(g_teapot != NULL) { g_teapot->Release(); g_teapot = NULL; }
}