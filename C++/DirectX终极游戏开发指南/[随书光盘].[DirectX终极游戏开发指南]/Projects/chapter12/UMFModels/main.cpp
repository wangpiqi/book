/*
   Demo Name:  UMF Model Loading Demo
      Author:  Allen Sherrod
     Chapter:  Ch 12
*/


#include<d3d9.h>
#include<d3dx9.h>
#include"UMFLoader.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "UMF Model Loading"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480
#define FULLSCREEN      0

// Function Prototypes...
bool InitializeD3D();
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Global window handle.
HWND g_hwnd = 0;


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;


// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


// Vertex buffer to hold the geometry.
LPDIRECT3DVERTEXBUFFER9 *g_vertexBuffer = NULL;


// A structure for our custom vertex type
struct stD3DVertex
{
    float x, y, z;
    float nx, ny, nz;
    unsigned long color;
};

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_VERTEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE)


// Model we are loading.
stUMFModel *g_model;
int g_totalMeshes;

// These are the x and y rotations for our object.
float g_xRot = 0.0f;
float g_yRot = 0.0f;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   static POINT oldMousePos;
   static POINT currentMousePos;
   static bool isMouseActive;

   switch(msg)
      {
         case WM_DESTROY:
         case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;

         case WM_LBUTTONDOWN:
				oldMousePos.x = currentMousePos.x = LOWORD(lParam);
				oldMousePos.y = currentMousePos.y = HIWORD(lParam);
            isMouseActive = true;
			   break;

			case WM_LBUTTONUP:
				isMouseActive = false;
				break;

			case WM_MOUSEMOVE:
				currentMousePos.x = LOWORD (lParam);
				currentMousePos.y = HIWORD (lParam);

				if(isMouseActive)
					{
                  g_xRot -= (currentMousePos.x - oldMousePos.x);
						g_yRot -= (currentMousePos.y - oldMousePos.y);
					}

				oldMousePos.x = currentMousePos.x;
				oldMousePos.y = currentMousePos.y;
			   break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Show the window
   ShowWindow(hWnd, SW_SHOWDEFAULT);
   UpdateWindow(hWnd);

   // Record for global.
   g_hwnd = hWnd;

   // Initialize Direct3D
   if(InitializeD3D())
      {
         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D()
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(FULLSCREEN)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;
   d3dpp.EnableAutoDepthStencil = TRUE;
   d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hwnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_D3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);


   // Load the model file.
   if(!LoadUMF("Spheres.umf", &g_model, &g_totalMeshes))
      return false;
   if(!g_model || g_totalMeshes <= 0) return false;


   // Create a list of vertex buffers, one for every mesh.
   g_vertexBuffer = new LPDIRECT3DVERTEXBUFFER9[g_totalMeshes];
   if(!g_vertexBuffer) return false;


   // Loop through and fill vertex buffer.
   for(int m = 0; m < g_totalMeshes; m++)
      {
         // Allocate temp D3D array for model data.
         stD3DVertex *objData = new stD3DVertex[g_model[m].totalFaces * 3];
         int size = sizeof(stD3DVertex) * (g_model[m].totalFaces * 3);

         if(!objData) continue;

         // Copy model data into vertex buffer.
         for(int i = 0, f = 0; i < g_model[m].totalFaces * 3; i+=3, f++)
            {
               for(int k = 0; k < 3; k++)
                  {
                     int r = 0, g = 0, b = 0;
                     int index = g_model[m].faces[f].indices[k];

                     objData[i+k].x = g_model[m].vertices[index].x;
                     objData[i+k].y = g_model[m].vertices[index].y;
                     objData[i+k].z = g_model[m].vertices[index].z;
                     objData[i+k].nx = g_model[m].faces[f].normal.x;
                     objData[i+k].ny = g_model[m].faces[f].normal.y;
                     objData[i+k].nz = g_model[m].faces[f].normal.z;

                     if(g_model[m].colors)
                        {
                           r = (int)(g_model[m].colors[index].x * 255);
                           g = (int)(g_model[m].colors[index].y * 255);
                           b = (int)(g_model[m].colors[index].z * 255);
                        }

                     if(r < 0) r = 0;
                     if(g < 0) g = 0;
                     if(b < 0) b = 0;

                     objData[i+k].color = D3DCOLOR_XRGB(r, g, b);
                  }
            }

         // Create the vertex buffer.
         if(FAILED(g_D3DDevice->CreateVertexBuffer(size, 0,
                  D3DFVF_VERTEX, D3DPOOL_DEFAULT,
                  &g_vertexBuffer[m], NULL))) return false;
         
         // Fill the vertex buffer.
         void *ptr;
         if(FAILED(g_vertexBuffer[m]->Lock(0, size,
            (void**)&ptr, 0))) return false;
         memcpy(ptr, objData, size);
         g_vertexBuffer[m]->Unlock();

         if(objData) delete[] objData;
      }


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 1.0f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, -200.0f, 50.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      // Used to rotate by mouse.
      D3DXMATRIX rot, rotX, rotY;

      // Set the rot value to the matrix.  Convert deg to rad.
      D3DXMatrixRotationX(&rotX, -g_yRot / 180.0f * 3.141592654f);
      D3DXMatrixRotationY(&rotY, g_xRot / 180.0f * 3.141592654f);

      // Set the rotation matrix.
      rot = rotX * rotY;
      g_D3DDevice->SetTransform(D3DTS_WORLD, &rot);

      // Draw the model.
      for(int i = 0; i < g_totalMeshes; i++)
         {
            g_D3DDevice->SetStreamSource(0, g_vertexBuffer[i],
                                         0, sizeof(stD3DVertex));
            g_D3DDevice->SetFVF(D3DFVF_VERTEX);
            g_D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0,
                                       g_model[i].totalFaces);
         }

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   for(int i = 0; i < g_totalMeshes; i++)
      {
         if(g_vertexBuffer[i] != NULL) g_vertexBuffer[i]->Release();
         g_vertexBuffer[i] = NULL;
      }

   if(g_vertexBuffer) delete[] g_vertexBuffer;
   g_vertexBuffer = NULL;

   if(g_model)
      {
         FreeModel(g_model, g_totalMeshes);
         delete[] g_model;
         g_model = NULL;
      }
}