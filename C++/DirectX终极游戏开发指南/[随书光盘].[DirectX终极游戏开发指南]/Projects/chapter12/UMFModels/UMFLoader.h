/*
   Demo Name:  UMF Model Loading Demo
      Author:  Allen Sherrod
     Chapter:  Ch 12

   Description:

      Load the website's ultimate model format (UMF).  This model format is
      meant to be extermely simple to read and write to.

   File format:
      id (must be 9000)
      number of meshes

      (For each mesh:)
      diffuse, specular, power
      num verts
      vert data

      num normals
      vert normals data

      num colors
      vert colors data

      num tex coords
      tex coord data

      number of faces
      face data

      number of faces
      face data
*/


#ifndef _UMF_LOADER_H_
#define _UMF_LOADER_H_

#include<stdio.h>
#include<math.h>


struct stVector
{
   stVector() : x(0), y(0), z(0) {}
   float x, y, z;
};


struct stTexCoord
{
   stTexCoord() : tu(0), tv(0) {}
   // tu, tv texture coordinates.
   float tu, tv;
};


struct stFace
{
   stFace() { indices[0] = indices[1] = indices[2] = 0; }

   // Vertex indexes and a surface normal.
   unsigned int indices[3];
   stVector normal;
};


struct stUMFModel
{
   stUMFModel() : vertices(0), faces(0), normals(0), colors(0),
                  totalVertices(0), totalFaces(0), power(0),
                  texCoords(0) {}

   // Material data.
   stVector diffuse;
   stVector specular;
   int power;

   // Model data.
   stVector *vertices;
   stTexCoord *texCoords;
   stVector *normals;
   stVector *colors;
   stFace *faces;

   // Array counters.
   int totalVertices;
   int totalFaces;

   // Bounding box data.
   stVector bbMin, bbMax;
};


bool SaveUMF(char *file, stUMFModel *meshes, int numMeshes);
bool LoadUMF(char *file, stUMFModel **model, int *totalModels);
void FreeModel(stUMFModel *mesh);
void FreeModel(stUMFModel *meshes, int numMeshes);

#endif