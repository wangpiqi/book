xof 0302txt 0064


Material UgpMat {			   // Material #0
   1.0;1.0;1.0;1.0;;       // Face color
   0.0;                    // Power
   0.0;0.0;0.0;;           // Specular color
   0.0;0.0;0.0;;           // Emissive color
   TextureFilename{"ugp.bmp";}   // Texture file name.
}

// The square mesh data.
Mesh Square {
	8;                      // Number of vertices for the front face.
	1.0; 1.0; 0.0;,         // Vertice 0
	-1.0; 1.0; 0.0;,        // Vertice 1
	-1.0;-1.0; 0.0;,        // Vertice 2
	1.0;-1.0; 0.0;          // Vertice 3

	1.0; 1.0; 0.0;,         // Vertice 4
	1.0;-1.0; 0.0;,         // Vertice 5
	-1.0;-1.0; 0.0;,        // Vertice 6
	-1.0; 1.0; 0.0;;        // Vertice 7

	4;                      // Number of triangles
	3;0,1,2;,               // Triangle #1
	3;0,2,3;,               // Triangle #2
	3;4,5,6;,               // Triangle #3
	3;4,6,7;;               // Triangle #4

MeshMaterialList {
	1;                      // Number of materials
	4;                      // Number of faces
	0,                      // Face #0 use material #0 (UgpMat)
	0,                      // Face #1 use material #0
	0,                      // Face #2 use material #0
	0;;                     // Face #3 use material #0
	{UgpMat}				      // Reference the material we created above.
}

MeshTextureCoords {
	8;
	0.0; 0.0;,
	0.0; 1.0;,
	1.0; 1.0;,
	1.0; 0.0;;
	0.0; 0.0;,
	0.0; 1.0;,
	1.0; 1.0;,
	1.0; 0.0;;
}

}