/*
   Demo Name:  X Model Loading Demo
      Author:  Allen Sherrod
     Chapter:  Ch 12
*/


#include<d3d9.h>
#include<d3dx9.h>

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "X Model Loading"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480
#define FULLSCREEN      0

// Function Prototypes...
bool InitializeD3D();
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Global window handle.
HWND g_hwnd = 0;


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;


// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


// Display object.
LPD3DXMESH g_model = NULL;
DWORD g_numMaterials;
LPD3DXBUFFER g_matBuffer = NULL;
D3DMATERIAL9* g_matList = NULL;
LPDIRECT3DTEXTURE9* g_textureList = NULL;

// Scene light source.
D3DLIGHT9 g_light;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
         case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Show the window
   ShowWindow(hWnd, SW_SHOWDEFAULT);
   UpdateWindow(hWnd);

   // Record for global.
   g_hwnd = hWnd;

   // Initialize Direct3D
   if(InitializeD3D())
      {
         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D()
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(FULLSCREEN)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;
   d3dpp.EnableAutoDepthStencil = TRUE;
   d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hwnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{

	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_D3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);


   // Setup the g_light source and material.
   g_light.Type = D3DLIGHT_DIRECTIONAL;
   g_light.Direction = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

   D3DCOLORVALUE white;
   white.a = white.r = white.g = white.b = 1;

   g_light.Diffuse = white;
   g_light.Specular = white;

   g_D3DDevice->SetLight(0, &g_light);
   g_D3DDevice->LightEnable(0, TRUE);


   // Create mesh.
   D3DVERTEXELEMENT9 elements[] =
      {
         { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
         { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
         { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
         D3DDECL_END()
      };


   // Load mesh into temp object then copy over (to set elements).
	LPD3DXMESH temp = NULL;
	if(FAILED(D3DXLoadMeshFromX("Model.x", D3DXMESH_SYSTEMMEM,
	          g_D3DDevice, NULL, &g_matBuffer, NULL,
             &g_numMaterials, &temp))) return false;
	temp->CloneMesh(D3DXMESH_SYSTEMMEM, elements, g_D3DDevice, &g_model);
	if(temp) temp->Release();

   // Calculate normals for lighting.
   D3DXComputeNormals(g_model, NULL);

   // Allocate the lists for materials and textures.
   g_matList = new D3DMATERIAL9[g_numMaterials];
   g_textureList  = new LPDIRECT3DTEXTURE9[g_numMaterials];

   // Get a pointer to the buffer
   D3DXMATERIAL* mat = (D3DXMATERIAL*)g_matBuffer->GetBufferPointer();

   // Loop and load each textture and get each material.
   for(DWORD i = 0; i < g_numMaterials; i++)
      {
         // Copy the materials from the buffer into our list.
         g_matList[i] = mat[i].MatD3D;

         // Load the textures into the list.
         if(FAILED(D3DXCreateTextureFromFile(g_D3DDevice,
                   mat[i].pTextureFilename,
                   &g_textureList[i]))) g_textureList[i] = NULL;
      }
   

   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, 0.0f, -10.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      // Draw the model.
      for(DWORD i = 0; i < g_numMaterials; i++)
         {
            g_D3DDevice->SetMaterial(&g_matList[i]);
            g_D3DDevice->SetTexture(0, g_textureList[i]);

            g_model->DrawSubset(i);
         }

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   if(g_model != NULL) g_model->Release();
   g_model = NULL;

   for(DWORD i = 0; i < g_numMaterials; i++)
      {
         if(g_textureList[i] != NULL)
            {
               g_textureList[i]->Release();
               g_textureList[i] = NULL;
            }
      }

   if(g_matList != NULL)
      {
         delete[] g_matList;
         g_matList = NULL;
      }

   if(g_textureList != NULL)
      {
         delete[] g_textureList;
         g_textureList = NULL;
      }

   if(g_matBuffer != NULL)
      {
         g_matBuffer->Release();
         g_matBuffer = NULL;
      }
}