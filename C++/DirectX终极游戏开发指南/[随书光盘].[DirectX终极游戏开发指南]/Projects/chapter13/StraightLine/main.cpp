/*
   Demo Name:  Straight Line Animation Demo
      Author:  Allen Sherrod
     Chapter:  Ch 13
*/


#include<d3d9.h>
#include<d3dx9.h>
#include<mmsystem.h>

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "winmm.lib")


#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Straight Line Animation"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480
#define FULLSCREEN      0

// Function Prototypes...
bool InitializeD3D();
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Global window handle.
HWND g_hwnd = 0;


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;


// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


// Display object.
LPD3DXMESH g_model = NULL;


//
struct stVector
{
   stVector() : x(0), y(0), z(0) {}
   float x, y, z;
};


// Path will hold the start and end position of our animation.
stVector Path[4];
stVector objPos;

// CurrentPath will hold which of the two striaght line paths we are on.
int CurrentPath = 1;

// Used for time based calculations.
float StartTime = 0;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
         case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Show the window
   ShowWindow(hWnd, SW_SHOWDEFAULT);
   UpdateWindow(hWnd);

   // Record for global.
   g_hwnd = hWnd;

   // Initialize Direct3D
   if(InitializeD3D())
      {
         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D()
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(FULLSCREEN)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;
   d3dpp.EnableAutoDepthStencil = TRUE;
   d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hwnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_D3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);


   // Here we are setting our striaght line path.
   Path[0].x = -2.5f; Path[0].y = 1.0f; Path[0].z = 0.0f;
   Path[1].x = 2.5f; Path[1].y = 1.0f; Path[1].z = 0.0f;
   Path[2].x = 2.5f; Path[2].y = 1.0f; Path[2].z = 0.0f;
   Path[3].x = -2.5f; Path[3].y = 1.0f; Path[3].z = 0.0f;

   // Initialize the start time for this simulation.
   StartTime = (float)timeGetTime();


   // Create mesh.
   if(FAILED(D3DXCreateBox(g_D3DDevice, 1, 1, 1, &g_model, NULL)))
      return false;


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, 0.0f, -10.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      // Update the time.  Since we are using time based movements
      // and calculations we get the time, sub from the start time,
      // then multiply that by the speed of the simulation.
      float Scalar = (float)timeGetTime();
      Scalar = (Scalar - StartTime) * 0.003f;

      // Get the new object position by using a simple interpolation equation.  If we are
      // in the first path, then we use the first path data, else the second path data.
      if(CurrentPath == 1)
         {
            // We must divide the scalar by the length of the path so our animation is
            // smooth and correct.  Then we use the interpolation calculation to determine
            // the position.
            stVector diff;
            diff.x = Path[1].x - Path[0].x;
            diff.y = Path[1].y - Path[0].y;
            diff.z = Path[1].z - Path[0].z;

            float len = (float)sqrt((diff.x * diff.x +
                                     diff.y * diff.y +
                                     diff.z * diff.z));

            Scalar = Scalar / len;
            objPos.x = (Path[1].x - Path[0].x) * Scalar + Path[0].x;
            objPos.y = (Path[1].y - Path[0].y) * Scalar + Path[0].y;
            objPos.z = (Path[1].z - Path[0].z) * Scalar + Path[0].z;
         }
      else
         {
            stVector diff;
            diff.x = Path[3].x - Path[2].x;
            diff.y = Path[3].y - Path[2].y;
            diff.z = Path[3].z - Path[2].z;

            float len = (float)sqrt((diff.x * diff.x +
                                     diff.y * diff.y +
                                     diff.z * diff.z));

            Scalar = Scalar / len;
            objPos.x = (Path[3].x - Path[2].x) * Scalar + Path[2].x;
            objPos.y = (Path[3].y - Path[2].y) * Scalar + Path[2].y;
            objPos.z = (Path[3].z - Path[2].z) * Scalar + Path[2].z;
         }

      // If Scalar reaches 1.0f then that means we are at the end of the animation.  We
      // simply reset the start time then increase the value that represents which path
      // we are on.  Since we have only two paths we then check to make sure we dont go
      // over that value.
      if(Scalar >= 1.0f)
      {
         // Reset the start time.
         StartTime = (float)timeGetTime();

         // Move to the next path.
         CurrentPath++;

         // We only have 2 paths so make sure we don't go over that number.
         if(CurrentPath > 2) CurrentPath = 1;
      }

      D3DXMATRIX mat;
      D3DXMatrixTranslation(&mat, objPos.x, objPos.y, objPos.z);
      g_D3DDevice->SetTransform(D3DTS_WORLD, &mat);

      // Draw the model.
      g_model->DrawSubset(0);

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   if(g_model != NULL) g_model->Release();
   g_model = NULL;
}