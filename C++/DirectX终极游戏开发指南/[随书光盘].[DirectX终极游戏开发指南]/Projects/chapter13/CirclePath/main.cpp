/*
   Demo Name:  Circle Path Animation Demo
      Author:  Allen Sherrod
     Chapter:  Ch 13
*/


#include<d3d9.h>
#include<d3dx9.h>
#include<mmsystem.h>
#include"Vector.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "winmm.lib")


#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Circle Path Animation"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480
#define FULLSCREEN      0
#define M_PI            3.141592654f

// Function Prototypes...
bool InitializeD3D();
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Global window handle.
HWND g_hwnd = 0;


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;


// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


// Display object.
LPD3DXMESH g_model = NULL;


// Center of circle path.
CVector3 g_center(0.0f, 0.0f, 0.0f);

// Start position of the animation and the point in the plane (direction).
CVector3 start(2.0f, 0.0f, 0.0f);
CVector3 planeDirection(0.0f, 0.0f, 1.0f);

// u and v are orthonormal vectors in the plane.
// u points from the center to the start and v is perpendicular to u.
CVector3 g_u, g_v;

// Circle's radius.
float g_radius = 0;

// objPos will hold the objects current position on the curve path.
CVector3 objPos;

// Used for time based calculations.
float StartTime = 0;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
         case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Show the window
   ShowWindow(hWnd, SW_SHOWDEFAULT);
   UpdateWindow(hWnd);

   // Record for global.
   g_hwnd = hWnd;

   // Initialize Direct3D
   if(InitializeD3D())
      {
         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D()
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(FULLSCREEN)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;
   d3dpp.EnableAutoDepthStencil = TRUE;
   d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hwnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_D3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);


   // First we make sure that the center, start pos, and point in the plane are colinear.
	CVector3 planeNormal;
   planeNormal.CrossProduct(start - g_center, planeDirection - g_center);

   // Check if they are colinear.
	if(planeNormal.GetLength() < 0.01f)
	   {
         MessageBox(0, "Circle path must be colinear!", "Error...", MB_OK);
		   return false;
	   }

   // If they are then we normalize and continue the calculation.
	planeNormal.Normal();

	// The radius will be determined by where the center of the circle is and the start
   // position of the animation.  The u and v are orthonormal vectors.  u goes from
   // the start to center while v is perpendicular to the u.
	g_radius = (start - g_center).GetLength();
	g_u = (start - g_center);
   g_u.Normal();
	g_v.CrossProduct(g_u, planeNormal);

   // Initialize the start time for this simulation.
   StartTime = (float)timeGetTime();


   // Create mesh.
   if(FAILED(D3DXCreateBox(g_D3DDevice, 1, 1, 1, &g_model, NULL)))
      return false;


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, 0.0f, -10.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


CVector3 CalcCirclePos(float angle, CVector3 center, CVector3 u,
                       CVector3 v, float radius)
{
   float newAngle = angle * (float)(M_PI / 180);
	return center + u * radius * float(cos(newAngle)) + v * radius * float(sin(newAngle));
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      // First we must calculate the object position along the
      // circle.  Since we are using time based movements for
      // everything we first get the time in seconds.  The value 0.06 will
      // act as the speed of the animation.
      float Time = (float)timeGetTime();
      Time = (Time - StartTime) * 0.06f;

      // Now we calculate the position on the curve by taking the circle path data and
      // using the scalar Time to dictate where on that circle (the angle) the position lies.
      objPos = CalcCirclePos(Time, g_center, g_u, g_v, g_radius);

      D3DXMATRIX mat;
      D3DXMatrixTranslation(&mat, objPos.x, objPos.y, objPos.z);
      g_D3DDevice->SetTransform(D3DTS_WORLD, &mat);

      // Draw the model.
      g_model->DrawSubset(0);

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   if(g_model != NULL) g_model->Release();
   g_model = NULL;
}