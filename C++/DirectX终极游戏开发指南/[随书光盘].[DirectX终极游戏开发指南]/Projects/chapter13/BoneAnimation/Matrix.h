/*
   Demo Name:  Bone Animation Demo
      Author:  Allen Sherrod
     Chapter:  Ch 13
*/


#ifndef CMATRIX_H
#define CMATRIX_H

#include"Vector.h"


class CMatrix4x4
{
   public:
      CMatrix4x4();                                // Constructor.
      CMatrix4x4(const CMatrix4x4 &m);             // Overloaded constructors.
      CMatrix4x4(float r11, float r12, float r13, float r14,
                 float r21, float r22, float r23, float r24,
                 float r31, float r32, float r33, float r34,
                 float r41, float r42, float r43, float r44);
      ~CMatrix4x4();                               // Destructor.

      void Clear();                                // Reset the matrix.

      void operator=(CMatrix4x4 m);                // Overloaded = sign.
      CMatrix4x4 operator*(CMatrix4x4 m);          // Overloaded * sign.

      void Translate(float x, float y, float z);   // Translate a point in 3D.
      void inverseTranslate();                     // Translate the opposite way.
      void Rotate(double angle, float x, float y,
                  float z);                        // Rotate a 3D point.

      CVector3 VectorMatrixMultiply(CVector3 v);   // Vector matrix multiply.
      CVector3 VectorMatrixMultiply3x3(CVector3 v);// Vector matrix multiply 3x3 matrix.

      float matrix[16];                            // The 4x4 matrix in a 1D array.
};

#endif