/*
   Demo Name:  Bone Animation Demo
      Author:  Allen Sherrod
     Chapter:  Ch 13
*/


#include<d3d9.h>
#include<d3dx9.h>
#include"Bone.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Bone Animation"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480
#define FULLSCREEN      0

// Function Prototypes...
bool InitializeD3D();
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Global window handle.
HWND g_hwnd = 0;


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;


// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


// Vertex stucture.
struct Vertex
{
   FLOAT x, y, z;
   DWORD color;
};

#define MAX_BONES 2

// g_xRot and g_yRot is used to rotate g_bones.
float g_xRot = 0.0f;
float g_yRot = 0.0f;

// Used to move our model with the arrow keys.
CVector3 g_trans;

// Hierarchy of g_bones kept in a simple array instead of a hierarchy data structure.
CBone g_bones[MAX_BONES];


#define D3DFVF_D3DVertex (D3DFVF_XYZ | D3DFVF_DIFFUSE)


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   static POINT oldMousePos;
   static POINT currentMousePos;
   static bool isMouseActive;

   switch(msg)
      {
         case WM_DESTROY:
         case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
            break;

         case WM_KEYUP:
            if(wParam == VK_ESCAPE) PostQuitMessage(0);
            break;

         case WM_KEYDOWN:
		      switch(wParam) 
		         {
                  case VK_UP:
                     g_trans.z -= 0.5f;
                     break;

                  case VK_DOWN:
                     g_trans.z += 0.5f;
                     break;

                  case VK_LEFT:
                     g_trans.x += 0.5f;
                     break;

                  case VK_RIGHT:
                     g_trans.x -= 0.5f;
                     break;
		         }
            break;

         case WM_LBUTTONDOWN:
				oldMousePos.x = currentMousePos.x = LOWORD (lParam);
				oldMousePos.y = currentMousePos.y = HIWORD (lParam);
            isMouseActive = true;

			   break;


			case WM_LBUTTONUP:
				isMouseActive = false;

				break;


			case WM_MOUSEMOVE:
				currentMousePos.x = LOWORD (lParam);
				currentMousePos.y = HIWORD (lParam);

				if(isMouseActive)
					{
                  g_xRot -= (currentMousePos.x - oldMousePos.x);
						g_yRot -= (currentMousePos.y - oldMousePos.y);
					}

				oldMousePos.x = currentMousePos.x;
				oldMousePos.y = currentMousePos.y;

			   break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Show the window
   ShowWindow(hWnd, SW_SHOWDEFAULT);
   UpdateWindow(hWnd);

   // Record for global.
   g_hwnd = hWnd;

   // Initialize Direct3D
   if(InitializeD3D())
      {
         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D()
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(FULLSCREEN)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;
   d3dpp.EnableAutoDepthStencil = TRUE;
   d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hwnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_D3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);


   // Seeing how our object is simple, we can just create each bone like so.
   // Our SetBone function takes the parent index and bone length.
   // SetBone create each bone for us.
   g_bones[0].SetBone(-1, 4.5f);
   g_bones[1].SetBone(0, 4.5f);


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, 4.0f, -15.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 4.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


void UpdateBones()
{
   // Temp matrices to hold rotations and translations.
   CMatrix4x4 rotTemp1, rotTemp2, tempRelative;

   // Here we will loop through the list of g_bones and
   // update the skeleton.
   for(int i = 0; i < MAX_BONES; i++)
      {
         // Only the root bone will have a -1 value for its
         // parent.  Root g_bones pretty much are directly
         // translated and rotated but it does not
         // inherit anything.
         if(g_bones[i].parent == -1)
            {
               // Manipulate the position of the root bone.
               g_bones[i].relative.Translate(g_trans.x, g_trans.y, g_trans.z);
               g_bones[i].absolute = g_bones[i].relative;
            }
         else
            {
               // Reset the bone's relative matrix.
               g_bones[i].relative.Clear();

               // First move this bone into position from parent.
               g_bones[i].relative.Translate(0, g_bones[g_bones[i].parent].length * i, 0);

               // Rotate the non-root g_bones with the mouse.
               rotTemp1.Rotate(g_xRot, 0, 1, 0);
               rotTemp2.Rotate(g_yRot, 0, 0, 1);

               // Inverse translate the relative matrix.
               tempRelative = g_bones[i].relative;
               tempRelative.inverseTranslate();

               // Calculate new relative matrix.
               g_bones[i].relative = g_bones[i].relative * (rotTemp1 * rotTemp2) * tempRelative;
               
               // Calculate final matrix (absolute).
               g_bones[i].absolute = g_bones[g_bones[i].parent].absolute * g_bones[i].relative;
            }
      }
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      // Update the bone list.
      UpdateBones();

      // Loop through and draw each bone.  This is for display
      // purposes only just so we can see the bones.
      for(int i = 0; i < MAX_BONES; i++)
         {
            g_D3DDevice->SetTransform(D3DTS_WORLD,
               (D3DMATRIX*)g_bones[i].absolute.matrix);
      
            unsigned long col = D3DCOLOR_XRGB(255,255,255);
      
      
            // Generate mesh to represent bones.
      
      
            // Draw a quad in lines.
            Vertex baseLines[] = {
               -0.4f, 0 + (g_bones[i].length * i), -0.4f, col,
               0.4f, 0 + (g_bones[i].length * i), -0.4f, col,
               0.4f, 0 + (g_bones[i].length * i), -0.4f, col,
               0.4f, 0 + (g_bones[i].length * i), 0.4f, col,
               0.4f, 0 + (g_bones[i].length * i), 0.4f, col,
               -0.4f, 0 + (g_bones[i].length * i), 0.4f, col,
               -0.4f, 0 + (g_bones[i].length * i), 0.4f, col,
               -0.4f, 0 + (g_bones[i].length * i), -0.4f, col };

            // Now from each end point go up until the bone length.
            Vertex coneLines[] = {
               -0.4f, 0 + (g_bones[i].length * i), -0.4f, col,
               0, g_bones[i].length + (g_bones[i].length * i), 0, col,
               0.4f, 0 + (g_bones[i].length * i), -0.4f, col,
               0, g_bones[i].length + (g_bones[i].length * i), 0, col,
               0.4f, 0 + (g_bones[i].length * i), 0.4f, col,
               0, g_bones[i].length + (g_bones[i].length * i), 0, col,
               -0.4f, 0 + (g_bones[i].length * i), 0.4f, col,
               0, g_bones[i].length + (g_bones[i].length * i), 0, col, };

            g_D3DDevice->SetFVF(D3DFVF_D3DVertex);
            g_D3DDevice->DrawPrimitiveUP(D3DPT_LINELIST, 4,
                                         baseLines, sizeof(Vertex));
            g_D3DDevice->DrawPrimitiveUP(D3DPT_LINELIST, 4,
                                         coneLines, sizeof(Vertex));
         }

   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;
}