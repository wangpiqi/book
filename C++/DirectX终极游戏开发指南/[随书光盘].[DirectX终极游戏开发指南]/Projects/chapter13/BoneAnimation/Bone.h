/*
   Demo Name:  Bone Animation Demo
      Author:  Allen Sherrod
     Chapter:  Ch 13
*/


#ifndef CBONE_H
#define CBONE_H

#include"Matrix.h"


class CBone
{
   public:
      CBone() : parent(0), length(0) { }
      
      void SetBone(int P, float L)
      {
         // Set the bone data.
         parent = P; length = L;
      }

      void SetBone(int P, float L,
                   CMatrix4x4 R, CMatrix4x4 A)
      {
         // Set the bone data.
         parent = P; length = L; relative = R; absolute = A;
      }


      int parent;
      float length;

      CMatrix4x4 relative;
      CMatrix4x4 absolute;
};

#endif