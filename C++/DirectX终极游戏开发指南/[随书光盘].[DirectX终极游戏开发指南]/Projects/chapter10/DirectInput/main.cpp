/*
   Demo Name:  Direct Input Demo
      Author:  Allen Sherrod
     Chapter:  Ch 10
*/


#include<d3d9.h>
#include<d3dx9.h>
#include"CDirectInput.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#define WINDOW_CLASS    "UGPDX"
#define WINDOW_NAME     "Input Detection using input"
#define WINDOW_WIDTH    640
#define WINDOW_HEIGHT   480

// Function Prototypes...
bool InitializeD3D(HWND hWnd, bool fullscreen);
bool InitializeObjects();
void RenderScene();
void Shutdown();


// Used for input initialize.
HWND g_hwnd = 0;
HINSTANCE g_hInstance = 0;


// Direct3D object and device.
LPDIRECT3D9 g_D3D = NULL;
LPDIRECT3DDEVICE9 g_D3DDevice = NULL;


// Matrices.
D3DXMATRIX g_projection;
D3DXMATRIX g_worldMatrix;
D3DXMATRIX g_ViewMatrix;


// Defines for which object we will display.
#define CUBE   0
#define SPHERE 1
#define TEAPOT 2


// Display objects.
LPD3DXMESH g_teapot = NULL;
LPD3DXMESH g_cube = NULL;
LPD3DXMESH g_sphere = NULL;


// Scene light source and material.
D3DLIGHT9 g_light;
D3DMATERIAL9 g_mat;


// These are the x and y rotations for our objects.
float g_xRot = 0.0f;
float g_yRot = 0.0f;

// This will allow us to cycle through the objects for display.
int g_object = 0;


// Here is our input system.
CDirectInputSystem input;


// Position of the mouse and game controller joysticks.
POINT g_lastPos, g_Pos;
POINT g_leftPos, g_rightPos;


LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
      {
         case WM_DESTROY:
         case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
            break;
      }

   return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE prevhInst, LPSTR cmdLine, int show)
{
   // Register the window class
   WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     WINDOW_CLASS, NULL };
   RegisterClassEx(&wc);

   // Create the application's window
   HWND hWnd = CreateWindow(WINDOW_CLASS, WINDOW_NAME, WS_OVERLAPPEDWINDOW,
                            100, 100, WINDOW_WIDTH, WINDOW_HEIGHT,
                            GetDesktopWindow(), NULL, wc.hInstance, NULL);

   // Show the window
   ShowWindow(hWnd, SW_SHOWDEFAULT);
   UpdateWindow(hWnd);

   // Record for globals.
   g_hwnd = hWnd;
   g_hInstance = hInst;

   // Initialize Direct3D
   if(InitializeD3D(hWnd, false))
      {
         // Enter the message loop
         MSG msg;
         ZeroMemory(&msg, sizeof(msg));

         while(msg.message != WM_QUIT)
            {
               if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
                  {
                     TranslateMessage(&msg);
                     DispatchMessage(&msg);
                  }
               else
                  RenderScene();
            }
      }

   // Release any and all resources.
   Shutdown();

   // Unregister our window.
   UnregisterClass(WINDOW_CLASS, wc.hInstance);
   return 0;
}


bool InitializeD3D(HWND hWnd, bool fullscreen)
{
   D3DDISPLAYMODE displayMode;

   // Create the D3D object.
   g_D3D = Direct3DCreate9(D3D_SDK_VERSION);
   if(g_D3D == NULL) return false;

   // Get the desktop display mode.
   if(FAILED(g_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)))
      return false;

   // Set up the structure used to create the D3DDevice
   D3DPRESENT_PARAMETERS d3dpp;
   ZeroMemory(&d3dpp, sizeof(d3dpp));

   if(fullscreen)
      {
         d3dpp.Windowed = FALSE;
         d3dpp.BackBufferWidth = WINDOW_WIDTH;
         d3dpp.BackBufferHeight = WINDOW_HEIGHT;
      }
   else
      d3dpp.Windowed = TRUE;
   d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
   d3dpp.BackBufferFormat = displayMode.Format;
   d3dpp.BackBufferCount = 1;
   d3dpp.EnableAutoDepthStencil = TRUE;
   d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


   // Create the D3DDevice
   if(FAILED(g_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
             D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE,
             &d3dpp, &g_D3DDevice))) return false;

   // Initialize any objects we will be displaying.
   if(!InitializeObjects()) return false;

   return true;
}


bool InitializeObjects()
{
   // Initialize input.
   if(!input.Initialize(g_hwnd, g_hInstance))
      return false;


	// Set default rendering states.
   g_D3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	g_D3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_D3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
	g_D3DDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_COLORVALUE(0.3f, 0.3f, 0.3f, 1.0f));


   // Setup the g_light source and material.
   g_light.Type = D3DLIGHT_DIRECTIONAL;
   g_light.Direction = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

   g_light.Diffuse.r = g_light.Diffuse.g = g_light.Diffuse.b = g_light.Diffuse.a = 1.0f;
   g_light.Specular.r = g_light.Specular.g = g_light.Specular.b = g_light.Specular.a = 1.0f;

   g_D3DDevice->SetLight(0, &g_light);
   g_D3DDevice->LightEnable(0, TRUE);

   ZeroMemory(&g_mat, sizeof(D3DMATERIAL9));
   g_mat.Diffuse.r = g_mat.Ambient.r = 0.6f;
   g_mat.Diffuse.g = g_mat.Ambient.g = 0.6f;
   g_mat.Diffuse.b = g_mat.Ambient.b = 0.7f;
   g_mat.Specular.r = g_mat.Specular.g = g_mat.Specular.b = 0.4f;
   g_mat.Power = 8.0f;


   // Create meshes.
   if(FAILED(D3DXCreateTeapot(g_D3DDevice, &g_teapot, NULL)))
      return false;
   if(FAILED(D3DXCreateBox(g_D3DDevice, 3, 3, 3, &g_cube, NULL)))
      return false;
   if(FAILED(D3DXCreateSphere(g_D3DDevice, 3, 25, 25, &g_sphere, NULL)))
      return false;


   // Set the projection matrix.
   D3DXMatrixPerspectiveFovLH(&g_projection, D3DX_PI / 4,
      WINDOW_WIDTH/WINDOW_HEIGHT, 0.1f, 1000.0f);

   g_D3DDevice->SetTransform(D3DTS_PROJECTION, &g_projection);


   // Define camera information.
   D3DXVECTOR3 cameraPos(0.0f, 0.0f, -10.0f);
   D3DXVECTOR3 lookAtPos(0.0f, 0.0f, 0.0f);
   D3DXVECTOR3 upDir(0.0f, 1.0f, 0.0f);

   // Build view matrix.
   D3DXMatrixLookAtLH(&g_ViewMatrix, &cameraPos,
                      &lookAtPos, &upDir);

   return true;
}


void GetInput()
{
   // Update all devices.
   input.UpdateDevices();

   // Keyboard input.
   if(input.KeyDown(DIK_ESCAPE)) PostQuitMessage(0);
   if(input.KeyDown(DIK_1)) g_object = CUBE;
   if(input.KeyDown(DIK_2)) g_object = SPHERE;
   if(input.KeyDown(DIK_3)) g_object = TEAPOT;


   // Mouse.
   g_Pos = input.GetMousePos();

   if(input.MouseButtonDown(LEFT_BUTTON))
		{
         g_xRot -= (g_Pos.x - g_lastPos.x);
			g_yRot -= (g_Pos.y - g_lastPos.y);
		}

   // Record last mouse pos.
	g_lastPos.x = g_Pos.x;
	g_lastPos.y = g_Pos.y;

   // Get mouse wheel movements.
	long mouseWheel = input.GetMouseWheelPos();
   if(mouseWheel < 0) g_object--;
   else if(mouseWheel > 0) g_object++;


	// Game controller.
	g_leftPos = input.GetLeftStickPos();
	g_rightPos = input.GetRightStickPos();

   // Rotation amount for joysticks.
   float rotAmt = 0;

   // If the left stick (or joystick) was moved
   // to the left then set the rotation amout to a neg number.
   // Depending on how far the stickw as moved will depend on
   // how fast to rotate.
   if(g_leftPos.x < -50) rotAmt = -0.5f;
   if(g_leftPos.x < -150) rotAmt = -1.5f;
   if(g_leftPos.x < -250) rotAmt = -2.5f;
   if(g_leftPos.x < -350) rotAmt = -3.5f;

   // Else if it was moved right, set it to a positive value.
   if(g_leftPos.x > 50) rotAmt = 0.5f;
   if(g_leftPos.x > 150) rotAmt = 1.5f;
   if(g_leftPos.x > 250) rotAmt = 2.5f;
   if(g_leftPos.x > 350) rotAmt = 3.5f;

   // Apply the rotation to the g_xRot and reset the rotAmt.
   g_xRot += rotAmt;
   rotAmt = 0;

   // Do the same with the y axis of the stick.
   if(g_leftPos.y < -50) rotAmt = -0.5f;
   if(g_leftPos.y < -150) rotAmt = -1.5f;
   if(g_leftPos.y < -250) rotAmt = -2.5f;
   if(g_leftPos.y < -350) rotAmt = -3.5f;

   if(g_leftPos.y > 50) rotAmt = 0.5f;
   if(g_leftPos.y > 150) rotAmt = 1.5f;
   if(g_leftPos.y > 250) rotAmt = 2.5f;
   if(g_leftPos.y > 350) rotAmt = 3.5f;

   // Apply y rotation and clear rotAmt.
   g_yRot += rotAmt;
   rotAmt = 0;

   // Do the same thing we did for the left stick
   // with the right stick assuming one exist.
   // The joysticks center is diffrent so we do
   // 32767 minus whatever amount the joystick was
   // moved to compensate.
   if(g_rightPos.x < 32767 - 500) rotAmt = -0.5f;
   if(g_rightPos.x < 32767 - 1000) rotAmt = -1.5f;
   if(g_rightPos.x < 32767 - 1500) rotAmt = -2.5f;
   if(g_rightPos.x < 32767 - 2000) rotAmt = -3.5f;

   if(g_rightPos.x > 32767 + 500) rotAmt = 0.5f;
   if(g_rightPos.x > 32767 + 1000) rotAmt = 1.5f;
   if(g_rightPos.x > 32767 + 1500) rotAmt = 2.5f;
   if(g_rightPos.x > 32767 + 2000) rotAmt = 3.5f;

   // Apply to the x rotation.
   g_xRot += rotAmt;
   rotAmt = 0;

   // Do the same thing as with the left stick rotation.  The
   // only thing is that the right stick's data is different from
   // the left.  With the left when it is centered the pos is 0, 0
   // but with the right the mid is 32511.  When you press the stick
   // all the way up it is 0, while all the way down is 65535.  I sub
   // the mid value to make the center 0, 0. I don't use exactly
   // 32511 so you can at least put your finger on the joystick
   // without the system thinking your moving it.
   if(g_rightPos.y < 32511 - 1000) rotAmt = -0.5f;
   if(g_rightPos.y < 32511 - 3000) rotAmt = -1.5f;
   if(g_rightPos.y < 32511 - 6000) rotAmt = -2.5f;
   if(g_rightPos.y < 32511 - 12000) rotAmt = -3.5f;

   if(g_rightPos.y > 32511 + 1000) rotAmt = 0.5f;
   if(g_rightPos.y > 32511 + 3000) rotAmt = 1.5f;
   if(g_rightPos.y > 32511 + 6000) rotAmt = 2.5f;
   if(g_rightPos.y > 32511 + 12000) rotAmt = 3.5f;

   // Apply rotation.
   g_yRot += rotAmt;

   // Change depending on which button was pressed.  0 is the upper left
   // (think square on PS2 controller), 1 is lower left (X), 2 is upper
   // right (triangle on PS2 controller), 3 is lower right (circle on PS2).
   // Some controllers can have more.  Mine have up to 12 not including
   // the sticks or arrow pad.
   if(input.ControllerButtonDown(0)) g_object = CUBE;
   if(input.ControllerButtonDown(1)) g_object = SPHERE;
   if(input.ControllerButtonDown(2)) g_object = TEAPOT;


   // Make sure we stay within the valid values.
   if(g_object >= 3) g_object = CUBE;
   if(g_object < 0) g_object = TEAPOT;

   if(g_xRot >= 360.0f) g_xRot = 0.0f;
   if(g_yRot >= 360.0f) g_yRot = 0.0f;
}


void RenderScene()
{
   // Clear the backbuffer.
   g_D3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
                      D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

   // Begin the scene.  Start rendering.
   g_D3DDevice->BeginScene();

      // Apply the view (camera).
      g_D3DDevice->SetTransform(D3DTS_VIEW, &g_ViewMatrix);

      g_D3DDevice->SetMaterial(&g_mat);

      // Get user input.
      GetInput();

      // Used to rotate by mouse/joystick/game pad.
      D3DXMATRIX rot, rotX, rotY;

      // Set the rotation value to the matrix.  Convert degress to radians.
      D3DXMatrixRotationX(&rotX, g_yRot / 180.0f * 3.141592654);
      D3DXMatrixRotationY(&rotY, g_xRot / 180.0f * 3.141592654);

      // Set the rotation matrix.
      rot = rotX * rotY;
      g_D3DDevice->SetTransform(D3DTS_WORLD, &rot);

      // Draw our object.
      switch(g_object)
         {
            case 0:
               g_cube->DrawSubset(0);
               break;

            case 1:
               g_sphere->DrawSubset(0);
               break;
         
            case 2:
               g_teapot->DrawSubset(0);
               break;

            default:
               g_object = 0;
               break;
         }


   // End the scene.  Stop rendering.
   g_D3DDevice->EndScene();

   // Display the scene.
   g_D3DDevice->Present(NULL, NULL, NULL, NULL);
}


void Shutdown()
{
   if(g_D3DDevice != NULL) g_D3DDevice->Release();
   g_D3DDevice = NULL;

   if(g_D3D != NULL) g_D3D->Release();
   g_D3D = NULL;

   if(g_teapot != NULL) g_teapot->Release();
   g_teapot = NULL;

   if(g_cube != NULL) g_cube->Release();
   g_cube = NULL;

   if(g_sphere != NULL) g_sphere->Release();
   g_sphere = NULL;

   input.Shutdown();
}