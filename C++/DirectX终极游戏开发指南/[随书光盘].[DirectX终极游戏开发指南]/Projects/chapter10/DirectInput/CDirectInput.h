/*
   Demo Name:  Direct Input Demo
      Author:  Allen Sherrod
     Chapter:  Ch 10
*/


#ifndef _UGP_DIRECTINPUT_H_
#define _UGP_DIRECTINPUT_H_

#define LEFT_BUTTON     0
#define RIGHT_BUTTON    1

#define KEYS_SIZE       256

#include<dinput.h>


class CDirectInputSystem
{
   public:
      CDirectInputSystem();
      ~CDirectInputSystem();

   public:
      bool Initialize(HWND hwnd, HINSTANCE hInstance, bool mouseExclusive = false);
      bool UpdateDevices();
      
      // Used to create the game controllers.
      BOOL EnumDeviceCallBack(const DIDEVICEINSTANCE *inst, void* pData);

      // Keyboard functions.
      int KeyUp(unsigned int key);
      int KeyDown(unsigned int key);

      // Mouse functions.
      int MouseButtonUp(unsigned int button);
      int MouseButtonDown(unsigned int button);

      // Get mouse position (x, y) and mouse wheel data (z).
      POINT GetMousePos();
      long GetMouseWheelPos();

      // Game controller functions.
      int ControllerButtonUp(unsigned int button);
      int ControllerButtonDown(unsigned int button);
      
      // Get controller main (left) and right stick position.
      POINT GetLeftStickPos();
      POINT GetRightStickPos();

      void Shutdown();

   protected:
      LPDIRECTINPUT8 m_InputSystem;
      
      // Keyboard device.
      LPDIRECTINPUTDEVICE8 m_Keyboard;
      char m_Keys[KEYS_SIZE];
      char m_oldKeys[KEYS_SIZE];
      
      // Mouse device.
      LPDIRECTINPUTDEVICE8 m_Mouse;
      DIMOUSESTATE m_Mouse_State;
      DIMOUSESTATE m_oldMouse_State;

      // Mouse x, y, and wheel position.
      long m_xMPos;
      long m_yMPos;
      long m_zMPos;

      // Game controller device.
      LPDIRECTINPUTDEVICE8 m_GameControl;
      DIJOYSTATE2 m_GC_State;
      DIJOYSTATE2 m_oldGC_State;
      char m_name[256];
      bool m_controllerFound;
      unsigned long m_numButtons;

      // Left and right stick x and y positions.
      long m_xGCPos;
      long m_yGCPos;
      long m_xGCPos2;
      long m_yGCPos2;
      
      // Window handled (needed for game controllers).
      HWND m_hwnd;
};

#endif