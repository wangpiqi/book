


#include<windows.h>
#include<stdio.h>
#include<memory.h>
#include"BMP.h"


unsigned char *LoadBmp(char *file, int &imageWidth, int &imageHeight)
{
	FILE *pFile = NULL;
	BITMAPFILEHEADER header;
	BITMAPINFOHEADER bitmapInfoHeader;
	unsigned char *image = NULL;
	unsigned char textureColors = 0;

   // Open image file.
	pFile = fopen(file, "rb");
	if(pFile == NULL) return NULL;

   // Read file header.
	fread(&header, sizeof(BITMAPFILEHEADER), 1, pFile);

   // Make sure type equals the bmp id.
	if(header.bfType != 0x4D42) { fclose(pFile); return NULL; }

   // Read info header.
	fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, pFile);

   // Some bmps don't save size and it needs to be calculated.  Photoshop
   // does not calculate bmp size correctly (most of the type off by a few bytes).
   bitmapInfoHeader.biSizeImage = bitmapInfoHeader.biWidth * bitmapInfoHeader.biHeight * 3;

   // Record width and height.
   imageWidth = bitmapInfoHeader.biWidth;
   imageHeight = bitmapInfoHeader.biHeight;

   // Move to start of the image data.
	fseek(pFile, header.bfOffBits, SEEK_SET);

   // Allocate image data.
	image = new unsigned char[bitmapInfoHeader.biSizeImage];
	if(!image) { fclose(pFile); return NULL; }

   // Read image data.
	fread(image, sizeof(unsigned char), bitmapInfoHeader.biSizeImage, pFile);

   // Convert from BGR to RGB.
	for(unsigned int index = 0; index < bitmapInfoHeader.biSizeImage; index += 3)
	   {
		   textureColors = image[index];
		   image[index] = image[index + 2];
		   image[index + 2] = textureColors;
	   }

	fclose(pFile);
	return image;
}