


#include"TGA.h"
#include<stdio.h>
#include<memory.h>


unsigned char *LoadUncompressedTga(char *fileName, unsigned char &bitCount,
                                   int &imageWidth, int &imageHeight)
{
   FILE *pfile;
   int components;
	long tgaSize;
   unsigned char *image, tempColor;
   unsigned char tgaHeader[12] = {0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	unsigned char tgaRead[12];
   unsigned char header[6];

   // Open file.
   if(!fileName) return NULL;
	pfile = fopen(fileName, "rb");
	if(!pfile) return NULL;

   // Read header.
	fread(tgaRead, 1, sizeof(tgaRead), pfile);

   // Compare header with one we know represents uncompressed tga.
	if(memcmp(tgaHeader, tgaRead, sizeof(tgaHeader)) != 0)
	   {
		   fclose(pfile);
		   return NULL;
	   }

   // Read image header.
	fread(header, 1, sizeof(header), pfile);
	
	// Calculate width and height of the image.
	imageWidth = header[1] * 256 + header[0];
	imageHeight = header[3] * 256 + header[2];
	
	// Get the bit count.
	bitCount = header[4];

   // Calculate num components (24 = RGB, 32 = RGBA) and image size.
	components = bitCount / 8;
	tgaSize = imageWidth * imageHeight * components;

   // Allocate image array and read image data.
	image = new unsigned char[sizeof(unsigned char) * tgaSize];
	fread(image, sizeof(unsigned char), tgaSize, pfile);
	
	// Convert from BGR to RGB.
	for(int index = 0; index < tgaSize; index += components)
	   {
		   tempColor = image[index];
		   image[index] = image[index + 2];
		   image[index + 2] = tempColor;
	   }

	fclose(pfile);
	return image;
}


bool WriteUncompressedTGA(char *file, int width, int height, int components, unsigned char *outImage)
{
   FILE *pFile = NULL;
   unsigned char tgaHeader[12] = {0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0};
   unsigned char header[6];
   unsigned char tempColors = 0;

   // Open file.
   pFile = fopen(file, "wb");
   if(!pFile) { fclose(pFile); return false; }

   // Set bit count.
   unsigned char bits = components * 8;

   // Create image header.
 	header[0] = width % 256; header[1] = width / 256; header[2] = height % 256;
	header[3] = height / 256; header[4] = bits; header[5] = 0;

   // Write tga header then the image's header.
   fwrite(tgaHeader, sizeof(tgaHeader), 1, pFile);
   fwrite(header, sizeof(header), 1, pFile);

   // Convert from RGB to BGR.
   for(int index = 0; index < width * height * components; index += components)
      {
         tempColors = outImage[index];
         outImage[index] = outImage[index + 2];
         outImage[index + 2] = tempColors;
      }

   // Save image then close file.
   fwrite(outImage, width * height * components, 1, pFile);
   fclose(pFile);

   return true;
}