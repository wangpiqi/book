


#ifndef _UGP_TGA_H_
#define _UGP_TGA_H_


unsigned char *LoadUncompressedTga(char *fileName, unsigned char &bitCount,
                                   int &imageWidth, int &imageHeight);

bool WriteUncompressedTGA(char *file, int width, int height, int components,
                          unsigned char *outImage);

#endif