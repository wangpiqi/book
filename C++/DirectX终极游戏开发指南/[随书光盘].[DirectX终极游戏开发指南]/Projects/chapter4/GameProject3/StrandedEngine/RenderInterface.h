/*
   Demo Name:  Game Project 3
      Author:  Allen Sherrod
     Chapter:  Chapter 4
*/


#ifndef _UGP_RENDERINTERFACE_H_
#define _UGP_RENDERINTERFACE_H_

#include"defines.h"
#include"material.h"
#include"light.h"


class CRenderInterface
{
   public:
      CRenderInterface() : m_screenWidth(0),
         m_screenHeight(0), m_near(0), m_far(0) { }
      virtual ~CRenderInterface() {}
      
      virtual bool Initialize(int w, int h,
         WinHWND mainWin, bool fullScreen) = 0;
      virtual void OneTimeInit() = 0;
      virtual void Shutdown() = 0;
      
      virtual void SetClearCol(float r, float g, float b) = 0;
      virtual void StartRender(bool bColor, bool bDepth,
                               bool bStencil) = 0;
      virtual void ClearBuffers(bool bColor, bool bDepth,
                                bool bStencil) = 0;
      virtual void EndRendering() = 0;

      virtual void SetMaterial(stMaterial *mat) = 0;

      virtual void SetLight(stLight *light, int index) = 0;
      virtual void DisableLight(int index) = 0;

      virtual void SetDepthTesting(RenderState state) = 0;
      virtual void SetTransparency(RenderState state,
         TransState src, TransState dst) = 0;

      virtual int AddTexture2D(char *file, int *texId) = 0;
      virtual void SetTextureFilter(int index, int filter,
                                    int val) = 0;
      virtual void SetMultiTexture() = 0;
      virtual void ApplyTexture(int index, int texId) = 0;
      virtual void SaveScreenShot(char *file) = 0;

      virtual void EnablePointSprites(float size, float min,
                                      float a, float b, float c) = 0;
      virtual void DisablePointSprites() = 0;

      virtual void CalculateProjMatrix(float fov, float n,
                                       float f) = 0;
      virtual void CalculateOrthoMatrix(float n, float f) = 0;
      
      virtual int CreateStaticBuffer(VertexType, PrimType,
         int totalVerts, int totalIndices,
         int stride, void **data, unsigned int *indices,
         int *staticId) = 0;

      virtual int Render(int staticId) = 0;

   protected:
      int m_screenWidth;
      int m_screenHeight;
      bool m_fullscreen;

      WinHWND m_mainHandle;
      
      float m_near;
      float m_far;
};

#endif