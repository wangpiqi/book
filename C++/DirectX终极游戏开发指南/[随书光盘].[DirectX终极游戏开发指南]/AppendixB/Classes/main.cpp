/*
   Demo Name:  Classes
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


class Birthday
{
   public:
      Birthday() { month = day = year = 0; }
      
      
      void Print()
      {
         cout << "The birthday is on: " << month <<
           "/" << day << "/" << year << "." << endl << endl;
      }


      void SetBirthDay(int m, int d, int y)
      {
         SetMonth(m);
         SetDay(d);
         SetYear(y);
      }


      void SetMonth(int m)
      {
         month = m;
      }
      
      int GetMonth()
      {
         return month;
      }


      void SetDay(int d)
      {
         day = d;
      }
      
      int GetDay()
      {
         return day;
      }


      void SetYear(int y)
      {
         year = y;
      }
      
      int GetYear()
      {
         return year;
      }


   private:
      int month;
      int day;
      int year;
};


int main(int arg, char **argc)
{
   Birthday bday;

   bday.SetBirthDay(4, 15, 2006);
   bday.Print();

   return 1;
}