/*
   Demo Name:  Separate Sources
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#ifndef _our_header_
#define _our_header_


#include<iostream>


using namespace std;


void Print1();

void Print2();

void Print3(char *message);

int Add(int val1, int val2);


#endif