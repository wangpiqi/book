/*
   Demo Name:  Pointers
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>

using namespace std;


int main(int arg, char **argc)
{
   int a = 0;
   int *b = 0;

   // Assign.   
   a = 100;
   b = &a;
   
   // This is the output.
   cout << "'a' is located at " << &a << " in memory." << endl;
   cout << "'a' has a value of " << a << "\n" << endl;

   // print out b's info.
   cout << "'b' is located at " << b << " in memory." << endl;
   cout << "'b' has a value of " << *b << "\n" << endl;

   cout << "Now lets make 'a' equals to 150\n" << endl;

   // Make a 150 and lets see if b will also say 150.
   a = 150;

   cout << "Now 'a' has a value of " << a << endl;
   cout << "And 'b' has a value of " << *b << "\n" << endl;
   cout << "Notice that a change in 'a' is the same in 'b'.\n" << endl;

   cout << "Now lets make 'b' equals to 15\n" << endl;

   // Now lets make b 15 and lets see if a is automatically 15.
   *b = 15;

   cout << "Now 'a' has a value of " << a << "\n" << endl;
   cout << "And 'b' has a value of " << *b << "\n" << endl;

   cout << "See how easy it is to use pointers.\n" << endl;

   cout << "Press any key to quit..." << endl;

   return 1;
}