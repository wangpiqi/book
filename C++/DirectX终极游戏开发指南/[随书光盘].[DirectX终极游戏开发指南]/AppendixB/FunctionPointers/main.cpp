/*
   Demo Name:  Functions Pointers
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


void printout()
{
   cout << "This is how you use function pointers." << endl << endl;
}


int main(int arg, char **argc)
{
   cout << "www.UltimateGameProgramming.com" << endl << endl;


   // Here we declare the function pointer.  To do so you need to declare
   // the return type, the name of the function pointer after the * symbol
   // in between () and finally the parameters list.

   void (*functionPtr)();

   // Assign the function pointer to the function.

   functionPtr = printout;

   // We can ue the function pointer instead of the original.
   functionPtr();

   return 1;
}