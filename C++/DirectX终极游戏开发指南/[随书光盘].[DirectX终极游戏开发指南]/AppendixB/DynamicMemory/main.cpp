/*
   Demo Name:  Dynamic Memory
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


int main(int arg, char **argc)
{
   char *string = NULL;
   int *variable = NULL;

   string = new char[64];
   variable = new int;

   if(string != NULL || variable != NULL)
      {
         cout << "Error allocating memory.\n\n";
      }

   if(string != NULL)
      {
         sprintf(string, "Dynamic Memory in C++.\n\n");
         cout << string;

         delete[] string;
      }

   if(variable != NULL)
      {
         *variable = 10;
         cout << *variable << "\n\n";

         delete variable;
      }

   return 1;
}