/*
   Demo Name:  Header Files
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#ifndef _our_header_
#define _our_header_


#include<iostream>


using namespace std;


void Print1()
{
   cout << "Printed from function 1." << endl;
}


void Print2()
{
   cout << "Printed from function 2." << endl;
}


void Print3(char *message)
{
   cout << message << endl;
}


int Add(int val1, int val2)
{
   return val1 + val2;
}


#endif