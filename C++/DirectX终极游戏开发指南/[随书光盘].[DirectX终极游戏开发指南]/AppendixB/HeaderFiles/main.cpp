/*
   Demo Name:  Header Files
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include"ourHeader.h"


int main(int arg, char **argc)
{
   cout << "Header Files Test\n\n";

   Print1();

   Print2();
   
   Print3("Printed from function 3.");
   
   cout << "Result of adding: " << Add(23, 64) << "." << endl;
   
   cout << endl << endl;

   return 1;
}