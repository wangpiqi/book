/*
   Demo Name:  Reading Files
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


// These are all of our includes.
#include<iostream>
#include<fstream>
#include<stdlib.h>


using namespace std;


int main(int arg, char **argc)
{
   char string[512];

   // Input file stream object.
   ifstream file;
   
   // Open file.
   file.open("UltimateGameProgramming.txt");
   
   // Validate that the file opened.
   if(!file)
      {
         cout << "Error: File could not be open.\n" << endl;
         exit(1);
      }

   // Read and print all lines until we reach EOF (end of file).
   while(!file.eof())
      {
         file.getline(string, 512, '\n');
         cout << string << endl;
      }

   cout << endl << endl;

   return 1;
}