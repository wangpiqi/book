/*
   Demo Name:  Writing Files
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>
#include<fstream>
#include<stdlib.h>


using namespace std;


int main(int arg, char **argc)
{
   char string[32] = "This was written to the file.";

   // Output file stream object.
   ofstream file;
   
   // Open file.
   file.open("output.txt");
   
   // Validate that the file opened.
   if(!file)
      {
         cout << "Error: File could not be open.\n" << endl;
         exit(1);
      }

   // Write out.
   //file.getline(string, 512, '\n');
   file.write(string, sizeof(string));
   cout << "Message wrote: " << string << endl;

   file.close();

   cout << endl << endl;

   return 1;
}