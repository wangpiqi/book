/*
   Demo Name:  Functions
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


void Print1()
{
   cout << "Printed from function 1." << endl;
}


void Print2()
{
   cout << "Printed from function 2." << endl;
}


void Print3(char *message)
{
   cout << message << endl;
}


int Add(int val1, int val2)
{
   return val1 + val2;
}


int main(int arg, char **argc)
{
   cout << "Functions Test\n\n";

   Print1();

   Print2();
   
   Print3("Printed from function 3.");
   
   cout << "Result of adding: " << Add(23, 64) << "." << endl;
   
   cout << endl << endl;

   return 1;
}