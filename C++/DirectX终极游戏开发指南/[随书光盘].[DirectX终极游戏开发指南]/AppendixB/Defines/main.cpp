/*
   Demo Name:  Define Statements
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


#define NUMBER 55


int main(int arg, char **argc)
{
   cout << "The NUMBER define is " << NUMBER << ".\n" << endl;

   return 1;
}