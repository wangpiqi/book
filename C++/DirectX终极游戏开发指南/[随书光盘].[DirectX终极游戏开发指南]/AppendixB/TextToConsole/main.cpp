/*
   Demo Name:  Displaying Text to the Console
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using std::cout;


int main(int arg, char **argc)
{
   cout << "www.UltimateGameProgramming.com\n\n";

   return 1;
}