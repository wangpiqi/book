/*
   Demo Name:  Variables
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


int main(int arg, char **argc)
{
   cout << "Variables Test\n\n";

   int var1 = 55;
   bool var2 = true;
   char var3 = 'A';
   float var4 = 29.93f;
   double var5 = 100.3945;

   cout << "var1 = " << var1 << endl;
   cout << "var2 = " << var2 << endl;
   cout << "var3 = " << var3 << endl;
   cout << "var4 = " << var4 << endl;
   cout << "var5 = " << var5 << endl;
   cout << endl << endl;

   return 1;
}