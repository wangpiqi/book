/*
   Demo Name:  Structures
      Author:  Allen Sherrod
     Chapter:  Appendix B
*/


#include<iostream>


using namespace std;


struct Birthday
{
   Birthday() { month = day = year = 0; }

   int month;
   int day;
   int year;
};


int main(int arg, char **argc)
{
   Birthday bday, bday2;

   bday.month = 4;
   bday.day = 15;
   bday.year = 2006;
   
   bday2.month = 1;
   bday2.day = 1;
   bday2.year = 2006;

   cout << "The birthday 1 is on: " << bday.month <<
           "/" << bday.day << "/" << bday.year <<
           "." << endl << endl;

   cout << "The birthday 2 is on: " << bday2.month <<
      "/" << bday2.day << "/" << bday2.year <<
      "." << endl << endl;


   return 1;
}